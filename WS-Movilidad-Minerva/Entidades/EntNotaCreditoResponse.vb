﻿Public Class EntNotaCreditoResponse

    'Solo para procesar respuestas de pedidos
    '//ESTADOS--> S: Sincronizado, N: No Sincronizado, R: Retenido, C: Cliente Congelado, A: Articulos congelados pero SI sincronizado

    Public Property Estado As String
        Public Property NumCab As Long
        Public Property MensajeError As String
        Public Property DocEntrySAP As Long
        Public Property DocNumSAP As String
        Public Property LineNumsCongelados As List(Of Long)

        Sub New()
            LineNumsCongelados = New List(Of Long)
            Estado = ""
            NumCab = 0
            MensajeError = ""
            DocEntrySAP = 0
            DocNumSAP = ""
        End Sub

End Class
