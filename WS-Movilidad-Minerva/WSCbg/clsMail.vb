﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.ComponentModel
Imports System.Net
Imports System.Net.Mail
Imports System.Threading

Public Class clsMail

    Private m_Asunto As String
    Private m_Cuerpo As String
    Private m_NombreOrigen As String
    Private m_MailOrigen As String
    Private m_LastError As String


    Public Function SendMail(ByVal DestinatariosSeparadosPorComas As String) As String
        Try
            If DestinatariosSeparadosPorComas = "" Then
                Return ""
            End If

            Dim Puerto As String = ConfigurationManager.AppSettings.Get("mail_PuertoSMTP").ToString
            Dim SMTP As String = ConfigurationManager.AppSettings.Get("mail_SMTP").ToString
            Dim Usuario As String = ConfigurationManager.AppSettings.Get("mail_Usuario").ToString
            Dim Password As String = ConfigurationManager.AppSettings.Get("mail_Password").ToString
            Dim SSL As Boolean = Boolean.Parse(ConfigurationManager.AppSettings.Get("mail_SSL").ToString)

            'Dim Destinatarios As String = ConfigurationManager.AppSettings.Get("mail_To").ToString
            Dim RemitenteMail As String = ConfigurationManager.AppSettings.Get("mail_From").ToString
            Dim RemitenteNombre As String = ConfigurationManager.AppSettings.Get("mail_Nombre").ToString


            Dim msg As New MailMessage()
            msg.From = New MailAddress(RemitenteMail, RemitenteNombre)

            For Each destino As String In DestinatariosSeparadosPorComas.Split(",")
                msg.To.Add(destino)
            Next

            msg.Body = pCUERPO
            msg.IsBodyHtml = True
            msg.Subject = pASUNTO
            msg.Priority = MailPriority.Normal
            Dim smt As New SmtpClient(SMTP, Puerto)
            smt.Credentials = New NetworkCredential(Usuario, Password)
            smt.EnableSsl = SSL
            smt.Send(msg)

            m_Asunto = ""
            m_Cuerpo = ""
            m_MailOrigen = ""
            m_NombreOrigen = ""

            Return ""

        Catch ex As Exception
            m_LastError = ex.Message
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try

    End Function


    Public Property pASUNTO() As String
        Get
            Return m_Asunto
        End Get
        Set(ByVal value As String)
            m_Asunto = value
        End Set
    End Property

    Public Property pCUERPO() As String
        Get
            Return m_Cuerpo
        End Get
        Set(ByVal value As String)
            m_Cuerpo = "<p>" & value & " </p>"
        End Set
    End Property

    Public ReadOnly Property pLAST_ERROR() As String
        Get
            Return m_LastError
        End Get
    End Property


End Class

