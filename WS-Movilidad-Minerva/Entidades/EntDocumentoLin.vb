﻿Option Strict On
Public Class EntDocumentoLin
    Public Property NumCab As Long
    Public Property LineNum As Long
    Public Property DocEntry As Integer

    Public Property ItemCode As String
    Public Property ItemDesc As String

    Public Property Precio As Double
    Public Property Lote As String
    Public Property CantidadFactor As Double
    Public Property CantidadReal As Double
    Public Property TipoUnidadesCajas As String

    Public Property dto1 As Double
    Public Property dto2 As Double
    Public Property dto3 As Double
    Public Property dto4 As Double
    Public Property dto5 As Double
    Public Property dtoTotalCalculado As Double
    Public Property dtoManual As Double
    Public Property CodigoDescuento As String
    Public Property TotalSinDescuentos As Double
    Public Property TotalConDescuentos As Double
    Public Property IVAPorcentajeLinea As Double
    Public Property IVARecargoPorcentajeLinea As Double
    Public Property ListaLotes As List(Of EntLote)

    Public Property LineaOferta As String
    Public Property LineaNueva As String

    Public Property Sincro As String

End Class
