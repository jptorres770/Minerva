﻿Option Strict On

Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web

Imports Entidades
Imports Newtonsoft.Json
Imports System.Globalization
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Resources

Public Class clsDatosSubida
    Private oWSOrigen As WebService

    Public Const c_IGIC As String = "IGIC"
    Public Const c_REC As String = "REC"
    Public Const c_IVA As String = "IVA"
    Public Const c_UE As String = "UE"   ' Unión Europea


    Sub New(ByVal oWebService As WebService)
        Me.oWSOrigen = oWebService
    End Sub

    'Crear clientes
    Public Function setCliente(ByVal Empleado As Integer, ByVal strJson As String) As EntClienteResponse
        Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
        Dim oCompany As SAPbobsCOM.Company = Nothing
        Dim retVal As EntClienteResponse = New EntClienteResponse()
        Dim objCliente As EntCliente = Nothing

        Try
            LogInSAP()
            oCompany = getCompany()
            objCliente = JsonConvert.DeserializeObject(Of EntCliente)(strJson)
            oBP = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners), SAPbobsCOM.BusinessPartners)

            oBP.CardCode = "C" + getCardCodeNumMax().ToString
            oBP.CardName = objCliente.Nombre
            oBP.FederalTaxID = objCliente.NIF
            oBP.SalesPersonCode = CInt(getSlpCode(Empleado))
            oBP.Phone1 = objCliente.Telefono1
            oBP.Phone2 = objCliente.Telefono2
            oBP.EmailAddress = objCliente.Email

            oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo
            oBP.Addresses.AddressName = "Bill To"
            oBP.Addresses.City = objCliente.CiudadFactura
            oBP.Addresses.Block = objCliente.ProvinciaFactura
            oBP.Addresses.State = objCliente.DireccionFactura
            oBP.Addresses.Add()

            oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo
            oBP.Addresses.AddressName = "Ship To"
            oBP.Addresses.City = objCliente.CiudadFactura
            oBP.Addresses.Block = objCliente.ProvinciaFactura
            oBP.Addresses.State = objCliente.DireccionFactura
            oBP.Addresses.UserFields.Fields.Item("U_SEIRUTA").Value = "100"
            oBP.Addresses.UserFields.Fields.Item("U_SEIPredeterminada").Value = "S"
            oBP.Addresses.Add()

            oBP.BPPaymentMethods.PaymentMethodCode = "Scotiabank"

            oBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer
            oBP.GroupCode = objCliente.Grupo
            oBP.ContactEmployees.Name = objCliente.Contacto

            If oBP.Add() = 0 Then
                retVal.Estado = "S"
                retVal.MensajeError = "Cliente Creado Correctamente"
            Else
                retVal.Estado = "N"
                retVal.MensajeError = "Error creando Cliente..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                clsLog.Log.Error("Empleado:" & Empleado & " Error creando Cliente..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                clsLog.Log.Info("Dump JSON CAB")
                clsLog.Log.Info(strJson)
                clsLog.Log.Info("End dump JSON CAB")
                Return retVal
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "Empleado:" & Empleado & " JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON CAB")

        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = "Empleado:" & Empleado & " Error: " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON CAB")

        Finally
            If Not oBP Is Nothing Then
                LiberarObjCOM(CObj(oBP))
            End If
        End Try

        Return retVal

    End Function


    Public Function setActividad(ByVal Empleado As Integer, ByVal strJson As String) As EntActividadResponse
        Dim oActividad As SAPbobsCOM.Contacts = Nothing
        Dim oCompany As SAPbobsCOM.Company = Nothing
        Dim retVal As EntActividadResponse = New EntActividadResponse()
        Dim objActividad As EntActividad = Nothing

        Try
            oCompany = getCompany()
            objActividad = JsonConvert.DeserializeObject(Of EntActividad)(strJson)
            oActividad = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oContacts), SAPbobsCOM.Contacts)


            oActividad.Activity = SAPbobsCOM.BoActivities.cn_Task
            oActividad.CardCode = objActividad.IDCliente
            oActividad.HandledBy = CInt(ConfigurationManager.AppSettings.Get("UsuarioTesoreria").ToString)
            oActividad.Notes = "PAGO RECIBIDO DE - " & objActividad.IDCliente + vbNewLine

            Dim ListaFacturas As List(Of EntFacturaPendienteCab) = New List(Of EntFacturaPendienteCab)


            For i As Integer = 0 To objActividad.ListaFacturas.Count - 1 Step 1

                oActividad.Notes += " " + vbNewLine +
                                    "Numero de Factura: " + objActividad.ListaFacturas(i).NumeroFactura.ToString + vbNewLine +
                                    "Numero de Factura Vendedor: " + objActividad.ListaFacturas(i).NumeroFacturaVendedor.ToString + vbNewLine +
                                    "Importe a Pagar: " + objActividad.ListaFacturas(i).ImporteAPagar.ToString
            Next

            oActividad.Notes += " " + vbNewLine +
                                "Comentarios: " + objActividad.Comentarios


            oActividad.Notes = objActividad.Comentarios

            If oActividad.Add() = 0 Then
                retVal.Estado = "S"
                retVal.MensajeError = "Actividad Creada Correctamente"
            Else
                retVal.Estado = "N"
                retVal.MensajeError = " Error creando Actividad..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                clsLog.Log.Error("Empleado:" & Empleado & " Error creando Actividad..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                clsLog.Log.Info("Dump JSON CAB")
                clsLog.Log.Info(strJson)
                clsLog.Log.Info("End dump JSON CAB")
                Return retVal
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "Empleado:" & Empleado & " JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON CAB")

        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = "Empleado:" & Empleado & " Error: " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON CAB")

        Finally
            If Not oActividad Is Nothing Then
                LiberarObjCOM(CObj(oActividad))
            End If
        End Try

        Return retVal

    End Function

    Public Function setPedidos(ByVal Empleado As Integer, ByVal strJsonCab As String, ByVal strJsonLin As String) As EntDocumentoResponse

        Dim oDoc As SAPbobsCOM.Documents = Nothing
        Dim retVal As New EntDocumentoResponse
        Dim oCompany As SAPbobsCOM.Company = Nothing

        Try
            'Conexion con SAP
            oCompany = getCompany()

            Dim objPedido As EntDocumentoCab = JsonConvert.DeserializeObject(Of EntDocumentoCab)(strJsonCab)
            Dim objLineasPedido As List(Of EntDocumentoLin) = JsonConvert.DeserializeObject(Of List(Of EntDocumentoLin))(strJsonLin)
            Dim objLineasPedidoNuevas As New List(Of EntDocumentoLin)

            'Si llega un pedido json valido
            If Not objPedido Is Nothing Then

                'Lo primero, igualamos al número de cabecera que nos llega.
                retVal.NumCab = objPedido.NumCab

                '--CREACION DEL PEDIDO--
                'Creamos objeto pedido.
                oDoc = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders), SAPbobsCOM.Documents)

                'CABECERA DEL PEDIDO
                oDoc.CardCode = objPedido.CardCode
                oDoc.DocDate = DateTime.ParseExact(objPedido.DocDate.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oDoc.SalesPersonCode = getComercial_de_Empleado(objPedido.EmpID)
                oDoc.DocDueDate = DateTime.ParseExact(objPedido.FechaEntrega.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oDoc.Comments = objPedido.Comentarios
                oDoc.UserFields.Fields.Item("U_SEI_APK").Value = "S"

                'CAMPOS DE USUARIO
                oDoc.UserFields.Fields.Item("U_SEILon").Value = Mid(objPedido.Longitud, 20)
                oDoc.UserFields.Fields.Item("U_SEILat").Value = Mid(objPedido.Latitud, 20)
                oDoc.UserFields.Fields.Item("U_SEIHCreacion").Value = objPedido.HoraCreacion
                oDoc.UserFields.Fields.Item("U_SEIFCreacion").Value = objPedido.DocDate
                oDoc.UserFields.Fields.Item("U_SEINumCab").Value = CInt(objPedido.NumCab)
                oDoc.UserFields.Fields.Item("U_SEIEmpID").Value = Empleado
                'FIN CAMPOS DE USUARIO


                'If objPedido.IDDireccion <> "" Then
                '    'Del tablet la direccion viene sin comilla simple. Hay que sacar la direccion real.
                '    Dim IDDireccionReal As String = getIDDireccionRealCliente(objPedido.CardCode, objPedido.IDDireccion)

                '    If IDDireccionReal <> "" Then
                '        oDoc.ShipToCode = IDDireccionReal
                '    End If
                'End If

                oDoc.NumAtCard = objPedido.NumeroPedidoCliente

                Dim slpCode As Integer = getComercial_de_Empleado(objPedido.EmpID)
                Dim sAlmacenCliente As String = getAlmacenCliente(slpCode)
                Dim sTipoImpuesto As String = ""

                'FIN CAMPOS DE USUARIO

                Dim iNumLin As Integer = 1
                For Each objLineaDoc As EntDocumentoLin In objLineasPedido

                    If iNumLin > 1 Then
                        oDoc.Lines.Add()
                    End If

                    oDoc.Lines.SetCurrentLine(oDoc.Lines.Count - 1)

                    'inicio asignacion

                    oDoc.Lines.ItemCode = objLineaDoc.ItemCode
                    oDoc.Lines.ItemDescription = objLineaDoc.ItemDesc
                    oDoc.Lines.WarehouseCode = sAlmacenCliente
                    oDoc.Lines.TaxCode = getImuestoItem(objLineaDoc.ItemCode)

                    If objLineaDoc.TipoUnidadesCajas = "C" Then
                        oDoc.Lines.Factor1 = objLineaDoc.CantidadFactor
                    Else
                        oDoc.Lines.Quantity = objLineaDoc.CantidadReal
                    End If

                    'Ojo, si llega con el literal 'Eliminados" no se hace nada. Ojo que tiene long.8 el campo. Se cambia el valor que no entra.
                    If objLineaDoc.CodigoDescuento = "Eliminados" Then
                        objLineaDoc.CodigoDescuento = "Elim."
                        clsLog.Log.Info("Eliminados descuentos forzosamente en linea " & objLineaDoc.LineNum & " Item:" & objLineaDoc.ItemCode)
                    Else

                        oDoc.Lines.DiscountPercent = objLineaDoc.dtoManual
                        If objLineaDoc.LineaOferta = "S" Then
                            oDoc.Lines.UnitPrice = 0
                        Else
                            oDoc.Lines.Price = objLineaDoc.Precio
                        End If

                    End If

                    'FIN
                    'Siguiente linea
                    iNumLin += 1

                Next

            End If

            'Si todas las lineas del pedido estan congeladas, el estado debe ser "N" y el mensaje "Todo congelado"
            If retVal.LineNumsCongelados.Count = objLineasPedido.Count Then
                retVal.Estado = "N"
                retVal.MensajeError = "Están todas las lineas congeladas!"
            Else


                If oDoc.Add <> 0 Then
                    'Controlamos si es error del TN es por pedido duplicado y devolvemos el DocNum
                    retVal.Estado = "N"
                    retVal.MensajeError = " Error creando pedido... " & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                    clsLog.Log.Error("Empleado: " & Empleado & " Error creando pedido..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                    clsLog.Log.Info("Dump JSON CAB")
                    clsLog.Log.Info(strJsonCab)
                    clsLog.Log.Info("End dump JSON CAB")
                    clsLog.Log.Info("Dump JSON LIN")
                    clsLog.Log.Info(strJsonLin)
                    clsLog.Log.Info("End dump JSON LIN")
                    Return retVal

                Else

                    If retVal.LineNumsCongelados.Count > 0 Then
                        retVal.Estado = "A"
                    Else
                        retVal.Estado = "S"
                        retVal.MensajeError = "Pedido Se Creo Correctamente"
                    End If

                    'Por referencia
                    Dim refDocNum As String = ""
                    Dim refDocEntry As Integer = 0

                    'Obtener el docnum y docentry creado.
                    getMaxDocNum_DocEntry(Empleado, objPedido.NumCab, refDocNum, refDocEntry)

                    retVal.DocNumSAP = refDocNum
                    retVal.DocEntrySAP = refDocEntry

                    GuardarLineasPedidoNuevas(objLineasPedidoNuevas, refDocEntry, refDocNum, objPedido.EmpID)

                    'Envio de correo a cliente.
                    If objPedido.EnvioMail = "S" Then

                        Dim oDestinoMailCliente As String = getCorreoCliente(objPedido.CardCode)
                        If oDestinoMailCliente <> "" Then
                            If oDoc.GetByKey(refDocEntry) Then
                                Dim oMail As New clsMail
                                Dim oMailFormateoPedido As New clsMailPedidoFormato(oDoc, refDocNum)
                                oMail.pASUNTO = "Minerva - Pedido realizado (N.: " & refDocNum & ")"
                                oMail.pCUERPO = oMailFormateoPedido.getHTML
                                oMail.SendMail(oDestinoMailCliente)
                            Else
                                clsLog.Log.Error("No se ha podido recuperar el Entry:" & refDocEntry)
                            End If
                        Else
                            clsLog.Log.Warn("Correo marcado con mail pero el cliente no lo tiene. CardCode:" & objPedido.CardCode)
                        End If

                    End If

                    clsLog.Log.Info("Creacion de pedido finalizada Entry:" & refDocEntry & " Estado:" & retVal.Estado)

                End If
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Finally
            If Not oDoc Is Nothing Then
                LiberarObjCOM(CObj(oDoc))
            End If
        End Try
        Return retVal

    End Function

    Public Function setNotasCredito(ByVal Empleado As Integer, ByVal strJsonCab As String, ByVal strJsonLin As String) As EntNotaCreditoResponse

        Dim oDoc As SAPbobsCOM.Documents = Nothing
        Dim retVal As New EntNotaCreditoResponse
        Dim oCompany As SAPbobsCOM.Company = Nothing

        Try
            'Conexion con SAP
            oCompany = getCompany()

            Dim objNotaCredito As EntNotaCreditoCab = JsonConvert.DeserializeObject(Of EntNotaCreditoCab)(strJsonCab)
            Dim objLineasNotaCredito As List(Of EntFacturaPendienteLin) = JsonConvert.DeserializeObject(Of List(Of EntFacturaPendienteLin))(strJsonLin)
            Dim objLineasNotaCreditoNuevas As New List(Of EntFacturaPendienteLin)


            'Si llega un factura json valido
            If Not objNotaCredito Is Nothing Then

                'Lo primero, igualamos al número de cabecera que nos llega.

                'COLOCAR
                'retVal.NumCab = objNotaCredito.NumCab

                '--CREACION DE FACTURA--
                'Creamos objeto FACTURA.
                oDoc = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes), SAPbobsCOM.Documents)
                'CABECERA DE FACTURA


                oDoc.CardCode = objNotaCredito.Factura.IDCliente
                oDoc.SalesPersonCode = getComercial_de_Empleado(Empleado)
                oDoc.Comments = objNotaCredito.Comentario
                'COLOCAR
                'oDoc.Comments = objNotaCredito.Comentarios

                oDoc.UserFields.Fields.Item("U_SEI_APK").Value = "S"

                'COLOCAR
                'oDoc.NumAtCard = objNotaCredito.NumeroPedidoCliente

                'CAMPOS DE USUARIO

                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEILon").Value = Mid(objNotaCredito.Longitud, 20)
                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEILat").Value = Mid(objNotaCredito.Latitud, 20)
                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEIHCreacion").Value = objNotaCredito.HoraCreacion
                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEIFCreacion").Value = objNotaCredito.DocDate
                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEINumCab").Value = CInt(objNotaCredito.NumCab)
                'COLOCAR
                'oDoc.UserFields.Fields.Item("U_SEIEmpID").Value = Empleado

                'FIN CAMPOS DE USUARIO

                Dim iNumLin As Integer = 1
                For Each objLineaDoc As EntFacturaPendienteLin In objLineasNotaCredito

                    If iNumLin > 1 Then
                        oDoc.Lines.Add()
                    End If

                    oDoc.Lines.SetCurrentLine(oDoc.Lines.Count - 1)

                    'Inicio asignacion
                    'CAMBIAR
                    oDoc.Lines.BaseEntry = CInt(objNotaCredito.Factura.DocEntry)
                    oDoc.Lines.BaseLine = CInt(objLineaDoc.LineNum)
                    oDoc.Lines.BaseType = SAPbobsCOM.BoObjectTypes.oInvoices
                    oDoc.Lines.Quantity = objLineaDoc.Cantidad
                    oDoc.Lines.ItemCode = objLineaDoc.CodigoArticulo

                    'LOTES                    

                    'agregar los los item que tengan lotes a una lista
                    Dim objLotes1 As List(Of EntLote) = New List(Of EntLote)
                    Dim objLotes As List(Of EntLote) = New List(Of EntLote)


                    Dim ListaItemCodes As List(Of String) = New List(Of String)

                    For Each item In objLineasNotaCredito
                        If RevisarGestionadoPorLotes(item.CodigoArticulo) = True Then

                            objLotes1 = getLotesNotaCredito(objNotaCredito.Factura.DocEntry, objLineaDoc.CodigoArticulo)

                            For j = 0 To objLotes1.Count - 1
                                If objLotes.Count > 0 Then
                                    Dim encontrado = False
                                    For t = 0 To objLotes.Count - 1
                                        If objLotes1(j).Lote = objLotes(t).Lote Then
                                            encontrado = True
                                            Exit For
                                        Else
                                            encontrado = False
                                        End If
                                    Next
                                    If encontrado = False Then
                                        objLotes.Add(objLotes1(j))
                                    End If
                                Else
                                    objLotes.Add(objLotes1(j))
                                End If

                            Next
                        End If
                    Next

                    'LOTES
                    If RevisarGestionadoPorLotes(objLineaDoc.CodigoArticulo) = True Then

                        If objLotes.Count > 0 Then
                            For i As Integer = 0 To objLotes.Count - 1
                                If objLineaDoc.Cantidad <> 0 Then
                                    If objLotes(i).CodigoItem = objLineaDoc.CodigoArticulo Then
                                        If objLotes(i).Cantidad > 0 Then
                                            If objLotes(i).Cantidad >= objLineaDoc.Cantidad Then
                                                oDoc.Lines.BatchNumbers.Quantity = objLineaDoc.Cantidad
                                                oDoc.Lines.BatchNumbers.BatchNumber = objLotes(i).Lote
                                                oDoc.Lines.BatchNumbers.BaseLineNumber = oDoc.Lines.Count - 1
                                                oDoc.Lines.BatchNumbers.Add()
                                                objLotes(i).Cantidad -= objLineaDoc.Cantidad
                                                objLineaDoc.Cantidad = 0
                                            ElseIf objLotes(i).Cantidad < objLineaDoc.Cantidad Then
                                                oDoc.Lines.BatchNumbers.Quantity = objLotes(i).Cantidad
                                                oDoc.Lines.BatchNumbers.BatchNumber = objLotes(i).Lote
                                                oDoc.Lines.BatchNumbers.BaseLineNumber = oDoc.Lines.Count - 1
                                                oDoc.Lines.BatchNumbers.Add()
                                                objLineaDoc.Cantidad -= objLotes(i).Cantidad
                                                objLotes(i).Cantidad = 0
                                            End If
                                        End If
                                    End If
                                End If

                            Next
                        End If
                    End If

                    'FIN LOTES
                    'FIN
                    'Siguiente linea
                    iNumLin += 1

                Next

            End If

            If oDoc.Add <> 0 Then
                'Controlamos si es error del TN es por pedido duplicado y devolvemos el DocNum
                If oCompany.GetLastErrorDescription.Contains("TN: Existe el pedido!") Then
                    clsLog.Log.Fatal(oCompany.GetLastErrorDescription)
                    retVal.Estado = "S"
                    retVal.MensajeError = "Ya existe"
                    Return retVal
                End If
                ''error al crear el pedido
                retVal.Estado = "N"
                retVal.MensajeError = "Error creando notas Credito..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                clsLog.Log.Error("Empleado: " & Empleado & " Error creando notas Credito..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                clsLog.Log.Info("Dump JSON CAB")
                clsLog.Log.Info(strJsonCab)
                clsLog.Log.Info("End dump JSON CAB")
                clsLog.Log.Info("Dump JSON LIN")
                clsLog.Log.Info(strJsonLin)
                clsLog.Log.Info("End dump JSON LIN")
                Return retVal
            Else

                If retVal.LineNumsCongelados.Count > 0 Then
                    retVal.Estado = "A"
                Else
                    retVal.Estado = "S"
                    retVal.MensajeError = "La Nota Credito Se Creo Correctamente"
                End If

                clsLog.Log.Info("Creacion de pedido finalizada Estado:" & retVal.Estado)

            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("Empleado: " & Empleado & " JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Finally
            If Not oDoc Is Nothing Then
                LiberarObjCOM(CObj(oDoc))
            End If
        End Try

        Return retVal

    End Function

    Public Function setFacturas(ByVal Empleado As Integer, ByVal strJsonCab As String, ByVal strJsonLin As String) As EntDocumentoResponse

        Dim oDoc As SAPbobsCOM.Documents = Nothing
        Dim retVal As New EntDocumentoResponse
        Dim oCompany As SAPbobsCOM.Company = Nothing
        Try
            'Conexion con SAP
            oCompany = getCompany()

            Dim objFactura As EntDocumentoCab = JsonConvert.DeserializeObject(Of EntDocumentoCab)(strJsonCab)
            Dim objLineasFactura As List(Of EntDocumentoLin) = JsonConvert.DeserializeObject(Of List(Of EntDocumentoLin))(strJsonLin)
            Dim objLineasFacturaNuevas As New List(Of EntDocumentoLin)


            'Si llega un factura json valido
            If Not objFactura Is Nothing Then

                'Lo primero, igualamos al número de cabecera que nos llega.
                retVal.NumCab = objFactura.NumCab

                '--CREACION DE FACTURA--
                'Creamos objeto FACTURA.
                oDoc = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices), SAPbobsCOM.Documents)

                'CABECERA DE FACTURA

                oDoc.CardCode = objFactura.CardCode
                oDoc.DocDate = DateTime.ParseExact(objFactura.DocDate.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oDoc.SalesPersonCode = getComercial_de_Empleado(objFactura.EmpID)
                'oDoc.DocDueDate = DateTime.ParseExact(objFactura.FechaEntrega.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oDoc.Comments = objFactura.Comentarios
                oDoc.UserFields.Fields.Item("U_SEI_APK").Value = "S"

                'CAMPOS DE USUARIO
                oDoc.UserFields.Fields.Item("U_SEILon").Value = Mid(objFactura.Longitud, 20)
                oDoc.UserFields.Fields.Item("U_SEILat").Value = Mid(objFactura.Latitud, 20)
                oDoc.UserFields.Fields.Item("U_SEIHCreacion").Value = objFactura.HoraCreacion
                oDoc.UserFields.Fields.Item("U_SEIFCreacion").Value = objFactura.DocDate
                oDoc.UserFields.Fields.Item("U_SEINumCab").Value = CInt(objFactura.NumCab)
                oDoc.UserFields.Fields.Item("U_SEIEmpID").Value = Empleado
                'FIN CAMPOS DE USUARIO

                'Direcciones
                oDoc.ShipToCode = objFactura.IDDireccion

                'If objFactura.IDDireccion <> "" Then
                'Del tablet la direccion viene sin comilla simple. Hay que sacar la direccion real.
                'Dim IDDireccionReal As String = getIDDireccionRealCliente(objFactura.CardCode, objFactura.IDDireccion)

                'If IDDireccionReal <> "" Then
                'oDoc.ShipToCode = IDDireccionReal
                'End If
                'End If

                oDoc.NumAtCard = objFactura.NumeroPedidoCliente
                        Dim slpCode As Integer = getComercial_de_Empleado(objFactura.EmpID)
                        Dim sAlmacenCliente As String = getAlmacenCliente(slpCode)
                'FIN CAMPOS DE USUARIO

                Dim iNumLin As Integer = 1


                'agregar los los item que tengan lotes a una lista
                Dim objLotes1 As List(Of EntLote) = New List(Of EntLote)
                Dim objLotes As List(Of EntLote) = New List(Of EntLote)


                Dim ListaItemCodes As List(Of String) = New List(Of String)

                For Each item In objLineasFactura
                    If RevisarGestionadoPorLotes(item.ItemCode) = True Then

                        objLotes1 = getLotes(slpCode, item.ItemCode)

                        For j = 0 To objLotes1.Count - 1
                            If objLotes.Count > 0 Then
                                Dim encontrado = False
                                For t = 0 To objLotes.Count - 1
                                    If objLotes1(j).Lote = objLotes(t).Lote And objLotes(t).CodigoItem = objLotes1(j).CodigoItem Then
                                        encontrado = True
                                        Exit For
                                    Else
                                        encontrado = False
                                    End If
                                Next
                                If encontrado = False Then
                                    objLotes.Add(objLotes1(j))
                                End If
                            Else
                                objLotes.Add(objLotes1(j))
                            End If

                        Next
                    End If
                Next


                For Each objLineaDoc As EntDocumentoLin In objLineasFactura
                            If iNumLin > 1 Then
                                oDoc.Lines.Add()
                            End If

                            oDoc.Lines.SetCurrentLine(oDoc.Lines.Count - 1)

                            'inicio asignacion

                            oDoc.Lines.ItemCode = objLineaDoc.ItemCode
                            oDoc.Lines.ItemDescription = objLineaDoc.ItemDesc
                            oDoc.Lines.WarehouseCode = sAlmacenCliente
                            oDoc.Lines.TaxCode = getImuestoItem(objLineaDoc.ItemCode)

                            If objLineaDoc.TipoUnidadesCajas = "C" Then
                                oDoc.Lines.Factor1 = objLineaDoc.CantidadFactor
                            Else
                                oDoc.Lines.Quantity = objLineaDoc.CantidadReal
                            End If

                            oDoc.Lines.DiscountPercent = objLineaDoc.dtoManual
                            If objLineaDoc.LineaOferta = "S" Then
                                oDoc.Lines.UnitPrice = 0
                            Else
                                oDoc.Lines.Price = objLineaDoc.Precio
                            End If

                    'LOTES
                    If RevisarGestionadoPorLotes(objLineaDoc.ItemCode) = True Then

                        If objLotes.Count > 0 Then
                            For i As Integer = 0 To objLotes.Count - 1
                                If objLineaDoc.CantidadReal <> 0 Then
                                    If objLotes(i).CodigoItem = objLineaDoc.ItemCode Then
                                        If objLotes(i).Cantidad > 0 Then
                                            If objLotes(i).Cantidad >= objLineaDoc.CantidadReal Then
                                                oDoc.Lines.BatchNumbers.Quantity = objLineaDoc.CantidadReal
                                                oDoc.Lines.BatchNumbers.BatchNumber = objLotes(i).Lote
                                                oDoc.Lines.BatchNumbers.BaseLineNumber = oDoc.Lines.Count - 1
                                                oDoc.Lines.BatchNumbers.Add()
                                                objLotes(i).Cantidad -= objLineaDoc.CantidadReal
                                                objLineaDoc.CantidadReal = 0
                                            ElseIf objLotes(i).Cantidad < objLineaDoc.CantidadReal Then
                                                oDoc.Lines.BatchNumbers.Quantity = objLotes(i).Cantidad
                                                oDoc.Lines.BatchNumbers.BatchNumber = objLotes(i).Lote
                                                oDoc.Lines.BatchNumbers.BaseLineNumber = oDoc.Lines.Count - 1
                                                oDoc.Lines.BatchNumbers.Add()
                                                objLineaDoc.CantidadReal -= objLotes(i).Cantidad
                                                objLotes(i).Cantidad = 0
                                            End If
                                        End If
                                    End If
                                End If

                            Next
                        End If
                    End If

                    'FIN LOTES

                    'If objFactura.DocumentoBase <> Nothing Then
                    'oDoc.Lines.BaseEntry = objFactura.DocumentoBase
                    'End If

                    'FIN
                    'Siguiente linea
                    iNumLin += 1

                        Next

                    End If


                    If oDoc.Add <> 0 Then
                ''error al crear la factura
                retVal.Estado = "N"
                retVal.MensajeError = "Error creando factura..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                clsLog.Log.Error("Empleado: " & Empleado & " Error creando factura..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                clsLog.Log.Info("Dump JSON CAB")
                clsLog.Log.Info(strJsonCab)
                clsLog.Log.Info("End dump JSON CAB")
                clsLog.Log.Info("Dump JSON LIN")
                clsLog.Log.Info(strJsonLin)
                clsLog.Log.Info("End dump JSON LIN")
                Return retVal
            Else

                If retVal.LineNumsCongelados.Count > 0 Then
                    retVal.Estado = "A"
                Else
                    retVal.Estado = "S"
                    retVal.MensajeError = "Factura Se Creo Correctamente"
                End If

                'Por referencia
                Dim refDocNum As String = ""
                Dim refDocEntry As Integer = 0

                'Obtener el docnum y docentry creado.
                getMaxDocNumFac_DocEntry(Empleado, objFactura.NumCab, refDocNum, refDocEntry)

                retVal.DocNumSAP = refDocNum
                retVal.DocEntrySAP = refDocEntry

                GuardarLineasPedidoNuevas(objLineasFacturaNuevas, refDocEntry, refDocNum, objFactura.EmpID)

                'Envio de correo a cliente.
                If objFactura.EnvioMail = "S" Then

                    Dim oDestinoMailCliente As String = getCorreoCliente(objFactura.CardCode)
                    If oDestinoMailCliente <> "" Then
                        If oDoc.GetByKey(refDocEntry) Then
                            Dim oMail As New clsMail
                            Dim oMailFormateoPedido As New clsMailPedidoFormato(oDoc, refDocNum)
                            oMail.pASUNTO = "Minerva - Pedido realizado (N.: " & refDocNum & ")"
                            oMail.pCUERPO = oMailFormateoPedido.getHTML
                            oMail.SendMail(oDestinoMailCliente)
                        Else
                            clsLog.Log.Error("No se ha podido recuperar el Entry:" & refDocEntry)
                        End If
                    Else
                        clsLog.Log.Warn("Correo marcado con mail pero el cliente no lo tiene. CardCode:" & objFactura.CardCode)
                    End If

                End If

                clsLog.Log.Info("Creacion de factura finalizada Entry:" & refDocEntry & " Estado:" & retVal.Estado)

            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("Empleado: " & Empleado & " JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON CAB")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON CAB")
            clsLog.Log.Info("Dump JSON LIN")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON LIN")

        Finally
            If Not oDoc Is Nothing Then
                LiberarObjCOM(CObj(oDoc))
            End If
        End Try

        Return retVal

    End Function

    Public Function setFacturasCobro(ByVal Empleado As Integer, ByVal Password As String, ByVal strJson As String) As EntCobroResponse

        Dim retVal As EntCobroResponse = New EntCobroResponse
        Dim oCompany As SAPbobsCOM.Company = Nothing
        Dim oCobro As SAPbobsCOM.Payments = Nothing

        Try

            Dim objCobro As EntFacturaCobroCab = JsonConvert.DeserializeObject(Of EntFacturaCobroCab)(strJson)
            'Conexion a SAP
            oCompany = getCompany()

            oCobro = CType(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments), SAPbobsCOM.Payments)
            If Not objCobro Is Nothing Then

                oCobro.DocType = SAPbobsCOM.BoRcptTypes.rCustomer
                oCobro.CardCode = objCobro.IDCliente
                oCobro.DocDate = DateTime.ParseExact(objCobro.FechaDeCobro.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oCobro.DueDate = DateTime.ParseExact(objCobro.FechaDeCobro.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oCobro.TaxDate = DateTime.ParseExact(objCobro.FechaDeCobro.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                oCobro.CounterReference = objCobro.ReferenciaDePago
                oCobro.UserFields.Fields.Item("U_SEI_APK").Value = "S"

                For i As Integer = 0 To objCobro.Facturas.Count - 1 Step 1
                    oCobro.Invoices.DocEntry = objCobro.Facturas(i).DocEntry
                    oCobro.Invoices.SumApplied = objCobro.Facturas(i).ImporteAPagar

                    If i < objCobro.Facturas.Count - 1 Then
                        oCobro.Invoices.Add()
                    End If
                Next

                Dim ListaCheques As List(Of EntMedioDePago) = New List(Of EntMedioDePago)

                For i As Integer = 0 To objCobro.MedioDePago.Count - 1 Step 1
                    If objCobro.MedioDePago(i).Importe <> 0 Then
                        Select Case objCobro.MedioDePago(i).Tipo
                            Case "E"
                                oCobro.CashAccount = getCuentaEfectivo(Empleado)
                                oCobro.CashSum = objCobro.MedioDePago(i).Importe
                            Case "T"
                                oCobro.TransferAccount = objCobro.MedioDePago(i).CuentaTransferencia
                                oCobro.TransferSum = objCobro.MedioDePago(i).Importe
                                oCobro.TransferReference = objCobro.MedioDePago(i).Referencia
                                oCobro.TransferDate = DateTime.ParseExact(objCobro.MedioDePago(i).Fecha.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                            Case "C"
                                oCobro.Checks.CheckAccount = objCobro.MedioDePago(i).CuentaCheque
                                ListaCheques.Add(objCobro.MedioDePago(i))
                        End Select
                    End If
                Next

                If ListaCheques.Count > 0 Then
                    For i As Integer = 0 To ListaCheques.Count - 1 Step 1
                        oCobro.Checks.BankCode = ListaCheques(i).CodBanco
                        oCobro.Checks.DueDate = DateTime.ParseExact(ListaCheques(i).FechaVencimiento.ToString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                        oCobro.Checks.CheckSum = ListaCheques(i).Importe
                        oCobro.Checks.CheckNumber = CInt(ListaCheques(i).NumCheque)
                        oCobro.Checks.Trnsfrable = CType(ListaCheques(i).Endosado, SAPbobsCOM.BoYesNoEnum)

                        If i < ListaCheques.Count - 1 Then
                            oCobro.Checks.Add()
                        End If
                    Next
                End If

                If oCobro.Add = 0 Then
                    Dim DocEntryCobro As String = oCompany.GetNewObjectKey()
                    oCobro.GetByKey(CInt(DocEntryCobro))
                    Dim DocNumCobro As String = oCobro.DocNum.ToString()
                    clsLog.Log.Info("Se ha creado correctamente el cobro" + DocNumCobro)
                    retVal.Estado = "S"
                    retVal.MensajeError = "Se ha creado correctamente el cobro: " + DocNumCobro
                    Return retVal
                Else
                    clsLog.Log.Info("Fallo la creacion del Cobro")
                    retVal.Estado = "N"
                    retVal.MensajeError = "Error creando Cobro..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription
                    clsLog.Log.Error("Empleado: " & Empleado & " Error creando Cobro..." & oCompany.GetLastErrorCode & "-" & oCompany.GetLastErrorDescription)
                    clsLog.Log.Info("Dump JSON CAB")
                    clsLog.Log.Info(strJson)
                    clsLog.Log.Info("End dump JSON CAB")
                End If
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal.Estado = "N"
            retVal.MensajeError = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("Empleado: " & Empleado & " JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")


        Catch ex As Exception
            retVal.Estado = "N"
            retVal.MensajeError = ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Finally
            If Not oCobro Is Nothing Then
                LiberarObjCOM(CObj(oCobro))
            End If
        End Try

        Return retVal

    End Function

    ''' <summary>
    ''' Funcion para actualizar posiciones de cliente. Campo de usuario en la ficha OCRD
    ''' </summary>
    ''' <param name="Empleado">Empleado logeado</param>
    ''' <param name="strJson">EntCliente completa en formato JSON</param>
    ''' <returns>Numero de registros afectados. Necesario para poner a sincronizado "N" estos campos.</returns>
    ''' <remarks>Puede ocurrir que de error en alguna insercion. en este caso no se bajaria los clientes...</remarks>
    Public Function setLocalizacionDireccionClientes(ByVal Empleado As Integer, ByVal strJson As String) As String
        Dim retVal As String = ""
        Dim iRegs As Integer = 0
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand = Nothing

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim objListaEntDireccionesCliente As List(Of EntClienteDireccion) = JsonConvert.DeserializeObject(Of List(Of EntClienteDireccion))(strJson)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not objListaEntDireccionesCliente Is Nothing Then
                mCon = New clsConexion().OpenConexionSQLSAP()
                ocommand = mCon.CreateCommand()

                For Each objCliDir As EntClienteDireccion In objListaEntDireccionesCliente

                    ocommand.CommandText = "UPDATE CRD1 SET U_SEILat = '" & objCliDir.Latitud & "', U_SEILon = '" & objCliDir.Longitud & "'" &
                    " WHERE CardCode = '" & objCliDir.CardCode & "'" &
                    " AND REPLACE(Address, '''', '') = REPLACE('" & objCliDir.IDDireccion & "', '''', '')"

                    If ocommand.ExecuteNonQuery() = 0 Then
                        Throw New Exception("No se encuentra la direccion " & objCliDir.IDDireccion & " para el cliente " & objCliDir.CardCode)
                    Else
                        iRegs += 1
                    End If

                Next

            End If

            retVal = iRegs.ToString

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            retVal = ex.Message
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    ''' <summary>
    ''' Localizacion de empleados. Necesario tener creada la tabla de localizaciones.
    ''' </summary>
    ''' <param name="Empleado">empleado (logeado)</param>
    ''' <param name="strJson">EntGPSLog en modo JSON</param>
    ''' <returns>Devuelve el numero de registros afectados. Necesariamente mayor que cero para borrar los registros de la tablet para este empleado</returns>
    ''' <remarks>Crear la tabla de localizacion con el script.</remarks>
    Public Function setLocalizacionesEmpleado(ByVal Empleado As Integer, ByVal strJson As String) As String
        Dim retVal As String = ""
        Dim iRegs As Integer = 0
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQL As String = ""

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim objListaPosiciones As List(Of EntGPSLog) = JsonConvert.DeserializeObject(Of List(Of EntGPSLog))(strJson)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not objListaPosiciones Is Nothing Then
                mCon = New clsConexion().OpenConexionSQLSAP()
                ocommand = mCon.CreateCommand()


                For Each objPos As EntGPSLog In objListaPosiciones
                    'No meter latitudes de test que vienen con un 0

                    If objPos.Latitud <> "0" And objPos.Latitud <> "0.0" And objPos.Longitud <> "0" And objPos.Longitud <> "0.0" Then

                        SQL = "INSERT INTO [@TABLETLOCALIZACIONE] ("
                        SQL &= " U_SEILon,"
                        SQL &= " U_SEILat,"
                        SQL &= " U_SEIFecha,"
                        SQL &= " U_SEIHora,"
                        SQL &= " U_SEIEmpleado,"
                        SQL &= " U_SEIProveedor ,"
                        SQL &= " Code,"
                        SQL &= " Name"
                        SQL &= " ) VALUES ( "
                        SQL &= " '" & objPos.Longitud & "', "
                        SQL &= " '" & objPos.Latitud & "', "
                        SQL &= " '" & objPos.Fecha & "', "
                        SQL &= " '" & objPos.Hora & "', "
                        SQL &= " '" & objPos.EmpID & "', "
                        SQL &= " '" & objPos.Proveedor & "', "
                        SQL &= " (SELECT COUNT(code) + 1 FROM [@TABLETLOCALIZACIONE]) , "
                        SQL &= " (SELECT COUNT(code) + 1 FROM [@TABLETLOCALIZACIONE]) )"

                        ocommand.CommandText = SQL
                        ocommand.Parameters.Clear()

                        If ocommand.ExecuteNonQuery() = 0 Then
                            Throw New Exception("La inserción devolvió 0 registros afectados")
                        Else
                            iRegs += 1
                        End If

                    Else
                        iRegs += 1
                    End If

                Next

            End If

            retVal = iRegs.ToString


        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON no valido"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            retVal = ex.Message
            clsLog.Log.Fatal("No se pudo insertar la localizacion de usuario" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    ''' <summary>
    ''' Funcion para dar alta un mensaje nuevo. Recibe el json del mensaje salida.
    ''' </summary>
    ''' <param name="Empleado">Empleado logeado en BD y SAP</param>
    ''' <param name="strJsonMensaje">JSON de entidad mensaje salida</param>
    ''' <param name="oCompany">Company SAP logeada</param>
    ''' <returns>Devuelve un 1 si se ha creado o un string con el error</returns>
    ''' <remarks>En la tablet se borra el mensaje enviado.</remarks>
    Public Function setMensajeSalida(ByVal Empleado As Integer, ByVal strJsonMensaje As String) As String

        Dim retVal As String = ""

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim objMensaje As EntMensajeSalida = JsonConvert.DeserializeObject(Of EntMensajeSalida)(strJsonMensaje)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not objMensaje Is Nothing Then

                retVal = MensajeSalidaSAP(objMensaje.Asunto, objMensaje.Texto, objMensaje.EmpIDOrigen, objMensaje.EmpIDDestino)

            End If


        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonMensaje)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception

            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message

        End Try

        Return retVal

    End Function

    'SETTERS TABLAS DE USUARIO

    Public Function setCatalogos(ByVal Empleado As Integer, ByVal strJsonCab As String, ByVal strJsonLin As String) As String

        Dim retVal As String = "1"
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim ListaCatalogos As List(Of EntCatalogo) = Nothing

        Try

            'Desserializamos los objetos JSON a entidades.
            ListaCatalogos = JsonConvert.DeserializeObject(Of List(Of EntCatalogo))(strJsonCab)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not ListaCatalogos Is Nothing Then
                If ListaCatalogos.Count > 0 Then


                    mCon = New clsConexion().OpenConexionSQLSAP()
                    ocommand = mCon.CreateCommand()
                    ocommand.Parameters.Clear()

                    ocommand.CommandText = " DELETE FROM [@SEICATALOGO] WHERE [U_SEIEmpID] = " & Empleado
                    ocommand.ExecuteNonQuery()


                    For Each objCatalogo In ListaCatalogos

                        If objCatalogo.Borrado = "N" Then

                            Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEICATALOGO]", mCon)

                            ocommand.Parameters.Clear()
                            ocommand.CommandText = "INSERT INTO [@SEICATALOGO] (" &
                            " [Code]," &
                            " [Name]," &
                            " [U_SEIIDCatalogo]," &
                            " [U_SEINombre]," &
                            " [U_SEICardCode]," &
                            " [U_SEICardName], " &
                            " [U_SEIFechaCrea], " &
                            " [U_SEINotas], " &
                            " [U_SEIEmpID]" &
                            " ) VALUES ( " &
                            " @Code, " &
                            " @Name, " &
                            " @IdCatalogo, " &
                            " @Nombre, " &
                            " @CardCode, " &
                            " @CardName, " &
                            " @FechaCreacion, " &
                            " @Notas, " &
                            " @Empleado)"

                            ocommand.Parameters.Clear()
                            ocommand.Parameters.AddWithValue("@Code", NextCode.ToString)
                            ocommand.Parameters.AddWithValue("@Name", NextCode.ToString)
                            ocommand.Parameters.AddWithValue("@IdCatalogo", objCatalogo.IDUnicoCatalogo)
                            ocommand.Parameters.AddWithValue("@Nombre", objCatalogo.NombreCatalogo)
                            ocommand.Parameters.AddWithValue("@CardCode", objCatalogo.CardCode)
                            ocommand.Parameters.AddWithValue("@CardName", objCatalogo.CardName)
                            ocommand.Parameters.AddWithValue("@FechaCreacion", Date.ParseExact(CStr(objCatalogo.FechaCreacion), "yyyyMMdd", CultureInfo.CurrentCulture))
                            ocommand.Parameters.AddWithValue("@Notas", objCatalogo.Notas)
                            ocommand.Parameters.AddWithValue("@Empleado", objCatalogo.EmpID)

                            retVal = ocommand.ExecuteNonQuery().ToString

                        End If

                    Next

                End If

            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException

            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Finally
            If mCon.State = ConnectionState.Open Then
                mCon.Close()
            End If
        End Try


        'Lineas de catalogo

        Try

            'Si al menos ha llegado una cabecera...
            If Not ListaCatalogos Is Nothing Then
                If ListaCatalogos.Count > 0 Then

                    'Desserializamos los objetos JSON a entidades.
                    Dim ListaCatalogosArticulos As List(Of EntCatalogoArticulos) = JsonConvert.DeserializeObject(Of List(Of EntCatalogoArticulos))(strJsonLin)

                    'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
                    If Not ListaCatalogosArticulos Is Nothing Then
                        mCon = New clsConexion().OpenConexionSQLSAP()
                        ocommand = mCon.CreateCommand()
                        ocommand.Parameters.Clear()

                        ocommand.CommandText = " DELETE FROM [@SEICATALOGOARTICULO] WHERE [U_SEIEmpID] = " & Empleado
                        ocommand.ExecuteNonQuery()

                        For Each objCatalogoArt In ListaCatalogosArticulos

                            Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEICATALOGOARTICULO]", mCon)
                            ocommand.Parameters.Clear()

                            ocommand.CommandText = "INSERT INTO [@SEICATALOGOARTICULO] (" &
                            " [Code]," &
                            " [Name]," &
                            " [U_SEIIDCatalogo]," &
                            " [U_SEIIDLinea]," &
                            " [U_SEIItemCode]," &
                            " [U_SEIItemDesc], " &
                            " [U_SEIPosicion], " &
                            " [U_SEIEmpID]" &
                            " ) VALUES ( " &
                            " @Code, " &
                            " @Name, " &
                            " @IdCatalogo, " &
                            " @IdLinea, " &
                            " @ItemCode, " &
                            " @ItemDesc, " &
                            " @Posicion, " &
                            " @Empleado)"

                            ocommand.Parameters.Clear()
                            ocommand.Parameters.AddWithValue("@Code", NextCode)
                            ocommand.Parameters.AddWithValue("@Name", NextCode)
                            ocommand.Parameters.AddWithValue("@IdCatalogo", objCatalogoArt.IDCatalogo)
                            ocommand.Parameters.AddWithValue("@IdLinea", objCatalogoArt.IDLinea)
                            ocommand.Parameters.AddWithValue("@ItemCode", objCatalogoArt.ItemCode)
                            ocommand.Parameters.AddWithValue("@ItemDesc", objCatalogoArt.ItemDesc)
                            ocommand.Parameters.AddWithValue("@Posicion", objCatalogoArt.Posicion)
                            ocommand.Parameters.AddWithValue("@Empleado", Empleado)

                            retVal = ocommand.ExecuteNonQuery().ToString

                        Next

                    End If

                End If

            End If


        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try


        Return retVal.ToString

    End Function

    Public Function setFavoritos(ByVal Empleado As Integer, ByVal strJson As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim listaFavoritos As List(Of EntFavoritos) = JsonConvert.DeserializeObject(Of List(Of EntFavoritos))(strJson)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not listaFavoritos Is Nothing Then
                mCon = New clsConexion().OpenConexionSQLSAP()
                ocommand = mCon.CreateCommand()

                ocommand.Parameters.Clear()

                ocommand.CommandText = " DELETE FROM [@SEIFAVORITOS] WHERE [U_SEIEmpID] = " & Empleado
                ocommand.ExecuteNonQuery()

                For Each objFavoritos In listaFavoritos

                    Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEIFAVORITOS]", mCon)

                    ocommand.Parameters.Clear()
                    ocommand.CommandText = "INSERT INTO [@SEIFAVORITOS] (" &
                    " [Code]," &
                    " [Name]," &
                    " [U_SEIItemCode]," &
                    " [U_SEIEmpID]" &
                    " ) VALUES ( " &
                    " @Code, " &
                    " @Name, " &
                    " @ItemCode, " &
                    " @Empleado)"

                    ocommand.Parameters.Clear()
                    ocommand.Parameters.AddWithValue("@Code", NextCode)
                    ocommand.Parameters.AddWithValue("@Name", NextCode)
                    ocommand.Parameters.AddWithValue("@ItemCode", objFavoritos.ItemCode)
                    ocommand.Parameters.AddWithValue("@Empleado", objFavoritos.EmpID)

                    retVal = ocommand.ExecuteNonQuery().ToString

                Next


            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")
        Finally

            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Public Function setNotas(ByVal Empleado As Integer, ByVal strJson As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim listaNotas As List(Of EntNota) = JsonConvert.DeserializeObject(Of List(Of EntNota))(strJson)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not listaNotas Is Nothing Then
                If listaNotas.Count > 0 Then

                    mCon = New clsConexion().OpenConexionSQLSAP()
                    ocommand = mCon.CreateCommand()
                    ocommand.Parameters.Clear()

                    ocommand.CommandText = " DELETE FROM [@SEINOTAS] WHERE [U_SEIEmpID] = " & Empleado
                    ocommand.ExecuteNonQuery()

                    Dim iRegistrosOK As Integer
                    For Each objNota In listaNotas

                        Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEINOTAS]", mCon)

                        ocommand.Parameters.Clear()
                        ocommand.CommandText = "INSERT INTO [@SEINOTAS] (" &
                        " [Code]," &
                        " [Name]," &
                        " [U_SEIIDNota]," &
                        " [U_SEIEmpID]," &
                        " [U_SEITexto]" &
                        " ) VALUES ( " &
                        " @Code, " &
                        " @Name, " &
                        " @IdNota, " &
                        " @Empleado, " &
                        " @Texto)"

                        ocommand.Parameters.Clear()
                        ocommand.Parameters.AddWithValue("@Code", NextCode)
                        ocommand.Parameters.AddWithValue("@Name", NextCode)
                        ocommand.Parameters.AddWithValue("@IdNota", objNota.Codigo)
                        ocommand.Parameters.AddWithValue("@Empleado", objNota.EmpId)
                        ocommand.Parameters.AddWithValue("@Texto", objNota.Texto)

                        iRegistrosOK += ocommand.ExecuteNonQuery()

                    Next

                    retVal = iRegistrosOK.ToString

                End If
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")
        Finally


            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try
        Return retVal

    End Function

    Public Function setArticulosHistoricosDesactivados(ByVal Empleado As Integer, ByVal strJson As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim listaArticulos As List(Of EntArticuloHistoricoDesactivado) = JsonConvert.DeserializeObject(Of List(Of EntArticuloHistoricoDesactivado))(strJson)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not listaArticulos Is Nothing Then
                If listaArticulos.Count > 0 Then

                    mCon = New clsConexion().OpenConexionSQLSAP()
                    ocommand = mCon.CreateCommand()
                    ocommand.Parameters.Clear()

                    'ocommand.CommandText = " DELETE FROM [@SEINOTAS] WHERE [U_SEIEmpID] = " & Empleado
                    'ocommand.ExecuteNonQuery()

                    Dim iRegistrosOK As Integer
                    For Each objArticulos In listaArticulos

                        Dim NextCode As String = New clsUtil().getNextCodeTabla("[@ARTHISTDESACTIVADO]", mCon)

                        ocommand.Parameters.Clear()


                        If objArticulos.Borrado = "S" Then
                            ocommand.CommandText = " DELETE FROM [@ARTHISTDESACTIVADO] WHERE [U_SEIEmpID] = " & Empleado & " AND [U_SEIItemCode]='" & objArticulos.ItemCode & "' AND [U_SEICardCode]='" & objArticulos.CardCode & "'"
                            ocommand.ExecuteNonQuery()
                        Else
                            ocommand.CommandText = "INSERT INTO [@ARTHISTDESACTIVADO] (" &
                                                    " [Code]," &
                                                    " [Name]," &
                                                    " [U_SEIEmpID]," &
                                                    " [U_SEIItemCode]," &
                                                    " [U_SEICardCode]," &
                                                    " [U_SEIFecha]" &
                                                    " ) VALUES ( " &
                                                    " @Code, " &
                                                    " @Name, " &
                                                    " @EmpID, " &
                                                    " @ItemCode, " &
                                                    " @CardCode, " &
                                                    " @Fecha)"

                            ocommand.Parameters.Clear()
                            ocommand.Parameters.AddWithValue("@Code", NextCode)
                            ocommand.Parameters.AddWithValue("@Name", NextCode)
                            ocommand.Parameters.AddWithValue("@EmpID", objArticulos.EmpID)
                            ocommand.Parameters.AddWithValue("@ItemCode", objArticulos.ItemCode.ToString)
                            ocommand.Parameters.AddWithValue("@CardCode", objArticulos.CardCode.ToString)
                            ocommand.Parameters.AddWithValue("@Fecha", objArticulos.Fecha.ToString)

                            iRegistrosOK += ocommand.ExecuteNonQuery()
                        End If

                    Next

                    retVal = iRegistrosOK.ToString

                End If
            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJson)
            clsLog.Log.Info("End dump JSON")
        Finally


            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try
        Return retVal

    End Function
    ''' <summary>
    ''' Funcion para marcar como leido un mensaje.
    ''' </summary>
    ''' <param name="Empleado">Empleado logeado en BD</param>
    ''' <param name="strJsonMensaje">JSON de entidad mensaje entrada</param>
    ''' <returns>Devuelve vacio o el error. En la tablet no se marca nada.</returns>
    ''' <remarks></remarks>
    Public Function setMensajeLeido(ByVal Empleado As Integer, ByVal strJsonMensaje As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try

            'Desserializamos los objetos JSON a entidades.
            Dim objMensaje As EntMensajeEntrada = JsonConvert.DeserializeObject(Of EntMensajeEntrada)(strJsonMensaje)

            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not objMensaje Is Nothing Then
                mCon = New clsConexion().OpenConexionSQLSAP()
                ocommand = mCon.CreateCommand()


                ocommand.Parameters.Clear()
                ocommand.CommandText = " " &
                " UPDATE OAIB SET WasRead ='Y' WHERE AlertCode = " & objMensaje.Code

                ocommand.ExecuteNonQuery()


            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonMensaje)
            clsLog.Log.Info("End dump JSON")

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonMensaje)
            clsLog.Log.Info("End dump JSON")
        Finally

            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If

        End Try

        Return retVal

    End Function




    'TESORERIA

    ''' <summary>
    ''' Funcion para gestionar tesorería.
    ''' </summary>
    ''' <param name="Empleado">Empleado logeado en BD</param>
    ''' <param name="strJsonMensaje">JSON de entidad mensaje entrada</param>
    ''' <returns>Devuelve vacio o el error. En la tablet no se marca nada.</returns>
    ''' <remarks></remarks>
    Public Function setTesoreria(ByVal Empleado As Integer, ByVal strJsonCab As String, ByVal strJsonLin As String) As String


        Dim retVal As String = ""
        Dim oConex As New clsConexion
        Dim Destinatarios As String = ""
        Dim IDTesoreriaGenerado As Long = 0


        Try

            'Desserializamos los objetos JSON a entidades.
            Dim CabeceraIngreso As EntTesoreriaCab = JsonConvert.DeserializeObject(Of EntTesoreriaCab)(strJsonCab)
            Dim ListaIngresos As List(Of EntTesoreriaLin) = JsonConvert.DeserializeObject(Of List(Of EntTesoreriaLin))(strJsonLin)


            'Si el objeto entidad se ha creado desde el JSON entramos. Si no es Nothing.
            If Not ListaIngresos Is Nothing Then


                'Insertamos en las tablas de TELYNET las tesorerias y actualizamos el plazo en SAP.
                IDTesoreriaGenerado = TESORERIA_TLY_INSERT_TESORERIA(CabeceraIngreso, ListaIngresos, Empleado)

                'Si todo ha ido bien, el ID generado sera mayor que 0
                If IDTesoreriaGenerado > 0 Then

                    'Envio de correo
                    Dim oMail As New clsMail
                    oMail.pASUNTO = "INGRESO TESORERIA (" & getNombreDeEmpleado(CabeceraIngreso.EmpID).ToUpper & ")"

                    oMail.pCUERPO = ""
                    oMail.pCUERPO &= "ID Ingreso: " & IDTesoreriaGenerado
                    oMail.pCUERPO &= "Fecha: " & Date.ParseExact(CabeceraIngreso.FechaIngreso.ToString, "yyyyMMdd", Globalization.CultureInfo.InvariantCulture) 'CabeceraIngreso.FechaIngreso.ToString
                    oMail.pCUERPO &= "Importe: " & CabeceraIngreso.ImporteTotal

                    If CabeceraIngreso.MedioIngreso = "E" Then
                        oMail.pCUERPO &= "Concepto: Metálico"
                    ElseIf CabeceraIngreso.MedioIngreso = "T" Then
                        oMail.pCUERPO &= "Concepto: Talón/Pagaré"
                    Else
                        oMail.pCUERPO &= "Concepto: sin clasificar"
                    End If

                    oMail.pCUERPO &= "Banco: " & CabeceraIngreso.NombreBanco & " - " & CabeceraIngreso.NumCuenta
                    oMail.pCUERPO &= " "
                    oMail.pCUERPO &= "-- Desglose--"

                    'REcorremos lineas de ingreso.
                    For Each LinIngreso As EntTesoreriaLin In ListaIngresos

                        'Seguimos con el mail
                        oMail.pCUERPO &= "Factura: " & LinIngreso.DocNumFra
                        oMail.pCUERPO &= "Documento: " & LinIngreso.DocEntryFra
                        oMail.pCUERPO &= "Cliente: " & LinIngreso.ClienteCode & " - " & LinIngreso.ClienteName
                        oMail.pCUERPO &= "Importe: " & LinIngreso.ImporteCobrado & " EUR."
                        oMail.pCUERPO &= "Resto: " & LinIngreso.ImporteRestante

                        If CabeceraIngreso.MedioIngreso = "T" Then
                            oMail.pCUERPO &= "Número de talón/pagaré: " & LinIngreso.NumTalon
                            oMail.pCUERPO &= "Fecha talón/pagaré: " & Date.ParseExact(LinIngreso.FechaVtoTalon.ToString, "yyyyMMdd", Globalization.CultureInfo.InvariantCulture) ' LinIngreso.FechaVtoTalon
                        End If

                        oMail.pCUERPO &= "-----------"

                    Next

                    'destinatarios de correo.
                    Dim oUtil As New clsUtil
                    Destinatarios = oUtil.getCorreosEmpleado(Empleado)

                    'Si hay destinatarios.
                    If Destinatarios <> "" Then
                        oMail.SendMail(Destinatarios)
                    Else
                        clsLog.Log.Error("No se han encontrado destinatarios de correo en Mail de tesoreria")
                    End If

                    'Devolución numerica si ha ido bien.
                    retVal = IDTesoreriaGenerado.ToString

                Else

                    Throw New Exception("No se ha insertado ninguna tesoreria en TLY. Ha ocurrido un error. No se ha enviado correo.")

                End If

            End If

        Catch ex As Newtonsoft.Json.JsonSerializationException
            retVal = "JSON incorrecto. Avise al administrador"
            clsLog.Log.Fatal("JSON incorrecto. Avise al administrador" & " en " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            'Si se ha insertado la tesoreria, debe devolver el numero insertado.
            If IDTesoreriaGenerado > 0 Then
                retVal = IDTesoreriaGenerado.ToString
            Else
                'Si no, el mensaje de error
                retVal = ex.Message
            End If

            clsLog.Log.Info("Dump JSON")
            clsLog.Log.Info(strJsonCab)
            clsLog.Log.Info(strJsonLin)
            clsLog.Log.Info("End dump JSON")
            Return retVal
        Finally

        End Try

        Return retVal.ToString

    End Function

    Private Function TESORERIA_TLY_INSERT_TESORERIA(ByVal Cab As EntTesoreriaCab, ByVal Lin As List(Of EntTesoreriaLin), ByVal Empleado As Integer) As Long

        Dim retVal As Long = 0
        Dim mConTelynet As SqlConnection = Nothing
        Dim mConSAP As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim sSQL As String = ""

        Try


            'Solo si es efectivo
            If Cab.MedioIngreso = "E" Then

                mConTelynet = New clsConexion().OpenConexionSQLTELYNET
                mConSAP = New clsConexion().OpenConexionSQLSAP

                Dim CabeceraIDTesoreria_TLY As Long
                CabeceraIDTesoreria_TLY = TESORERIA_TLY_ObtenerSiguienteIDTesoreriaUsuario_Cabecera(Empleado)

                Dim SlpCode As Integer = getComercial_de_Empleado(Empleado)

                Dim blnExisteTesoreria As Boolean = TESORERIA_TLY_ComprobarExisteRegistro(Cab, Empleado)
                'Si se devuelve ID nuevo y no existe ya la tesoreria.
                If CabeceraIDTesoreria_TLY > 0 And blnExisteTesoreria = False Then
                    With Cab

                        'CABECERAS
                        sSQL = " INSERT INTO TESORERIA_TLY "
                        sSQL &= "([ID]"
                        sSQL &= ",[Create_Date]"
                        sSQL &= ",[Create_User]"
                        sSQL &= ",[Modify_Date]"
                        sSQL &= ",[Modify_User]"
                        sSQL &= ",[status_mobile]"
                        sSQL &= ",[Concepto]"
                        sSQL &= ",[Cant_Ingresada]"
                        sSQL &= ",[Fecha_Ingreso]"
                        sSQL &= ",[TipoDocumento]"
                        sSQL &= ",[Factura]"
                        sSQL &= ",[NumDocumento]"
                        sSQL &= ",[Code_Account]"
                        sSQL &= ",[Code_Agent]"
                        sSQL &= ",[Revisado])"
                        sSQL &= " VALUES "
                        sSQL &= "(" & CabeceraIDTesoreria_TLY
                        sSQL &= ",'" & .FechaIngreso & "'"
                        sSQL &= ",'" & SlpCode & "'"
                        sSQL &= ",'" & .FechaIngreso & "'"
                        sSQL &= ",'" & SlpCode & "'"
                        sSQL &= ",'10' "
                        sSQL &= ",'1'"
                        sSQL &= ",'" & .ImporteTotal.ToString.Replace(",", ".") & "'"
                        sSQL &= ",'" & .FechaIngreso & "'"
                        sSQL &= ",NULL"
                        sSQL &= ",NULL"
                        sSQL &= ",NULL"
                        sSQL &= ",'" & getCuentaContable_de_Banco_Exception(Cab.IDBanco) & "'"
                        sSQL &= ",'" & SlpCode & "'"
                        sSQL &= ",NULL)"

                    End With

                    ocommand = mConTelynet.CreateCommand()
                    ocommand.CommandText = sSQL
                    ocommand.Parameters.Clear()
                    If ocommand.ExecuteNonQuery = 0 Then
                        Throw New Exception("No se ha insertado cabecera de tesoreria en TELYNET")
                    Else
                        clsLog.Log.Info("Insertada cabecera de tesoreria numero: " & CabeceraIDTesoreria_TLY)
                    End If


                    'LINEAS
                    For Each LinIngreso As EntTesoreriaLin In Lin

                        Dim LineaIDTesoreria_TLY As Long
                        LineaIDTesoreria_TLY = TESORERIA_TLY_ObtenerSiguienteIDTesoreriaUsuario_Linea(Empleado)

                        With LinIngreso

                            sSQL = " INSERT INTO TESORERIADET_TLY "
                            sSQL &= "([ID]"
                            sSQL &= ",[Create_Date]"
                            sSQL &= ",[Create_User]"
                            sSQL &= ",[Modify_Date]"
                            sSQL &= ",[Modify_User]"
                            sSQL &= ",[status_mobile]"
                            sSQL &= ",[Id_Tesoreria]"
                            sSQL &= ",[Factura]"
                            sSQL &= ",[NumDocumento]"
                            sSQL &= ",[IdPlazo]"
                            sSQL &= ",[ImportePlazo]"
                            sSQL &= ",[Revisado]"
                            sSQL &= ",[Cobrado]"
                            sSQL &= ",[TipoDocumento]"
                            sSQL &= ",[Talon]"
                            sSQL &= ",[Metalico])"
                            sSQL &= " VALUES "
                            sSQL &= "(" & LineaIDTesoreria_TLY
                            sSQL &= ",'" & Cab.FechaIngreso & "'"
                            sSQL &= ",'" & SlpCode & "'"
                            sSQL &= ",'" & Cab.FechaIngreso & "'"
                            sSQL &= ",'" & SlpCode & "'"
                            sSQL &= ",'10' "
                            sSQL &= ",'" & CabeceraIDTesoreria_TLY & "'"
                            sSQL &= ",'" & .DocNumFra & "'"
                            sSQL &= ",'" & .DocEntryFra & "'"
                            sSQL &= ",'" & .IDVto & "'"
                            sSQL &= ",'" & (.ImporteCobrado + .ImporteRestante).ToString.Replace(",", ".") & "'"
                            sSQL &= ",NULL"
                            sSQL &= ",'" & .ImporteCobrado.ToString.Replace(",", ".") & "'"
                            sSQL &= CStr(IIf(.TipoDoc = "F", ",'F'", ",'A'"))
                            sSQL &= ",'" & .NumTalon & "'"

                            If Cab.MedioIngreso = "E" Then
                                sSQL &= ",'S'"
                            Else
                                sSQL &= ",'N'"
                            End If

                            sSQL &= ")"

                            ocommand = mConTelynet.CreateCommand()
                            ocommand.CommandText = sSQL
                            ocommand.Parameters.Clear()
                            If ocommand.ExecuteNonQuery = 0 Then
                                Throw New Exception("No se ha insertado linea de tesoreria en TELYNET")
                            Else
                                clsLog.Log.Info("Insertada linea de tesoreria numero: " & LineaIDTesoreria_TLY)
                            End If



                            Dim sTabla As String = ""
                            If .ImporteCobrado >= 0 Then
                                sTabla = "INV6" ' Facturas
                            Else
                                sTabla = "RIN6" ' Abonos
                            End If

                            sSQL = " UPDATE " & sTabla & " SET  "
                            sSQL &= " U_SEIpdape = 'Y'"  ' Pendiente de gestionar por CBG y cobrado por PDA
                            sSQL &= " WHERE DocEntry = '" & .DocEntryFra & "'"
                            sSQL &= " AND InstlmntID = '" & .IDVto & "'"

                            ocommand = mConSAP.CreateCommand()
                            ocommand.CommandText = sSQL
                            ocommand.ExecuteNonQuery()


                            'Lo quitamos de la tabla de cobros parciales.
                            sSQL = " DELETE FROM [@COBROSPARCIALES] WHERE 1=1 "
                            sSQL &= " AND U_SEIDocEntry = '" & .DocEntryFra & "'"
                            sSQL &= " AND U_SEIVto = '" & .IDVto & "'"

                            ocommand = mConSAP.CreateCommand()
                            ocommand.CommandText = sSQL
                            ocommand.ExecuteNonQuery()

                        End With
                    Next


                    'Si todo va bien, el valor de retorno es el ID de cabecera
                    retVal = CabeceraIDTesoreria_TLY
                ElseIf CabeceraIDTesoreria_TLY > 0 AndAlso blnExisteTesoreria Then
                    retVal = CabeceraIDTesoreria_TLY
                End If

            Else

                mConTelynet = New clsConexion().OpenConexionSQLTELYNET
                mConSAP = New clsConexion().OpenConexionSQLSAP

                'Si es talon...
                'Es correcto. Lo eliminamos de la temporal
                For Each LinIngreso As EntTesoreriaLin In Lin

                    With LinIngreso


                        'Lo quitamos de la tabla de cobros parciales.
                        sSQL = " DELETE FROM [@COBROSPARCIALES] WHERE 1=1 "
                        sSQL &= " AND U_SEIDocEntry = '" & .DocEntryFra & "'"
                        sSQL &= " AND U_SEIVto = '" & .IDVto & "'"
                        sSQL &= " AND U_SEIMedioCobro = 'T'"

                        ocommand = mConSAP.CreateCommand()
                        ocommand.CommandText = sSQL
                        ocommand.ExecuteNonQuery()

                    End With

                Next

                'Marcamos como correcta. Hay que devolver numerico > 0
                retVal = 1

            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

            If Not IsNothing(mConSAP) Then
                If mConSAP.State = ConnectionState.Open Then
                    mConSAP.Close()
                End If
            End If

            If Not IsNothing(mConTelynet) Then
                If mConTelynet.State = ConnectionState.Open Then
                    mConTelynet.Close()
                End If
            End If

        End Try

        Return retVal

    End Function

    'Numeradores de linea y cabecera 

    Private Function TESORERIA_TLY_ObtenerSiguienteIDTesoreriaUsuario_Cabecera(ByVal Empleado As Integer) As Long
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQL As String = ""
        Dim retVal As Long = 0

        Try

            'Conexion BBDD Telynet
            mCon = New clsConexion().OpenConexionSQLSAP
            ocommand = mCon.CreateCommand()

            'Máximo numero de cabecera
            SQL = "  SELECT MAX(VISTA.Numero) + 1 FROM ("
            SQL &= " SELECT ISNULL((select MAX(ID) FROM MOV_TLYERP.dbo.TESORERIA_TLY WHERE CAST(ID as varchar) LIKE RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '%'), RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '000') as Numero "
            SQL &= " UNION ALL "
            SQL &= " SELECT ISNULL((select MAX(U_SEItesor) from ORCT WHERE CAST(U_SEItesor as varchar) LIKE RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '%'), RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '000') as Numero "
            SQL &= " ) AS VISTA "

            ocommand.CommandText = SQL
            ocommand.Parameters.Clear()

            Dim oObj As Object = ocommand.ExecuteScalar
            If Not oObj Is Nothing Then
                retVal = CLng(ocommand.ExecuteScalar)
            Else
                Throw New Exception("Command = Nothing")
            End If




        Catch ex As Exception
            clsLog.Log.Fatal("Ocurrió un problema al obtener el siguiente numero de tesoreria. " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Fatal("--SQL --" & SQL & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Fatal("--Emp --" & Empleado & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function TESORERIA_TLY_ObtenerSiguienteIDTesoreriaUsuario_Linea(ByVal Empleado As Integer) As Long
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQL As String = ""
        Dim retVal As Long = 0

        Try

            'Conexion BBDD Telynet
            mCon = New clsConexion().OpenConexionSQLSAP
            ocommand = mCon.CreateCommand()

            'Máximo numero de cabecera
            SQL = "  SELECT MAX(VISTA.Numero) + 1 FROM ("
            SQL &= " SELECT ISNULL((select MAX(ID) FROM MOV_TLYERP.dbo.TESORERIADET_TLY WHERE CAST(ID as varchar) LIKE RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '%'), RIGHT(CAST(YEAR(GETDATE()) as varchar),2) + RIGHT('000' + '" & Empleado & "',3) + '00000') as Numero "
            SQL &= " ) AS VISTA "

            ocommand.CommandText = SQL
            ocommand.Parameters.Clear()

            Dim oObj As Object = ocommand.ExecuteScalar
            If Not oObj Is Nothing Then
                retVal = CLng(ocommand.ExecuteScalar)
            Else
                Throw New Exception("Command = Nothing")
            End If




        Catch ex As Exception
            clsLog.Log.Fatal("Ocurrió un problema al obtener el siguiente numero de tesoreria. " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Fatal("--SQL --" & SQL & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Fatal("--Emp --" & Empleado & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function TESORERIA_TLY_ComprobarExisteRegistro(ByVal Cab As EntTesoreriaCab, ByVal Empleado As Integer) As Boolean
        Dim retVal As Boolean = False
        Dim mConTelynet As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim sSQL As String = ""

        Try

            'Solo si es efectivo
            mConTelynet = New clsConexion().OpenConexionSQLTELYNET
            Dim SlpCode As Integer = getComercial_de_Empleado(Empleado)

            With Cab

                'CABECERAS
                sSQL = " SELECT COUNT(*) FROM TESORERIA_TLY "
                sSQL &= " WHERE 1 = 1"
                sSQL &= " AND Create_Date = '" & .FechaIngreso & "'"
                sSQL &= " AND Create_User = '" & SlpCode & "'"
                sSQL &= " AND Modify_Date = '" & .FechaIngreso & "'"
                sSQL &= " AND Modify_User = '" & SlpCode & "'"
                sSQL &= " AND Cant_Ingresada = '" & .ImporteTotal.ToString.Replace(",", ".") & "'"
                sSQL &= " AND Fecha_Ingreso = '" & .FechaIngreso & "'"
                sSQL &= " AND Code_Account = '" & getCuentaContable_de_Banco_Exception(Cab.IDBanco) & "'"
                sSQL &= " AND Code_Agent = '" & SlpCode & "'"

            End With

            ocommand = mConTelynet.CreateCommand()
            ocommand.CommandText = sSQL
            ocommand.Parameters.Clear()

            Dim oObj As Object = ocommand.ExecuteScalar
            If Not oObj Is Nothing Then
                If CInt(oObj) > 0 Then
                    retVal = True
                    clsLog.Log.Warn("Ya existe la tesoreria!")

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mConTelynet) Then
                If mConTelynet.State = ConnectionState.Open Then
                    mConTelynet.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

#Region "PRIVADAS"


    'UTILES PEDIDOS

    Private Sub GuardarLineasPedidoNuevas(ByVal ListaLineasNuevas As List(Of EntDocumentoLin), ByVal DocEntry As Long, ByVal DocNum As String, ByVal EmpID As Integer)

        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try
            If ListaLineasNuevas.Count > 0 Then

                mCon = New clsConexion().OpenConexionSQLSAP()
                ocommand = mCon.CreateCommand()

                For Each objLinea As EntDocumentoLin In ListaLineasNuevas

                    Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEIESTADISTICASPED]", mCon)

                    ocommand.Parameters.Clear()
                    ocommand.CommandText = "INSERT INTO [@SEIESTADISTICASPED] (" &
                    " [Code]," &
                    " [Name]," &
                    " [U_SEICodArt]," &
                    " [U_SEIUnds]," &
                    " [U_SEICajas]," &
                    " [U_SEIDocEnt], " &
                    " [U_SEIDocNum], " &
                    " [U_SEIEmpID]" &
                    " ) VALUES ( " &
                    " @Code, " &
                    " @Name, " &
                    " @CodArt, " &
                    " @Unds, " &
                    " @Cajas, " &
                    " @DocEnt, " &
                    " @DocEnt, " &
                    " @EmpID)"

                    ocommand.Parameters.Clear()
                    ocommand.Parameters.AddWithValue("@Code", NextCode.ToString)
                    ocommand.Parameters.AddWithValue("@Name", NextCode.ToString)
                    ocommand.Parameters.AddWithValue("@CodArt", objLinea.ItemCode)
                    ocommand.Parameters.AddWithValue("@Unds", objLinea.CantidadReal)
                    ocommand.Parameters.AddWithValue("@Cajas", objLinea.CantidadFactor)
                    ocommand.Parameters.AddWithValue("@DocEnt", DocEntry)
                    ocommand.Parameters.AddWithValue("@DocNum", DocNum)
                    ocommand.Parameters.AddWithValue("@EmpID", EmpID)

                    ocommand.ExecuteNonQuery().ToString()

                Next

            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)

        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub getDocNum_DocEntry_PostCongelado(ByRef DocEntry As Long, ByRef DocNum As String, ByVal objPedido As EntDocumentoCab, ByVal Empleado As Integer)

        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion
        Dim SQL As String = ""

        Try
            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()

            SQL = " SELECT TOP 1 isnull(DocEntry,0) as DocEntry from ORDR WHERE CardCode = '" & objPedido.CardCode & "' AND SlpCode = '" & getComercial_de_Empleado(Empleado) & "' ORDER BY DocEntry DESC "
            oCommand.CommandText = SQL
            Dim oObjEntry As Object = oCommand.ExecuteScalar()

            If Not oObjEntry Is Nothing Then
                DocEntry = CLng(oObjEntry.ToString)
            End If


            SQL = " SELECT DocNum from ORDR WHERE DocEntry = " & DocEntry
            oCommand.CommandText = SQL

            Dim oObjNum As Object = oCommand.ExecuteScalar()
            If Not oObjNum Is Nothing Then
                DocNum = oCommand.ToString
            End If

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Sub

    Private Function getOrdenRutaCliente(ByVal Cliente As String) As Integer

        Dim retVal As Integer = 0
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion

        Dim SQL As String = ""
        Try

            SQL = "SELECT ISNULL(U_SEIorden, 0) from OCRD where CardCode = '" & Cliente & "'"

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString)
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & Sql & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal


    End Function

    Private Function getIDDireccionRealCliente(ByVal CardCode As String, ByVal IDDireccionSinComilla As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion
        Dim SQL As String = ""
        Try

            'Del tablet la direccion viene sin comilla simple. Hay que sacar la direccion real.

            SQL = "SELECT ISNULL(Address,'') as Direccion from CRD1 where CardCode='" & CardCode & "' AND REPLACE(Address, '''','') = '" & IDDireccionSinComilla & "'"

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            Dim oObj As Object = oCommand.ExecuteScalar()

            If Not oObj Is Nothing Then
                retVal = oObj.ToString
            End If

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & Sql & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal


    End Function

    Private Function getSlpCode(ByVal Empleado As Integer) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""

        Try
            SQL = ""
            SQL &= "SELECT " &
                    "T0.salesPrson " &
                    "FROM OHEM T0 " &
                    "WHERE empID = " & Empleado

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Almacen", "Almacen")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


    'traer maximo cardcode
    Private Function getCardCodeNumMax() As Integer
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Dim numMax As Integer = -1

        Try
            SQL = ""
            SQL &= "SELECT TOP 1 CardCode FROM OCRD WHERE CardCode LIKE 'C%' ORDER BY CardCode DESC"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "CARDCODEMAX", "CARDCODEMAX")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If

            numMax = CInt(retVal.Replace("C", "")) + 1

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return numMax

    End Function


    Private Function getCuentaEfectivo(ByVal Empleado As Integer) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try
            Dim slpCode As String
            slpCode = getSlpCode(Empleado)
            SQL = ""
            SQL &= "SELECT U_SEICuentaCaja FROM [@SEIEMPLE] WHERE U_SEIslppa = " & slpCode

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Cuenta_Caja", "Cuenta_Caja")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If
        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal
    End Function

    Private Function getDocEntryFactura(ByVal DocNum As Integer) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try
            SQL = "SELECT DocEntry from OINV WHERE DocNum = " & DocNum

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "DonEntry", "DocEntry")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try
        Return retVal
    End Function

    Private Function getImuestoItem(ByVal ItemCode As String) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try

            SQL = ""
            SQL &= "SELECT ISNULL(T0.U_SEIImpuesto, 'EX') FROM OITM T0 WHERE T0.ItemCode = '" & ItemCode & "' "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "CodigoImpuesto", "CodigoImpuesto")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If


        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function RevisarGestionadoPorLotes(ByVal ItemCode As String) As Boolean

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""

        Try

            SQL = ""
            SQL &= "SELECT T0.ManBtchNum AS 'GestionadoPorLotes' FROM OITM T0 WHERE T0.ItemCode = '" & ItemCode & "'"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Es_Lote", "Es_Lote")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        If DR.Item("GestionadoPorLotes").ToString = "N" Then
                            clsLog.Log.Info(ItemCode & "No es por lotes")
                            Return False
                        End If
                    Next
                End If
            End If
            clsLog.Log.Info(ItemCode & " si es por lotes")
            Return True

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Function

    Public Function getLotes(ByVal slpCode As Integer, ByVal ItemCode As String) As List(Of EntLote)
        Dim oLista As New List(Of EntLote)
        Try
            Dim SQL As String = ""
            SQL &= "SELECT T0.WhsCode AS 'Almacen' , T0.ItemCode AS 'CodigoItem', "
            SQL &= "T0.Quantity As 'Cantidad', T1.DistNumber AS 'Lote', T1.CreateDate AS 'FechaCreacion' FROM OBTQ T0 "
            SQL &= "INNER JOIN OBTN T1 ON T0.ItemCode = T1.ItemCode and T0.SysNumber = T1.SysNumber "
            SQL &= "WHERE T0.ItemCode = '" & ItemCode & "' AND T0.Quantity != 0 AND T0.WhsCode = '" & getAlmacenCliente(slpCode) & "' ORDER BY T1.CreateDate ASC"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_LOTES", "TAB_LOTES")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntLote
                        With oEntidad
                            .Almacen = linea.Item("Almacen").ToString
                            .Cantidad = CDbl(linea.Item("Cantidad").ToString)
                            .CodigoItem = linea.Item("CodigoItem").ToString
                            .FechaCreacion = CDate(linea.Item("FechaCreacion").ToString)
                            .Lote = linea.Item("Lote").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    Public Function getLotesNotaCredito(ByVal DocEntry As Integer, ByVal ItemCode As String) As List(Of EntLote)
        Dim oLista As New List(Of EntLote)
        Try
            Dim SQL As String = ""
            SQL &= "Select "
            SQL &= "T1.Quantity * -1 As 'CantidadAsignada', "
            SQL &= "T2.Quantity AS 'CantidadDisponible', "
            SQL &= "T2.WhsCode As 'Almacen', "
            SQL &= "T2.ItemCode AS 'CodigoItem', "
            SQL &= "T3.DistNumber As 'Lote', "
            SQL &= "T3.CreateDate AS 'FechaCreacion', "
            SQL &= "T0.DocLine As 'LineaDocumento', "
            SQL &= "T0.DocEntry AS 'DocEntry' "
            SQL &= "From OITL T0 "
            SQL &= "INNER JOIN ITL1 T1 "
            SQL &= "On T1.LogEntry = T0.LogEntry "
            SQL &= "INNER Join OBTQ T2 "
            SQL &= "On T2.ItemCode = T0.ItemCode And T1.SysNumber = T2.SysNumber "
            SQL &= "INNER Join OBTN T3 "
            SQL &= "On T3.ItemCode = T2.ItemCode And T1.SysNumber = T3.SysNumber "
            SQL &= "WHERE T1.ItemCode = '" & ItemCode & "' "
            SQL &= " And T0.DocEntry = '" & DocEntry & "' "
            SQL &= "ORDER BY T3.CreateDate ASC "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_LOTES_NOTA_CREDITO", "TAB_LOTES_NOTA_CREDITO")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntLote
                        With oEntidad
                            .Almacen = linea.Item("Almacen").ToString
                            .Cantidad = CDbl(linea.Item("CantidadDisponible").ToString)
                            .CantidadAsignada = CDbl(linea.Item("CantidadAsignada").ToString)
                            .CodigoItem = linea.Item("CodigoItem").ToString
                            .FechaCreacion = CDate(linea.Item("FechaCreacion").ToString)
                            .Lote = linea.Item("Lote").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Private Function getAlmacenCliente(ByVal sComercial As Integer) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try
            '
            SQL = ""
            SQL &= " Select ISNULL(U_SEIalmac,'')"
            SQL &= " FROM  OSLP T0"
            SQL &= " WHERE T0.SlpCode =" & sComercial

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Almacen", "Almacen")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows
                        retVal = DR.Item(0).ToString
                    Next
                End If
            End If


        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function



    Private Function ObtenerSerieIC(ByVal CardCode As String) As Integer
        Dim retVal As Integer = 0
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion
        Dim SQL As String = ""

        Try

            SQL = "Select ISNULL(T1.U_SEICODI,0) from OCRD T0 INNER JOIN [@SEICATEGORIA] T1 On T0.U_SEICateg = T1.Code WHERE T0.CardCode = '" & CardCode & "'"

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString)
            oCommand = Nothing


        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    'UTILES PEDIDOS - IVA

    Public Function AsignarGrupoImpuesto_LineaV(ByRef oDoc As SAPbobsCOM.Documents,
                                                 ByVal sTipoImpuesto As String) As Boolean

        Try


            Dim sIndicador As String = ""  ' Indicador de Impuestos
            Dim sArticulo As String = ""
            Dim sMsj As String = ""
            AsignarGrupoImpuesto_LineaV = True
            '
            'En cas que no tingui per defecte la línea que toca, passar també nLin i
            'fer SetCurrenLine

            sArticulo = oDoc.Lines.ItemCode
            '
            If sTipoImpuesto = c_UE Then
                sIndicador = ObtenerUE_Ventas(sArticulo)
                sMsj = ("Línea Nº: " & oDoc.Lines.LineNum.ToString & " el Artículo: " & sArticulo & " no tiene asignado un grupo de Impuestos UE valido")
            End If
            '
            If sTipoImpuesto = c_IGIC Then
                sIndicador = ObtenerIGIC_Ventas(sArticulo)
                sMsj = ("Línea Nº: " & oDoc.Lines.LineNum.ToString & " el Artículo: " & sArticulo & " no tiene asignado un grupo de Impuestos IGIC valido")
            End If
            '
            If sTipoImpuesto = c_REC Then
                sIndicador = ObtenerREC_Ventas(sArticulo)
                sMsj = ("Línea Nº: " & oDoc.Lines.LineNum.ToString & " el Artículo: " & sArticulo & " no tiene asignado un grupo de Impuesto Compensatorio valido")
            End If
            '
            If sIndicador = "" Then
                AsignarGrupoImpuesto_LineaV = False
                Throw New Exception(sMsj)
                Exit Function
                '
            End If
            '
            oDoc.Lines.VatGroup = sIndicador

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        End Try

    End Function

    Public Function ObtenerUE_Ventas(ByVal sArticulo As String) As String
        '
        Dim ls As String
        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        '
        ObtenerUE_Ventas = ""
        '
        ls = ""
        ls = ls & " SELECT T0.U_SEIUEV"
        ls = ls & " FROM OITM T0 INNER JOIN OVTG T1 "
        ls = ls & " ON T0.U_SEIUEV=T1.Code"
        ls = ls & " WHERE T0.Itemcode='" & sArticulo & "'"
        ls = ls & " AND   T1.Category='O'"                 ' Iva Repercutido Ventas
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        ' El artículo tiene que tener asignado un grupo de iva
        ' en el campo de usuario U_SEIUEV
        ' y el grupo asignado tiene que existir
        '
        If Not oRcs.EoF Then
            ObtenerUE_Ventas = oRcs.Fields.Item("U_SEIUEV").Value.ToString
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function

    Public Function ObtenerIGIC_Ventas(ByVal sArticulo As String) As String
        '
        Dim ls As String
        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        '
        ObtenerIGIC_Ventas = ""
        '
        ls = ""
        ls = ls & " SELECT T0.U_SEIIGICV"
        ls = ls & " FROM OITM T0 INNER JOIN OVTG T1 "
        ls = ls & " ON T0.U_SEIIGICV=T1.Code"
        ls = ls & " WHERE T0.Itemcode='" & sArticulo & "'"
        ls = ls & " AND   T1.Category='O'"                 ' Iva Repercutido
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        ' El artículo tiene que tener asignado un grupo de iva
        ' en el campo de usuario U_SEIIGICV
        ' y el grupo asignado tiene que existir
        '
        If Not oRcs.EoF Then
            ObtenerIGIC_Ventas = oRcs.Fields.Item("U_SEIIGICV").Value.ToString
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function
    '
    Public Function ObtenerREC_Ventas(ByVal sArticulo As String) As String
        '
        ' Recargo de Equivalencia o Impuesto Compensatorio
        Dim ls As String
        Dim oRcs As SAPbobsCOM.Recordset
        '
        ObtenerREC_Ventas = ""
        '
        ls = ""
        ls = ls & " SELECT T0.U_SEIRECEV"
        ls = ls & " FROM OITM T0 INNER JOIN OVTG T1 "
        ls = ls & " ON T0.U_SEIRECEV=T1.Code"
        ls = ls & " WHERE T0.Itemcode='" & sArticulo & "'"
        ls = ls & " AND   T1.Category='O'"                 ' Iva Repercutido (Ventas)
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        ' El artículo tiene que tener asignado un grupo de iva
        ' en el campo de usuario U_SEIRECEV
        ' y el grupo asignado tiene que existir
        '
        If Not oRcs.EoF Then
            ObtenerREC_Ventas = oRcs.Fields.Item("U_SEIRECEV").Value.ToString
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function

    Public Function CambiarGrupoImpuesto(ByVal sCardCode As String, ByRef sTipoImpuesto As String) As Boolean
        '
        Dim ls As String
        Dim oRcs As SAPbobsCOM.Recordset
        '
        CambiarGrupoImpuesto = False
        '
        ls = ""
        ls = ls & " SELECT U_SEIIGIC, EQU"
        ls = ls & " FROM OCRD "
        ls = ls & " WHERE CardCode ='" & sCardCode & "'"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EoF Then
            ' UE (Unión Europea)
            If oRcs.Fields.Item("U_SEIIGIC").Value.ToString = "UE" Then
                sTipoImpuesto = c_UE
                CambiarGrupoImpuesto = True
            Else
                ' IGIC y Recargo Equivalencia
                If oRcs.Fields.Item("U_SEIIGIC").Value.ToString = "IGIC" And oRcs.Fields.Item("EQU").Value.ToString = "Y" Then
                    sTipoImpuesto = c_IGIC
                    CambiarGrupoImpuesto = True
                End If
                ' IGIC y NO Recargo Equivalencia
                If oRcs.Fields.Item("U_SEIIGIC").Value.ToString = "IGIC" And oRcs.Fields.Item("EQU").Value.ToString = "N" Then
                    sTipoImpuesto = c_IGIC
                    CambiarGrupoImpuesto = True
                End If
                ' IVA y Recargo Equivalencia
                If oRcs.Fields.Item("U_SEIIGIC").Value.ToString = "IVA" And oRcs.Fields.Item("EQU").Value.ToString = "Y" Then
                    sTipoImpuesto = c_REC
                    CambiarGrupoImpuesto = True
                End If
                ' IVA y NO Recargo Equivalencia
                If oRcs.Fields.Item("U_SEIIGIC").Value.ToString = "IVA" And oRcs.Fields.Item("EQU").Value.ToString = "N" Then
                    sTipoImpuesto = c_IVA
                    CambiarGrupoImpuesto = False
                End If
            End If
            '
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function

    'UTILES PEDIDOS - PORTES

    Public Sub PortesCliente(ByVal lDocEntry As Integer)
        '
        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        Dim ls As String
        Dim sCliente As String
        Dim slpCode As Integer
        Dim sDocDueDate As String
        Dim sArticuloPortes As String
        Dim dImportePedidos As Double
        Dim dImporteMinimo As Double
        Dim lNumPortes As Long
        Dim lPropiedad As Long
        Dim dImportePropiedad As Double
        Dim sAlmacenCliente As String
        Dim lPropiedadSinPortes As Long
        Dim sAnd As String
        '
        ObtenerPropiedad(lPropiedad, dImportePropiedad)
        lPropiedadSinPortes = Convert.ToInt64(getConstanteNumero("NOPORTES")) ' NO cobrar Portes
        '
        If (lPropiedadSinPortes >= 1 And lPropiedadSinPortes <= 64) And lPropiedadSinPortes <> 0 Then
            sAnd = " AND T1.QryGroup" & lPropiedadSinPortes.ToString & " ='N'"   ' si es Y no entra en el calculo
        Else
            sAnd = ""
        End If
        '
        ' Por Segmento siempe se ejecuta
        '
        ls = ""
        ls = ls & " SELECT"
        ls = ls & "  1 as Orden"
        ls = ls & " ,T0.CardCode"
        ls = ls & " ,T0.DocTotal"
        ls = ls & " ,T1.U_SEIsegme"
        ls = ls & " ,T2.U_SEIimport"
        ls = ls & " ,T1.U_SEIcateg"
        ls = ls & " ,T3.U_SEIportes"
        ls = ls & " ,T0.Docduedate"
        ls = ls & " FROM ORDR T0"
        ls = ls & " INNER JOIN OCRD T1 ON T0.CardCode=T1.CardCode"
        ls = ls & " INNER JOIN [@SEIPORTESCLI] T2 ON T1.U_SEIsegme=T2.U_SEIsegme"
        ls = ls & " INNER JOIN [@SEICATEGORIA] T3 ON T1.U_SEIcateg=T3.code"
        ls = ls & " WHERE T0.DocEntry = " & lDocEntry.ToString
        ls = ls & sAnd   ' Que tenga desactivada (valor 'N') la propiedad sin portes
        '
        If (lPropiedad >= 1 And lPropiedad <= 64) And dImportePropiedad <> 0 Then
            '
            ls = ls & " UNION ALL"
            '
            ls = ls & " SELECT"
            ls = ls & "  2 as Orden"
            ls = ls & " ,T0.CardCode"
            ls = ls & " ,T0.DocTotal"
            ls = ls & " ,T1.U_SEIsegme"
            ls = ls & " ," & dImportePropiedad.ToString.Replace(",", ".") & " as U_SEIimport"
            ls = ls & " ,T1.U_SEIcateg"
            ls = ls & " ,T3.U_SEIportes"
            ls = ls & " ,T0.Docduedate"
            ls = ls & " ,T0.SlpCode"
            ls = ls & " FROM ORDR T0"
            ls = ls & " INNER JOIN OCRD T1 ON T0.CardCode=T1.CardCode"
            ls = ls & " INNER JOIN [@SEICATEGORIA] T3 ON T1.U_SEIcateg=T3.code"
            ls = ls & " WHERE T0.DocEntry = " & lDocEntry.ToString
            ls = ls & " and   T1.QryGroup" & lPropiedad.ToString & " ='Y'"
            ls = ls & sAnd   ' Que tenga desactivada (valor 'N') la propiedad sin portes
            ls = ls & " order by Orden"
        End If
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        '
        ' Si el importe del documento
        ' es inferior al configurado
        ' se le aplica Portes
        '
        If Not oRcs.EoF Then

            '
            ' Antes de Aplicar los Portes es necesario
            ' validar si el cliente tiene otro pedido para la misma fecha de entrega
            ' si tiene Pedido validar que la suma dele
            '
            sCliente = CStr(oRcs.Fields.Item("CardCode").Value)
            slpCode = CInt(oRcs.Fields.Item("SlpCode").Value)
            sDocDueDate = CDate(oRcs.Fields.Item("Docduedate").Value).ToString("yyyyMMdd")
            sArticuloPortes = CStr(oRcs.Fields.Item("U_SEIportes").Value)
            dImporteMinimo = CDbl(oRcs.Fields.Item("U_SEIimport").Value)
            sAlmacenCliente = getAlmacenCliente(slpCode)
            '
            dImportePedidos = ImportePedidosFentrega(sCliente, sDocDueDate, sArticuloPortes)
            lNumPortes = TienePortes(sCliente, sDocDueDate, sArticuloPortes)
            '
            If sArticuloPortes <> "" Then
                If dImportePedidos < dImporteMinimo Then
                    If lNumPortes = 0 Then
                        ' Añadir Portes
                        AñadirPortes(lDocEntry, sArticuloPortes, 1, sAlmacenCliente)
                    End If
                Else
                    ' Si tiene 2 portes se entiende que tiene el positivo y negativo
                    If lNumPortes = 1 Then
                        ' Quitar Portes Poner cantidad 1 en negativo
                        AñadirPortes(lDocEntry, sArticuloPortes, lNumPortes * -1, sAlmacenCliente)
                    End If
                End If
            End If

            '
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Sub

    Public Sub ObtenerPropiedad(ByRef lPropiedad As Long, ByRef dImportePropiedad As Double)
        '
        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        Dim ls As String
        '
        ls = ""
        ls = ls & " SELECT TOP 1 ISNULL(U_SEIpropiedad,0) as U_SEIpropiedad,  ISNULL(U_SEIimport,0) as U_SEIimport"
        ls = ls & " FROM  [@SEIPORTESCLI2]"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        lPropiedad = 0
        dImportePropiedad = 0
        '
        If Not oRcs.EOF Then
            '
            lPropiedad = CLng(oRcs.Fields.Item("U_SEIpropiedad").Value)
            dImportePropiedad = CDbl(oRcs.Fields.Item("U_SEIimport").Value)
            '
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Sub

    Public Function getConstanteNumero(ByVal sContador As String) As Double

        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        Dim ls As String

        ls = ""
        ls = ls & " SELECT ISNULL(U_SEIconta, 0) as U_SEIconta"
        ls = ls & " FROM [@SEICONTA]"
        ls = ls & " Where Code = '" & sContador & "'"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EoF Then
            getConstanteNumero = CDbl(oRcs.Fields.Item("U_SEIconta").Value)
        Else
            getConstanteNumero = 0
        End If

        LiberarObjCOM(CObj(oRcs))

    End Function

    Public Function ImportePedidosFentrega(ByVal sCardCode As String, ByVal sFechaEntrega As String, ByVal sArticuloPortes As String) As Double
        '
        Dim oRcs As SAPbobsCOM.Recordset
        Dim ls As String
        '
        ImportePedidosFentrega = 0
        '
        ' Importes Pedidos agrupados por Cliente-Fecha entrega y restar los portes si los tiene
        '
        '
        ls = ""
        ls = ls & " SELECT SUM(PP.ImporteTotal) as 'ImporteTotal'"
        ls = ls & " From"
        ls = ls & " ("
        ls = ls & " SELECT"
        ls = ls & " ISNULL("
        'ls = ls & " (SELECT SUM(T100.LineTotal+T100.vatsum)"
        ls = ls & " (SELECT SUM(T100.LineTotal)"
        ls = ls & " FROM RDR1 T100"
        ls = ls & " Where T100.DocEntry = T0.DocEntry"
        ls = ls & " AND   T100.ItemCode <> '" & sArticuloPortes & "'),0) as 'ImporteTotal'"
        ls = ls & " FROM ORDR T0"
        ls = ls & " Where T0.CardCode  = '" & sCardCode & "'"
        ls = ls & " AND   T0.DocDueDate= '" & sFechaEntrega & "'"
        ls = ls & " AND   T0.DocStatus='O'"
        ls = ls & " ) AS PP"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EOF Then
            '
            ImportePedidosFentrega = CDbl(oRcs.Fields.Item("ImporteTotal").Value)
            '
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function

    Public Function TienePortes(ByVal sCardCode As String, ByVal sFechaEntrega As String, ByVal sArticuloPortes As String) As Long
        '
        Dim oRcs As SAPbobsCOM.Recordset
        Dim ls As String
        '
        TienePortes = 0
        '
        ls = ""
        ls = ls & " SELECT Count(*) as 'CantidadPortes' "
        ls = ls & " FROM  ORDR T0 INNER JOIN RDR1 T1 "
        ls = ls & " ON T0.DocEntry = T1.DocEntry"
        ls = ls & " Where T0.CardCode  = '" & sCardCode & "'"
        ls = ls & " AND   T0.DocDueDate= '" & sFechaEntrega & "'"
        ls = ls & " AND   T1.ItemCode='" & sArticuloPortes & "'"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EOF Then
            '
            TienePortes = CLng(oRcs.Fields.Item("CantidadPortes").Value)
            '
        End If
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function

    Public Sub AñadirPortes(ByVal lDocEntry As Integer, ByVal sArticuloPortes As String, ByVal lCantidad As Long, ByVal sAlmacenCliente As String)
        '
        Dim oDocP As SAPbobsCOM.Documents
        Dim lr As Long
        Dim sError As String = ""
        '
        oDocP = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders), SAPbobsCOM.Documents)
        '
        If Not oDocP.GetByKey(lDocEntry) Then
            oDocP = Nothing
            Exit Sub
        End If
        '
        oDocP.Lines.Add()
        oDocP.Lines.SetCurrentLine(oDocP.Lines.Count - 1)
        oDocP.Lines.ItemCode = sArticuloPortes
        oDocP.Lines.Quantity = lCantidad
        oDocP.Lines.WarehouseCode = sAlmacenCliente
        '
        lr = oDocP.Update
        If lr <> 0 Then
            Throw New Exception("Error al modificar portes: " & getCompany.GetLastErrorDescription)
        End If
        '
        LiberarObjCOM(CObj(oDocP))
        '
    End Sub

    'CONGELADOS TELYNET

    Private Sub InsertarPedidoCongeladoEnTablaTemporal(ByVal objPedidoCab As EntDocumentoCab, ByVal objPedidoLin As List(Of EntDocumentoLin), ByVal Empleado As Integer,
                                                       ByVal numPedidoInterno As String)
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQL As String = ""

        Try
            'If ExistePedidoCongelado() Then

            'End If

            'Dim numPedidoInterno As String
            'numPedidoInterno = CreateNumPedidoInternoTLY(objPedidoCab.NumCab, Empleado)

            'Conexion BBDD Telynet
            mCon = New clsConexion().OpenConexionSQLTELYNET()
            ocommand = mCon.CreateCommand()

            'Insertar cabecera
            SQL = "INSERT INTO ORDERS_HEADERS_TLY "
            SQL &= "([Create_Date]"
            SQL &= ",[Create_User]"
            SQL &= ",[Modify_Date]"
            SQL &= ",[Modify_User]"
            SQL &= ",[status_mobile]"
            SQL &= ",[Code_Agent]"
            SQL &= ",[Brute_Amount]"
            SQL &= ",[Code_Addess_Delivery]"
            SQL &= ",[Code_Client]"
            SQL &= ",[Delivery_Date]"
            SQL &= ",[Itinerary]"
            SQL &= ",[Net_Amount]"
            SQL &= ",[NPedido]"
            SQL &= ",[Observations]"
            SQL &= ",[Observations2]"
            SQL &= ",[OrderDate]"
            SQL &= ",[Order_Num]"
            SQL &= ",[Order_Type]"
            SQL &= ",[Error]"
            SQL &= ",[ErrorDesc]"
            SQL &= ",[SMS]"
            SQL &= ",[ID_FECHA_HISTORICO] "
            SQL &= " ) VALUES ( "
            SQL &= " '" & objPedidoCab.DocDate & "', "
            'SQL &= " '" & objPedidoCab.EmpID & "', "
            SQL &= " '" & getComercial_de_Empleado(objPedidoCab.EmpID) & "', "
            SQL &= " '" & objPedidoCab.DocDate & "', "
            'SQL &= " '" & objPedidoCab.EmpID & "', "
            SQL &= " '" & getComercial_de_Empleado(objPedidoCab.EmpID) & "', "
            SQL &= " '10', "
            'SQL &= " '" & objPedidoCab.EmpID & "', "
            SQL &= " '" & getComercial_de_Empleado(objPedidoCab.EmpID) & "', "
            SQL &= " '" & CInt(objPedidoCab.TotalBruto) & "', "
            SQL &= " '', "
            SQL &= " '" & objPedidoCab.CardCode & "', "
            SQL &= " '" & objPedidoCab.FechaEntrega & "', "
            SQL &= " '" & objPedidoCab.RutaID & "', "
            SQL &= " '" & CInt(objPedidoCab.TotalNeto) & "', "
            SQL &= " '" & numPedidoInterno & "', "
            SQL &= " '" & objPedidoCab.Comentarios & "', "
            SQL &= " '" & objPedidoCab.Comentarios & "', "
            SQL &= " '" & objPedidoCab.DocDate & "', "
            SQL &= " '" & numPedidoInterno & "', "
            SQL &= " 'N', "
            SQL &= " 'S', "
            SQL &= " 'Cliente BLOQUEADO', "
            SQL &= " 'S', "
            SQL &= " '' "
            SQL &= " ) "

            ocommand.CommandText = SQL
            ocommand.Parameters.Clear()

            If ocommand.ExecuteNonQuery() = 0 Then
                Throw New Exception("La inserción devolvió 0 registros afectados en cabecera")
            End If


            'Insertar lineas
            Dim iLineNum As Integer = 1
            For Each Linea As EntDocumentoLin In objPedidoLin
                SQL = "INSERT INTO ORDERS_LINE_TLY "
                SQL &= " ([Create_Date]"
                SQL &= " ,[Create_User]"
                SQL &= " ,[Modify_Date]"
                SQL &= " ,[Modify_User]"
                SQL &= " ,[status_mobile]"
                SQL &= " ,[Line_Num]"
                SQL &= " ,[Order_Num]"
                SQL &= " ,[Quantity]"
                SQL &= " ,[Quantity_Unit]"
                SQL &= " ,[Unit_Type]"
                SQL &= " ,[Brute_Amount]"
                SQL &= " ,[Code_Client]"
                SQL &= " ,[Code_Product]"
                SQL &= " ,[Dis1]"
                SQL &= " ,[Dis2]"
                SQL &= " ,[Dis3]"
                SQL &= " ,[Dto_Promocional]"
                SQL &= " ,[Notes]"
                SQL &= " ,[Order_Date]"
                SQL &= " ,[Price]"
                SQL &= " ,[OfertaCaducidad]"
                SQL &= " ,[DescuentoCero]"
                SQL &= " ) VALUES ( "
                SQL &= " '" & objPedidoCab.DocDate & "', "
                SQL &= " '" & objPedidoCab.EmpID & "', "
                SQL &= " '" & objPedidoCab.DocDate & "', "
                SQL &= " '" & objPedidoCab.EmpID & "', "
                SQL &= " '10', "
                SQL &= " '" & iLineNum & "', "
                SQL &= " '" & numPedidoInterno & "', "
                'Tenemos que dividir entre el factor porque al pasar los pedidos de TLY a SAP recalculan la cantidad multiplicando por el factor
                'y en algunos casos como por ejemplo los articulos KG se vuelve a multiplicar por el factor
                SQL &= " '" & (Linea.CantidadReal / BuscarFactor(Linea.ItemCode)).ToString.Replace(",", ".") & "', "   'SQL &= " '" & Linea.CantidadReal.ToString.Replace(",", ".") & "', "
                '
                SQL &= " '0', "
                SQL &= " '" & Linea.TipoUnidadesCajas & "', "
                SQL &= " '" & Linea.TotalSinDescuentos.ToString.Replace(",", ".") & "', "
                SQL &= " '" & objPedidoCab.CardCode & "', "
                SQL &= " '" & Linea.ItemCode & "', "
                SQL &= " '" & Linea.dto1.ToString.Replace(",", ".") & "', "
                SQL &= " '" & Linea.dto2.ToString.Replace(",", ".") & "', "
                SQL &= " '" & Linea.dto3.ToString.Replace(",", ".") & "', "
                SQL &= " '" & Linea.dtoManual.ToString.Replace(",", ".") & "', " 'SQL &= " '" & Linea.dtoTotalCalculado.ToString.Replace(",", ".") & "', "
                SQL &= " '" & objPedidoCab.Comentarios & "', "
                SQL &= " '" & objPedidoCab.FechaEntrega & "', "
                SQL &= " '" & Linea.Precio.ToString.Replace(",", ".") & "', "
                SQL &= " '" & Linea.LineaOferta & "', "
                SQL &= " '" & CInt(IIf(Linea.CodigoDescuento & String.Empty = "Eliminados", 1, 0)) & "' " 'SQL &= " '" & Linea.dtoManual.ToString.Replace(",", ".") & "' "
                SQL &= " ) "

                ocommand.CommandText = SQL
                ocommand.Parameters.Clear()

                If ocommand.ExecuteNonQuery() = 0 Then
                    Throw New Exception("La inserción devolvió 0 registros afectados en lineas")
                End If

                iLineNum += 1
            Next

        Catch ex As Exception
            clsLog.Log.Fatal("No se pudo insertar pedido congelado: " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Sub

    Private Function ExistePedidoCongelado(ByVal Cliente As String, ByVal Empleado As String, ByVal FechaCrea As Integer, ByVal NPedido As String,
                                           ByVal TotalNeto As Double, ByVal TotalBruto As Double) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try

            SQL = " SELECT TOP 1 NPedido FROM ORDERS_HEADERS_TLY WHERE 1=1 "
            SQL &= " AND Code_Client = '" & Cliente & "'"
            SQL &= " AND isnull(Code_Agent,0) = '" & Empleado & "'"
            SQL &= " AND isnull(Create_Date,'') = '" & FechaCrea & "'"
            SQL &= " AND isnull(Create_User,'') = '" & Empleado & "'"
            SQL &= " AND isnull(NPedido,'') = '" & NPedido & "'"
            SQL &= " AND isnull(Net_Amount,0) = '" & CInt(TotalNeto) & "'"
            SQL &= " AND isnull(Brute_Amount,0) = '" & CInt(TotalBruto) & "'"

            mCon = New clsConexion().OpenConexionSQLTELYNET()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            Dim executeSQL As Object = oCommand.ExecuteScalar()
            If Not executeSQL Is Nothing Then
                retVal = executeSQL.ToString
            End If

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        If retVal <> "" Then
            clsLog.Log.Info("YA EXISTE PEDIDO CONGELADO: Emp:'" & Empleado.ToString & "' NumTLY:'" & NPedido.ToString & "' Cliente:'" & Cliente & "' FCrea:'" & FechaCrea & "'")
            clsLog.Log.Info("Existe:" & retVal)
        End If

        Return retVal
    End Function

    Private Function BuscarFactor(ByVal sItemCode As String) As Double
        '
        '  En los Pedidos de PDA la cantidad entrada por el pedido es la valida
        '  En cambio el mismo articulo y la misma cantidad por el Formulario de SBO
        '  incrementa la cantidad
        '
        Dim ls As String
        Dim oRcs As SAPbobsCOM.Recordset = Nothing
        Dim dBuscarFactor As Double
        '
        BuscarFactor = 0
        '
        ls = ""
        ls = ls & " SELECT salfactor2, salfactor3, salPackmsr "
        ls = ls & " FROM OITM "
        ls = ls & " WHERE ItemCode='" & sItemCode & "'"
        '
        oRcs = CType(getCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EoF Then
            If CStr(oRcs.Fields.Item("salPackmsr").Value) = "C" Then
                'Modificacion Victor 27-09-2010
                dBuscarFactor = CDbl(oRcs.Fields.Item("salfactor2").Value)
                'dBuscarFactor = NullToDoble(osalfactor3.Item(0).InnerText.Replace(".", ","))
            Else
                dBuscarFactor = CDbl(oRcs.Fields.Item("salfactor2").Value)
            End If
        Else
            dBuscarFactor = 1
        End If
        '
        If dBuscarFactor = 0 Then
            dBuscarFactor = 1
        End If
        '
        BuscarFactor = dBuscarFactor
        '
        LiberarObjCOM(CObj(oRcs))
        '
    End Function


    Private Function PedidoSigueCongeladoEnTablaTemporal(objPedido As EntDocumentoCab, Empleado As Integer) As Boolean
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQL As String = ""
        Dim retVal As Boolean = False

        Try

            'Conexion BBDD Telynet
            mCon = New clsConexion().OpenConexionSQLTELYNET()
            ocommand = mCon.CreateCommand()

            Dim NumPedidoInterno As String = CreateNumPedidoInternoTLY(objPedido.NumCab, Empleado)

            'Insertar cabecera
            'SQL = "SELECT COUNT(ID) FROM ORDERS_HEADERS_TLY WHERE Create_User='" & Empleado & "' AND Order_Num = '" & NumPedidoInterno & "' AND Code_Client= '" & objPedido.CardCode & "'"
            SQL = "SELECT COUNT(ID) FROM ORDERS_HEADERS_TLY WHERE Create_User='" & getComercial_de_Empleado(objPedido.EmpID) & "' AND Order_Num = '" & NumPedidoInterno & "' AND Code_Client= '" & objPedido.CardCode & "'"
            ocommand.CommandText = SQL
            ocommand.Parameters.Clear()

            retVal = (CInt(ocommand.ExecuteScalar) > 0)




        Catch ex As Exception
            clsLog.Log.Fatal("No se pudo insertar pedido congelado: " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function CreateNumPedidoInternoTLY(ByVal NumCab As Long, ByVal Empleado As Integer) As String

        'Funcion para devolver un numero unico para la gestion TLY. 
        'Este numero se usa en cabecera y lineas (ORDERS_HEADERS_TLY, ORDERS_LINES_TLY)
        'Segun Marcel no importa el numero que vaya aqui mientras sea unico y se vea el empleado en las primeras posiciones.

        Dim retVal As String = "000000000000"

        Try

            'Formato de pedido TLY = 064000018
            'Return Right("000" & Empleado.ToString, 3) & Right("000000" & NumCab.ToString, 6)

            '05/07/2016 - MG.- Nuevo numero unico TLY. 12 digitos. [empleado x3, fecha x6, numCab x3]
            retVal = Right("000" & Empleado.ToString, 3) & Date.Now.ToString("yyMMdd") & Right("000" & NumCab.ToString, 3)

            'No deberia tener mas de 12 digitos, pero por si acaso, hacemos un left para que quede el empleado y no un right.
            retVal = Left(retVal, 12)


        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return retVal

    End Function


    'RESTO
    Private Function getCuentaContable_de_Banco_Exception(ByVal IDBanco As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion

        Try

            If IDBanco = "0" Then
                Return "0"
            End If

            Dim SQL As String
            SQL = " SELECT ISNULL(GLAccount,'') FROM ODSC T0 "
            SQL &= " INNER JOIN DSC1 T1 ON T0.BankCode = T1.BankCode AND T0.DfltAcct = T1.Account "
            SQL &= " WHERE T0.BankCode = '" & IDBanco & "'"

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            Dim oObj As Object = oCommand.ExecuteScalar()
            If Not oObj Is Nothing Then
                retVal = oObj.ToString
            End If

            If retVal = "" Then
                Throw New Exception("No hay cuenta contable definida")
            End If

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal


    End Function

    Private Function getComercial_de_Empleado(ByVal Empleado As Integer) As Integer

        Dim retVal As Integer = -1
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion

        Try

            Dim SQL As String
            SQL = "SELECT ISNULL(salesPrson, -1) from ohem where empID = " & Empleado

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString)
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal


    End Function

    Private Function ExistePedido(ByVal Empleado As Integer, ByVal Cliente As String, ByVal HCrea As String, ByVal Lat As String, ByVal Lon As String, ByVal NumTablet As Long) As String
        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion

        Try

            Dim SQL As String
            SQL = " SELECT TOP 1 DocNum FROM ORDR WHERE 1=1 "
            SQL &= " AND CardCode = '" & Cliente & "'"
            SQL &= " AND isnull(U_SEIEmpID,0) = '" & Empleado & "'"
            SQL &= " AND isnull(U_SEIHCreacion,0) = '" & HCrea & "'"
            'SQL &= " AND isnull(U_SEILat,'') = '" & Lat & "'"
            'SQL &= " AND isnull(U_SEILon,'') = '" & Lon & "'"
            SQL &= " AND case when U_SEILat is null then '0.0' when U_SEILat='' then '0.0' else U_SEILat end = '" & Lat & "'"
            SQL &= " AND case when U_SEILon is null then '0.0' when U_SEILon='' then '0.0' else U_SEILon end =  '" & Lon & "'"
            SQL &= " AND isnull(U_SEINumCab,0) = '" & NumTablet & "'"

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            'clsLog.Log.Info(SQL & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            retVal = CStr(oCommand.ExecuteScalar())

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        If retVal <> "" Then
            clsLog.Log.Info("YA EXISTE PEDIDO: Emp:'" & Empleado.ToString & "' NumTablet:'" & NumTablet.ToString & "' Cliente:'" & Cliente & "' HCrea:'" & HCrea & "'")
            clsLog.Log.Info("Existe:" & retVal)
        End If

        Return retVal

    End Function

    Private Sub getMaxDocNum_DocEntry(ByVal Empleado As Integer, ByVal NumCab As Long, ByRef DocNum As String, ByRef DocEntry As Integer)

        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion
        Dim SQL As String = ""
        Try

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()

            SQL = " SELECT MAX(cast(isnull(DocEntry,0) as numeric)) as DocEntry from ORDR WHERE U_SEINumCab = " & NumCab & " AND U_SEIEmpID = " & Empleado
            oCommand.CommandText = SQL
            DocEntry = CInt(oCommand.ExecuteScalar().ToString)

            SQL = " SELECT DocNum from ORDR WHERE DocEntry = " & DocEntry
            oCommand.CommandText = SQL
            DocNum = oCommand.ExecuteScalar().ToString

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub getMaxDocNumFac_DocEntry(ByVal Empleado As Integer, ByVal NumCab As Long, ByRef DocNum As String, ByRef DocEntry As Integer)

        Dim mCon As SqlConnection = Nothing
        Dim oConex As New clsConexion
        Dim SQL As String = ""
        Try

            mCon = oConex.OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()

            SQL = " SELECT MAX(cast(isnull(DocEntry,0) as numeric)) as DocEntry from OINV WHERE U_SEIEmpID = " & Empleado
            oCommand.CommandText = SQL
            DocEntry = CInt(oCommand.ExecuteScalar().ToString)

            SQL = " SELECT DocNum from OINV WHERE DocEntry = " & DocEntry
            oCommand.CommandText = SQL
            DocNum = oCommand.ExecuteScalar().ToString

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Fatal("SQL-" & SQL & " " & ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Sub

    Private Function getNombreDeEmpleado(ByVal Empleado As Integer) As String

        If Empleado = 0 Then
            Return "Administrador"
        End If

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try


            SQL = "Select ISNULL(Emp.firstName,'') + ' ' + ISNULL(Emp.middleName,'') + ' ' + ISNULL(Emp.lastName,'') from OHEM Emp where Emp.empID ='" & Empleado & "'"

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = oCommand.ExecuteScalar().ToString
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function getSAPUserDeEmpleado_Remitente(ByVal Empleado As Integer) As Integer

        If Empleado = 0 Then
            Return -1
        End If

        Dim retVal As Integer = -1
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try


            SQL = "Select ISNULL(userId,0) from OHEM where empID ='" & Empleado & "'"

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString)
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function getSAPUserDeEmpleado_Destinatario(ByVal Empleado As Integer) As String

        If Empleado = 0 Then
            Return ""
        End If

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try


            SQL = "select USER_CODE from OUSR WHERE USERID = (Select ISNULL(userId,0) from OHEM where empID ='" & Empleado & "')"


            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = oCommand.ExecuteScalar().ToString
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function getArticuloCongelado(ByVal ItemCode As String) As Boolean

        Dim retVal As Boolean = False
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try


            SQL = "Select COUNT(ItemCode) from OITM where ItemCode ='" & ItemCode & "' AND ISNULL(FrozenFor,'N')='Y'"

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString) > 0
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function getClienteCongelado(ByVal CardCode As String) As Boolean

        Dim retVal As Boolean = False
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try

            SQL = "Select COUNT(CardCode) from OCRD where CardCode ='" & CardCode & "' AND ISNULL(FrozenFor,'N')='Y' "

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString) > 0
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


    Private Function getClienteBloqueadoRiesgo(ByVal CardCode As String) As Boolean

        Dim retVal As Boolean = False
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try

            SQL = "Select COUNT(CardCode) from OCRD where CardCode ='" & CardCode & "' AND ISNULL(qrygroup9,'N')='Y' "

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CInt(oCommand.ExecuteScalar().ToString) > 0
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Function getCorreoCliente(ByVal CardCode As String) As String

        Dim retVal As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""
        Try


            SQL = "SELECT ISNULL(e_mail, '') as Correo FROM OCRD WHERE CardCode = '" & CardCode & "'"

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            Dim executeSQL As Object = oCommand.ExecuteScalar()
            If Not executeSQL Is Nothing Then
                retVal = executeSQL.ToString
            End If

            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


    'SISTEMA

    Private Function MensajeSalidaSAP(ByVal Asunto As String, ByVal Mensaje As String, ByVal EmpleadoOrigen As Integer, ByVal EmpleadoDestino As Integer) As String

        Dim oCmpSvc As SAPbobsCOM.CompanyService = Nothing
        Dim oMensaje As SAPbobsCOM.Message = Nothing
        Dim oMsjSvc As SAPbobsCOM.MessagesService = Nothing
        Dim oDestinatario As SAPbobsCOM.Recipient = Nothing

        Dim retVal As String = ""

        Try

            oCmpSvc = getCompany().GetCompanyService()

            oMsjSvc = CType(oCmpSvc.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService), SAPbobsCOM.MessagesService)
            oMensaje = CType(oMsjSvc.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage), SAPbobsCOM.Message)

            oMensaje.Subject = Asunto
            oMensaje.Text = Mensaje & vbNewLine & " " & "Enviado por" & " " & getNombreDeEmpleado(EmpleadoOrigen)
            oMensaje.User = getSAPUserDeEmpleado_Remitente(EmpleadoOrigen)
            oMensaje.Priority = SAPbobsCOM.BoMsgPriorities.pr_High

            'Destino

            oDestinatario = oMensaje.RecipientCollection.Add

            oDestinatario.SendEmail = SAPbobsCOM.BoYesNoEnum.tNO
            oDestinatario.SendFax = SAPbobsCOM.BoYesNoEnum.tNO
            oDestinatario.SendSMS = SAPbobsCOM.BoYesNoEnum.tNO
            oDestinatario.SendInternal = SAPbobsCOM.BoYesNoEnum.tYES

            If EmpleadoDestino >= 0 Then
                oDestinatario.UserCode = getSAPUserDeEmpleado_Destinatario(EmpleadoDestino).ToString
            Else
                oDestinatario.UserCode = "manager"
            End If


            oDestinatario.UserType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser

            oMsjSvc.SendMessage(oMensaje)
            retVal = "1"

        Catch ex As Exception

            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message

        Finally

            If Not oCmpSvc Is Nothing Then
                LiberarObjCOM(CObj(oCmpSvc))
            End If

            If Not oMensaje Is Nothing Then
                LiberarObjCOM(CObj(oMensaje))
            End If

            If Not oDestinatario Is Nothing Then
                LiberarObjCOM(CObj(oDestinatario))
            End If

            If Not oMsjSvc Is Nothing Then
                LiberarObjCOM(CObj(oMsjSvc))
            End If

        End Try

        Return retVal

    End Function

    <WebMethod(EnableSession:=True)>
    Private Function getCompany() As SAPbobsCOM.Company
        Try
            If IsNothing(oWSOrigen.Application.Item("ObjetoConexionSAP")) Then
                Dim oCon As New SAPbobsCOM.Company
                oWSOrigen.Application.Add("ObjetoConexionSAP", oCon)
                Return oCon
            Else
                Return CType(oWSOrigen.Application.Item("ObjetoConexionSAP"), SAPbobsCOM.Company)
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return Nothing

    End Function

    Private Sub LiberarObjCOM(ByRef oObjCOM As Object, Optional ByVal bCollect As Boolean = False)

        'Liberar y destruir Objeto com 
        If Not IsNothing(oObjCOM) Then
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oObjCOM)
            oObjCOM = Nothing
            If bCollect Then
                GC.Collect()
            End If
        End If
    End Sub

    Public Function checkLoginSAP() As Boolean

        Dim oCon As SAPbobsCOM.Company = getCompany()
        If oCon Is Nothing Then
            If LogInSAP() = False Then
                Return False
            End If
        ElseIf oCon.Connected = False Then
            If LogInSAP() = False Then
                Return False
            End If
        End If

        Return True

    End Function

    <WebMethod(EnableSession:=True)>
    Private Function LogInSAP() As Boolean

        Try
            Dim oCon As SAPbobsCOM.Company = getCompany()
            If Not IsNothing(oCon) Then
                If oCon.Connected = True Then
                    Return True
                End If
            End If

            Dim BD As String = ConfigurationManager.AppSettings.Get("bd").ToString
            Dim Server As String = ConfigurationManager.AppSettings.Get("server").ToString
            Dim User As String = ConfigurationManager.AppSettings.Get("userSAP").ToString()
            Dim Password As String = ConfigurationManager.AppSettings.Get("passSAP").ToString()
            Dim License As String = ConfigurationManager.AppSettings.Get("licenseServer").ToString

            oCon.CompanyDB = BD
            oCon.UserName = User
            oCon.Password = Password
            oCon.LicenseServer = License
            oCon.Server = Server
            oCon.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014

            oCon.language = SAPbobsCOM.BoSuppLangs.ln_Spanish


            Dim iEstado As Integer = oCon.Connect
            Dim err As String = oCon.GetLastErrorDescription

            If iEstado = 0 Then
                clsLog.Log.Fatal("Se ha conectado correctamente")
                Return True
            Else
                clsLog.Log.Fatal("El estado en connect es " & iEstado & " : " & err & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " test en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return False
        End Try
    End Function
#End Region
End Class
