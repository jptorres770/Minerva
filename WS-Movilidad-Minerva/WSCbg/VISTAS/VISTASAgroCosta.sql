USE [TEST_TABLET]
GO

/****** Object:  View [dbo].[TAB_ARTICULOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_ARTICULOS]
AS
SELECT TOP 100 Percent
Art.ItemCode,  
REPLACE(REPLACE(REPLACE(REPLACE(Art.ItemName  , '''', '�'), '"', ''), '<', ''), '>', '')AS ItemDesc, 
ISNULL((SELECT TOP 1 ISNULL(AdjFoto.FileName, '') + '.' + ISNULL(AdjFoto.FileExt, '') FROM OATC AdjCab INNER JOIN ATC1 AdjFoto ON AdjCab.AbsEntry = AdjFoto.AbsEntry AND AdjFoto.FileName LIKE 'FOTO_%' WHERE AdjCab.AbsEntry = Art.AtcEntry),'') as RutaFoto,
ISNULL((SELECT TOP 1 ISNULL(AdjFicha.FileName, '') + '.' + ISNULL(AdjFicha.FileExt, '') FROM OATC AdjCab INNER JOIN ATC1 AdjFicha ON AdjCab.AbsEntry = AdjFicha.AbsEntry AND AdjFicha.FileName LIKE 'FT_%' WHERE AdjCab.AbsEntry = Art.AtcEntry),'') as RutaFicha,
'' as ListaPrecioCompra,
'' as ListaPrecioVenta,
ISNULL(Art.Codebars, '') AS EAN,
ISNULL(Grp.ItmsGrpCod,'') as FamiliaCode,
ISNULL(Grp.ItmsGrpNam,'') as FamiliaName,
--ISNULL(SubFam.Code,'') as SubFamiliaCode,
--ISNULL(SubFam.Name,'') as SubFamiliaName,
'' as SubFamiliaCode,
'' as SubFamiliaName,
ISNULL(Fab.FirmCode,'') as FabricanteCode,
ISNULL(Fab.FirmName,'') as FabricanteName,
ISNULL(Prov.CardCode,'') as ProveedorCode,
ISNULL(Prov.CardName,'') as ProveedorName,
ISNULL(Art.SalPackMsr, 'C') AS UnidadVenta, 
ISNULL(Art.SalFactor3, 1) AS UnidadesPorCaja, 
ISNULL(Art.SalFactor1, 1) AS Cajas, 
ISNULL(Art.SalFactor2, 1) AS Kilos, 
ISNULL(Vat.Rate, 21) as IVA,
--ISNULL(VatReq.EquVatPr, 5.2) as IVARecargo,
0 as IVARecargo,
ISNULL(Art.PurFactor1,0) as Paletizacion1,
ISNULL(Art.PurFactor3,0) as Paletizacion2,
ISNULL(Art.PurFactor4,0) as Paletizacion3,
CASE WHEN ISNULL(Art.QryGroup10, 'N') <> 'N' THEN 'S' ELSE ISNULL(Art.QryGroup10, 'N') END  as SobrePedido,
'0' as PlazoServicio,
--ISNULL(Art.U_SEIOBS, '') as Notas, preguntar
'' as Notas,
'N' as Promocion,
'N' as Novedad
FROM dbo.OITM AS Art 
INNER JOIN dbo.ITM1 AS Lis ON Art.ItemCode = Lis.ItemCode AND Lis.PriceList = 1
LEFT JOIN dbo.OITB AS Grp ON Art.ItmsGrpCod = Grp.ItmsGrpCod
--LEFT JOIN dbo.[@SEISUBFAMILIA] AS SubFam ON Art.U_SEISubFa = SubFam.Code
LEFT JOIN dbo.OMRC AS Fab ON Art.FirmCode = Fab.FirmCode
LEFT JOIN dbo.OCRD AS Prov ON Art.CardCode = Prov.CardCode
LEFT JOIN OVTG Vat ON Art.VatGourpSa = Vat.Code
--LEFT JOIN OVTG VatReq ON ISNULL(Art.U_SEIRECEV,'') = VatReq.Code
WHERE     
Art.validFor='Y' AND 
Art.InvntItem = 'Y' AND
Art.SellItem = 'Y'

ORDER BY Art.ItemCode
GO
/****** Object:  View [dbo].[TAB_ARTICULOSOFERTAS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--CREATE VIEW [dbo].[TAB_ARTICULOSOFERTAS]
--AS
--SELECT 
--ISNULL(T0.U_SEIarti,'') as ItemCode,
--ISNULL(T0.U_SEIalmac,'') as Almacen,
--ISNULL(T0.U_SEIcanti,0) as Cantidad,
--CONVERT(char(10), ISNULL(T0.U_SEIfcadu,'20000101'), 112) as FechaCaducidad
--FROM [@SEIOFERTA] T0
--WHERE 
--ISNULL(T0.U_SEIfcadu,GETDATE()) > GETDATE()
--GO
--/****** Object:  View [dbo].[TAB_ARTICULOSPRECIOS]    Script Date: 04/07/2016 19:52:12 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

	
ALTER VIEW [dbo].[TAB_ARTICULOSPRECIOS]
AS
SELECT
Art.ItemCode,  
Lis.ListNum AS ListaPrecios, 
Lis.ListName AS ListaNombre, 
ISNULL(Pre.Price,0) as Precio
FROM OITM AS Art 
INNER JOIN ITM1 as Pre ON Art.ItemCode = Pre.ItemCode
INNER JOIN dbo.OPLN AS Lis ON Pre.PriceList = Lis.ListNum
WHERE     
Art.validFor='Y' AND 
Art.InvntItem = 'Y' AND
Art.SellItem = 'Y' 
--AND
--Lis.ListNum IN (SELECT Cli.ListNum FROM OCRD Cli WHERE  CardType = 'C' AND SlpCode IN ('1','3') GROUP BY Cli.ListNum) --Tirar de los empleados activos
--U_SEICateg <> '99' AND
GO
/****** Object:  View [dbo].[TAB_ARTICULOSPREMIOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--	CREATE VIEW [dbo].[TAB_ARTICULOSPREMIOS]
--	AS
--	SELECT TOP 100 Percent
--	Pre.U_SEIArt AS ItemCode,  
--	Pre.U_SEIPremio AS Premio
--	FROM [@SEIARTPREMIOS] as Pre 
--	ORDER BY Pre.U_SEIArt

--GO

--/****** Object:  View [dbo].[TAB_ARTICULOSSTOCKS]    Script Date: 04/07/2016 19:52:12 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE VIEW [dbo].[TAB_ARTICULOSSTOCKS]
	AS
	SELECT TOP 100 Percent
Art.ItemCode,  
Alm.WhsCode AS WhsCode, 
Alm.WhsName AS WhsName, 
ISNULL(Sto.OnHand,0) as StockAlmacen,
ISNULL(Sto.IsCommited,0) as StockComprometido,
--ISNULL((SELECT SUM(Ofe.U_SEIcanti) FROM [@SEIOFERTA] Ofe WHERE Ofe.U_SEIarti = Art.ItemCode AND Ofe.U_SEIalmac = Alm.WhsCode AND ISNULL(Ofe.U_SEIfcadu,GETDATE())> GETDATE()), 0) as StockOferta,
'' as StockOferta,
--Emp.EmpID as EmpID
'' as EmpID
FROM OITM AS Art 
INNER JOIN OITW as Sto ON Art.ItemCode = Sto.ItemCode
INNER JOIN OWHS AS Alm ON Alm.WhsCode = Sto.WhsCode
--INNER JOIN TAB_USUARIOS Emp ON Emp.AlmacenUsuario = Alm.WhsCode
WHERE     
Art.validFor='Y' AND 
Art.InvntItem = 'Y' AND
Art.SellItem = 'Y'
ORDER BY Art.ItemCode

GO

/****** Object:  View [dbo].[TAB_BANCOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_BANCOS]
AS
select
T0.BankCode as IDBanco,
T0.BankName as NombreBanco,
T1.IBAN as Cuenta
      
from ODSC T0 
INNER JOIN DSC1 T1 ON T0.BankCode = T1.BankCode AND T0.DfltAcct = T1.Account 
WHERE T1.BankCode <> '9999'
AND ISNULL(T0.DfltAcct,'') <> ''
GROUP BY T0.BankCode, T0.BankName, T1.IBAN 
UNION ALL
SELECT
'0',
'-Envio a central-',
'-Envio a central-'




GO

/****** Object:  View [dbo].[TAB_CATALOGOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--CREATE VIEW [dbo].[TAB_CATALOGOS]
--AS
--select 
--	 ISNULL([U_SEIIDCatalogo],0) as IDCatalogo
--      ,ISNULL([U_SEINombre],'') as NombreCatalogo
--      ,ISNULL([U_SEICardCode] ,'') as CardCode
--      ,ISNULL([U_SEICardName],'') as CardName
--      ,CONVERT(char(10), ISNULL([U_SEIFechaCrea],'20000101'), 112) as FechaCreacion
--      ,ISNULL([U_SEINotas],'') as Notas
--      ,ISNULL([U_SEIEmpID],0) as EmpID
--      ,'S' as Sincro
--      ,'' as ErrorSincro
--from [@SEICATALOGO] T0
--GO
--/****** Object:  View [dbo].[TAB_CATALOGOSARTICULOS]    Script Date: 04/07/2016 19:52:12 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO



--CREATE VIEW [dbo].[TAB_CATALOGOSARTICULOS]
--AS

--select 

--	 ISNULL([U_SEIIDCatalogo],0) as IDCatalogo
--      ,ISNULL([U_SEIIDLinea],0) as IDLinea
--      ,ISNULL([U_SEIItemCode] ,'') as ItemCode
--      ,ISNULL([U_SEIItemDesc],'') As ItemDesc
--      ,ISNULL([U_SEIPosicion],0) as Posicion
--      ,ISNULL([U_SEIEmpID],0) as EmpID
--from [@SEICATALOGOARTICULO] T0
--GO
--/****** Object:  View [dbo].[TAB_CLIENTECOMPARATIVA]    Script Date: 04/07/2016 19:52:12 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO



--CREATE VIEW [dbo].[TAB_CLIENTECOMPARATIVA]
--AS
--SELECT
--Vista.CardCode,
--Vista.CardName,
--Vista.EmpID,
--Vista.ImpActual,
--Vista.ImpAnterior,
--Vista.ImpActual - Vista.ImpAnterior  AS Diferencia,
--100 - ((Vista.ImpAnterior  * 100 / CASE WHEN Vista.ImpActual= 0 THEN 1 ELSE Vista.ImpActual END)) AS PorcDiferencia
--FROM (
--SELECT
--T0.Codigo AS CardCode,
--T0.Nombre AS CardName,
--ISNULL(T0.EmpID, 0) AS EmpID,
----ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = T0.Codigo AND YEAR(P.DocDate) = Year(GetDate())-1) ,0) as ImpAnterior, 
----ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = T0.Codigo AND YEAR(P.DocDate) = Year(GetDate()) ),0) as ImpActual 
--ISNULL((SELECT SUM(L.LineTotal) FROM OINV P INNER JOIN INV1 L ON P.DocEntry = L.DocEntry WHERE P.CardCode = T0.Codigo AND P.SlpCode = T1.SlpCode AND P.DocDate BETWEEN DATEADD(YEAR, -2, GETDATE()) AND DATEADD(YEAR, -1, GETDATE())) ,0) as ImpAnterior, 
--ISNULL((SELECT SUM(L.LineTotal) FROM OINV P INNER JOIN INV1 L ON P.DocEntry = L.DocEntry WHERE P.CardCode = T0.Codigo AND P.SlpCode = T1.SlpCode AND P.DocDate BETWEEN DATEADD(YEAR, -1, GETDATE()) AND GetDate()) ,0) as ImpActual 
--FROM TAB_CLIENTES T0
----INNER JOIN TAB_USUARIOS T1 ON T0.EmpID = T1.EmpID
--) AS Vista
--GO
--/****** Object:  View [dbo].[TAB_CLIENTECOMPARATIVADETALLE]    Script Date: 04/07/2016 19:52:12 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO



CREATE VIEW [dbo].[TAB_CLIENTECOMPARATIVADETALLE] AS

SELECT 
Vista.CardCode, 
Vista.ItemCode, 
Vista.ItemName, 
Emp.EmpID as EmpID, 
SUM(Vista.ImpAnterior) as ImpAnterior, 
SUM(Vista.ImpActual) as ImpActual,
SUM(Vista.ImpActual) - SUM(Vista.ImpAnterior)  AS Diferencia,
100 - ((SUM(Vista.ImpAnterior) * 100 / CASE WHEN SUM(Vista.ImpActual)= 0 THEN 1 ELSE SUM(Vista.ImpActual) END)) AS PorcDiferencia
FROM 
(
SELECT 
T3.CardCode as CardCode,
T1.ItemCode as ItemCode,
T2.ItemName as ItemName,
T0.SlpCode,
SUM(T1.LineTotal) as ImpAnterior,
0 as ImpActual
FROM OINV T0
INNER JOIN INV1 T1 ON T0.DocEntry = T1.DocEntry
INNER JOIN OITM T2 ON T1.ItemCode = T2.ItemCode
INNER JOIN OCRD T3 ON T3.CardCode = T0.CardCode
WHERE 
T0.DocDate BETWEEN DATEADD(YEAR, -2, GETDATE()) AND DATEADD(YEAR, -1, GETDATE())
AND T1.LineTotal > 0
GROUP BY 
T3.CardCode,
T1.ItemCode,
T2.ItemName,
T0.SlpCode
UNION ALL
SELECT 
T3.CardCode as CardCode,
T1.ItemCode as ItemCode,
T2.ItemName as DesArt,
T0.SlpCode,
0 as ImpAnterior,
SUM(T1.LineTotal) as ImpActual
FROM OINV T0
INNER JOIN INV1 T1 ON T0.DocEntry = T1.DocEntry
INNER JOIN OITM T2 ON T1.ItemCode = T2.ItemCode
INNER JOIN OCRD T3 ON T3.CardCode = T0.CardCode
WHERE 
T0.DocDate BETWEEN DATEADD(YEAR, -1, GETDATE()) AND GetDate()
AND T1.LineTotal > 0
GROUP BY 
T3.CardCode,
T1.ItemCode,
T2.ItemName,
T0.SlpCode
) as Vista
INNER JOIN OHEM as Emp ON Vista.SlpCode = Emp.salesPrson 
--OR Vista.SlpCode IN (SELECT SEIEmp.U_SEIslphi FROM [@SEIEMPLE] SEIEmp WHERE ISNULL(SEIEmp.U_SEIestad,'') <> '99' AND SEIEmp.U_SEIslppa = Emp.salesPrson)
GROUP BY 
CardCode, ItemCode, ItemName, empID
--, EmpID

GO

/****** Object:  View [dbo].[TAB_CLIENTES]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










SELECT CreateDate FROM OCRD



ALTER VIEW [dbo].[TAB_CLIENTES]
AS
SELECT 
	 --Cab.SlpCode as Test,
	 Cab.CardCode AS Codigo,
	 REPLACE(REPLACE(REPLACE(REPLACE(Cab.CardName , '''', '�'), '"', ''), '<', ''), '>', '')AS Nombre,
	 REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(Cab.CardFName, Cab.CardName) , '''', '�'), '"', ''), '<', ''), '>', '') AS NombreComercial,
	 --CONVERT(char(10), ISNULL(Cab.U_SEIFALT,'20000101'), 112) AS FechaAlta, preguntar este campo es necesario.
	 --CONVERT(char(10), '20000101') as FechaAlta,
	 CONVERT(char(10),ISNULL(Cab.CreateDate, '20000101'), 112) as FechaAlta,
	 --ISNULL(Cab.U_SEIsegme, '') AS Grupo,	 preguntar tambien este campo
	 '' as Grupo,
	 REPLACE(ISNULL(Cab.LicTradNum, ''),'ES','') AS NIF,
	 ISNULL(Emp.empID, '') AS EmpID,
	 CASE WHEN ISNULL(Cab.frozenFor, 'N') <> 'N' THEN 'S' ELSE ISNULL(Cab.frozenFor, 'N') END as Congelado,
	 ISNULL(Cab.Phone1, '') AS Telefono1,
	 ISNULL(Cab.Phone2, '') AS Telefono2,
	 ISNULL(Cab.Cellular, '') AS TelefonoMv,
	 ISNULL(Cab.E_Mail, '') AS Email,
	 ISNULL(Cab.CntctPrsn, '') AS Contacto,	 	 
	 -- CREDITO (Teniendo en cuenta consolidacion)--
	 ISNULL(Cab.CreditLine, 0) AS LimiteCreditoTotal,
	 CASE WHEN ISNULL(Cab.FatherCard ,'')='' THEN Cab.Balance + Cab.DNotesBal+ Cab.OrdersBal
	 ELSE (SELECT SUM(Pad.Balance + Pad.DNotesBal + Pad.OrdersBal) From OCRD Pad WHERE Cab.FatherCard= Pad.FatherCard) END as CreditoConsumido,
	 ISNULL(Cab.CreditLine, 0) - CASE WHEN ISNULL(Cab.FatherCard ,'')='' THEN Cab.Balance + Cab.DNotesBal+ Cab.OrdersBal
	 ELSE (SELECT SUM(Pad.Balance + Pad.DNotesBal + Pad.OrdersBal) From OCRD Pad WHERE Cab.FatherCard= Pad.FatherCard) END as CreditoDisponible,
	 -- FIN CREDITO --
	 ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = Cab.Cardcode AND P.DocDate BETWEEN DATEADD(YEAR, -1, GETDATE()) AND GetDate()) ,0) as VentasUltAnyo , 
	 ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = Cab.Cardcode AND P.DocDate BETWEEN DATEADD(YEAR, -2, GETDATE()) AND DATEADD(YEAR, -1, GETDATE())) ,0) as VentasAnyoAnt, 
	 --ISNULL((SELECT SUM(F.DocTotal) FROM OINV F WHERE F.CardCode = Cab.Cardcode AND F.DocDate BETWEEN DATEADD(Year,-1, GetDate()) AND GetDate()) ,0) as VentasUltAnyo, 
	 --ISNULL((SELECT SUM(F.DocTotal) FROM OINV F WHERE F.CardCode = Cab.Cardcode AND YEAR(F.DocDate) = Year(GetDate())-1 ),0) as VentasAnyoAnt, 
	 --CASE WHEN ISNULL(Cab.U_SEIopera, '') = '03' THEN 'S' ELSE 'N' END AS GestionSAC, preguntar este campo
	 'N' as GestionSAC,
	 ISNULL((SELECT AVG(P.DocTotal) FROM ORDR P WHERE P.CardCode = Cab.Cardcode),0) as ImpMedioPedido, 
	 CONVERT(char(10), ISNULL((SELECT MAX(A.DocDate) FROM ODLN A WHERE A.CardCode = Cab.Cardcode),'20000101'), 112) as FechaUltAlbaran, 
	 --ISNULL(Cab.U_SEIOBSAL, '') AS ObsAlbaran, preguntar este campo
	 '' as ObsAlbaran,
	 --ISNULL(Cab.U_SEIobstv, '') AS Notas, preguntar este campo
	 '' as Notas,
	 REPLACE(ISNULL((SELECT TOP 1 DF.Street FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as DireccionFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.City FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as PoblacionFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.ZipCode FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as CPFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.City FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as CiudadFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 PF.Name FROM CRD1 AS DF LEFT JOIN OCST PF ON DF.State = PF.Code WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as ProvinciaFactura ,
	 --de donde tomar la latitud y la longitud como crear los campos
	 0 as Latitud ,
	 0 as Longitud ,
	 ISNULL(Pag.PymntGroup, '') AS CondicionPago,
	 ISNULL((SELECT TOP 1 C.PmntDate FROM CRD5 as C WHERE C.CardCode = Cab.CardCode),'') as DiaPago,
	 ISNULL((SELECT TOP 1 Pago.Descript FROM CRD2 AS V INNER JOIN OPYM Pago ON V.PymCode=Pago.PayMethCod WHERE V.CardCode = Cab.CardCode),'') as ViaPago,
	 CASE WHEN ISNULL(Cab.QryGroup2,'N') = 'Y'  THEN 'Fac.Agrupada' 
	 ELSE CASE WHEN ISNULL(Cab.QryGroup3,'N') = 'Y'  THEN 'Fac.Agr.Albar�n' 
	 ELSE CASE WHEN ISNULL(Cab.QryGroup6,'N') = 'Y'  THEN 'Fac.Agr.Dom.Entrega'
	 ELSE CASE WHEN ISNULL(Cab.QryGroup7,'N') = 'Y'  THEN 'Fac.Agr.Quincenal'
	 ELSE '' END  END END END as Facturacion,
	 --preguntar el por que de este case
	 ISNULL(Pre.ListNum,'') AS ListaPreciosCode,
	 ISNULL(Pre.ListName,'') AS ListaPreciosName,
	 ISNULL((Select TOP 1 AVG(DATEDIFF(DD,Fac.DocDate, CabCobro.DocDate)) as PlazoMedio
			from RCT2 LinCobro 
			INNER JOIN ORCT CabCobro on LinCobro.DocNum=CabCobro.DocEntry
			INNER JOIN OINV Fac on Fac.DocEntry=LinCobro.DocEntry and LinCobro.InvType=13
			WHERE CabCobro.CardCode = Cab.CardCode
			GROUP BY CabCobro.CardCode),0) AS PlazoMedioPago,
	 ISNULL(Ban.Account, '') AS NumeroCuenta,
	 CASE WHEN ISNULL(Cab.Equ, 'N') <> 'N' THEN 'S' ELSE  ISNULL(Cab.Equ, 'N') END AS RecargoEquiv,

     --ISNULL(Cab.U_SEICateg,'') AS Categoria, preguntar es el mismo grupo de socios de negocio
	 '' as Categoria,
	 --ISNULL(Act.Name, '') AS Actividad,
	 '' as Actividad,
	 'S' as Sincro, 
	 0 as FechaSincro,
	 '' as IncidenciaSincro
	 --preguntar sobre estos sincro para que se usan y por que estan quemados de esta manera
	FROM OCRD AS Cab 	
	--LEFT JOIN  [@SEIACTIVIDAD] Act ON Cab.U_SEIActi = Act.Code Preguntar esta tabla y este campo para que se usan	
	LEFT JOIN OPLN Pre ON Cab.ListNum = Pre.ListNum
	LEFT JOIN OCTG Pag ON Cab.GroupNum = Pag.GroupNum
	LEFT JOIN OHEM AS Emp ON Cab.SlpCode = Emp.salesPrson 
	--OR Cab.SlpCode IN (SELECT SEIEmp.U_SEIslphi FROM [@SEIEMPLE] SEIEmp WHERE ISNULL(SEIEmp.U_SEIestad,'') <> '99' AND SEIEmp.U_SEIslppa = Emp.salesPrson)
	LEFT JOIN CRD1 AS Lin ON (Cab.ShipToDef = Lin.Address) AND (Cab.CardCode = Lin.CardCode) AND (Lin.AdresType = 'S') 
	LEFT JOIN OCRB AS Ban ON Ban.CardCode = Cab.CardCode AND Ban.BankCode = Cab.BankCode AND Cab.BankCode = Ban.Account
	WHERE (Cab.CardType = 'C')
GO
/****** Object:  View [dbo].[TAB_CLIENTESCONTACTOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


select * from ocrd





	CREATE VIEW [dbo].[TAB_CLIENTESCONTACTOS]
	AS

		select 
		Con.cntctCode as Orden,
		Con.CardCode as CardCode,
		ISNULL(Con.Position,'') as Puesto,
		ISNULL(Con.Name,'') as Nombre,
		ISNULL(Con.E_MailL,'') as Email,
		ISNULL(Con.Tel1,'') as Telefono,
		ISNULL(Vis.EmpID,'0') as EmpID
		--'' as EmpID
		FROM ocpr Con 
		INNER JOIN TAB_CLIENTES Vis ON VIS.Codigo = Con.CardCode
GO
/****** Object:  View [dbo].[TAB_CLIENTESDIRECCIONES]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER VIEW [dbo].[TAB_CLIENTESDIRECCIONES]
AS
select 
Vis.Codigo as CardCode,
ISNULL(Dir.Address,'') as IDDireccion,
REPLACE(ISNULL(Dir.Street,''),'''','�') as Direccion,
REPLACE(ISNULL(Dir.City,''),'''','�') as Poblacion,
REPLACE(ISNULL(Dir.City,''),'''','�') as Ciudad,
ISNULL(Dir.ZipCode,'') as CP,
REPLACE(ISNULL(Prov.Name,''),'''','�') as Provincia,
--ISNULL(U_SEILat,'0') as Latitud,
'' as Latitud,
--ISNULL(U_SEILon, '0') as Longitud,
'' as Longitud,
--ISNULL(Rut.U_SEIrname,'') as RutaNombre,
'' as RutaNombre,
--ISNULL(Rut.U_SEIrutid,'') as RutaID,
'' as RutaID,
--ISNULL(Rut.U_SEIdia1,'N') as Lunes, 
'' as Lunes,
--ISNULL(Rut.U_SEIdia2,'N') as Martes,
'' as Martes,
--ISNULL(Rut.U_SEIdia3,'N') as Miercoles,
'' as Miercoles,
--ISNULL(Rut.U_SEIdia4,'N') as Jueves,
'' as Jueves,
--ISNULL(Rut.U_SEIdia5,'N') as Viernes,
'' as Viernes,
--ISNULL(Rut.U_SEIdia6,'N') as Sabado,
'' as Sabado,
Vis.EmpID as EmpID
FROM CRD1 Dir 
LEFT JOIN OCST Prov ON Dir.State = Prov.Code
--LEFT JOIN  [@SEIRUTAS] Rut ON Dir.U_SEIRUTA = Rut.Code
INNER JOIN TAB_Clientes Vis ON Dir.CardCode = Vis.Codigo AND Dir.AdresType = 'S' 
GO
/****** Object:  View [dbo].[TAB_CLIENTESRIESGO]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_CLIENTESRIESGO]
AS
SELECT 
--ISNULL(T0.U_SEIbanco,'') as Banco,
'' as Banco,
--ISNULL(T0.U_CardCode,'') as CardCode,
'' as CardCode,
--ISNULL(T1.Nombre,'') as CardName,
'' as CardName,
--ISNULL(T0.U_SEIclas,'') as CL,
'' as CL,
--ISNULL(T0.U_comments,'') as Comentarios,
'' as Comentarios,
ISNULL(T1.EmpID,'') as EmpID,
--ISNULL(T0.U_SEIestef,'') as Estado,
'' as Estado,
--ISNULL(T1.EmpID,'') as EV,
'' as EV,
--ISNULL(RIGHT('0000' + CAST(year(T0.U_docdate) as varchar(4)),4) + RIGHT('00' + CAST(Month(T0.U_docdate) as varchar(4)),2) + RIGHT('00' + CAST(Day(T0.U_docdate)as varchar(4)),2),'20000101') as FechaContable,
'20000101' as FechaContable,
--ISNULL(RIGHT('0000' + CAST(year(T0.U_duedate) as varchar(4)),4) + RIGHT('00' + CAST(Month(T0.U_duedate) as varchar(4)),2) + RIGHT('00' + CAST(Day(T0.U_duedate)as varchar(4)),2),'20000101') as FechaVencimiento,
'20000101' as FechaVencimiento,
--ISNULL(RIGHT('0000' + CAST(year(T0.U_duedatef) as varchar(4)),4) + RIGHT('00' + CAST(Month(T0.U_duedatef) as varchar(4)),2) + RIGHT('00' + CAST(Day(T0.U_duedatef)as varchar(4)),2),'20000101') as FechaVencimientoEfecto,
'20000101' as FechaVencimientoEfecto,
--ISNULL(T3.PymntGroup,'') as FormaCobro,
'' as FormaCobro,
--ISNULL(T0.U_docnum,0) as NumDoc,
0 as NumDoc,
--ISNULL(T0.U_SEIrebut,'') as NumEfecto,
'' as NumEfecto,	
--ISNULL(T0.U_SEIremes,'') as NumRemesa,
'' as NumRemesa,	
--ISNULL(T0.U_docpend,0) as Pendiente,
0 as Pendiente,	
--ISNULL(T0.U_SEIcredi,0) as Riesgo,
0 as Riesgo,
--ISNULL(T0.U_SEItipod,'') as TipoDoc,
'' as TipoDoc,
--ISNULL(T0.U_doctotal,0) as TotalDoc,
'' as TotalDoc,
--ISNULL(T0.U_SEItotap,0) as TotalEfecto,
'' as TotalEfecto,
--ISNULL(T2.Descript,'') as ViaCobro
'' as ViaCobro
--FROM [@SEIINFO6] T0
FROM  TAB_CLIENTES T1
--INNER JOIN TAB_CLIENTES T1 ON T0.U_CardCode = T1.Codigo
--LEFT OUTER JOIN OPYM T2 ON T0.U_pymcode = T2.PayMethCod
--LEFT OUTER JOIN OCTG T3 ON T0.U_groupnum = T3.GroupNum
GO
/****** Object:  View [dbo].[TAB_DESCUENTOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[TAB_DESCUENTOS]
AS
SELECT 
ISNULL(DTO.Code,'') as Code,
ISNULL(DTO.U_SEICateg,'') as U_SEICateg,
ISNULL(DTO.U_SEIActi,'') as U_SEIActi,
ISNULL(DTO.U_SEIInt,'') as U_SEIInt,
ISNULL(DTO.U_SEIMarca, 0) as U_SEIMarca,
ISNULL(FAB.FirmName, '') as U_SEIMarcaName,
ISNULL(DTO.U_SEIFam, 0) as U_SEIFam,
ISNULL(DTO.U_SEISubFa,'') as U_SEISubFa,
ISNULL(DTO.U_SEIArt,'') as U_SEIArt,
ISNULL(DTO.U_SEIQtt,0) as U_SEIQtt,
ISNULL(RIGHT('0000' + CAST(year(DTO.U_SEIIni) as varchar(4)),4) + RIGHT('00' + CAST(Month(DTO.U_SEIIni) as varchar(4)),2) + RIGHT('00' + CAST(Day(DTO.U_SEIIni)as varchar(4)),2),'20000101') as U_SEIIni,
ISNULL(RIGHT('0000' + CAST(year(DTO.U_SEIFin) as varchar(4)),4) + RIGHT('00' + CAST(Month(DTO.U_SEIFin) as varchar(4)),2) + RIGHT('00' + CAST(Day(DTO.U_SEIFin)as varchar(4)),2),'20991231') as U_SEIFin,
ISNULL(DTO.U_SEIemple,0) as U_SEIemple,
ISNULL(DTO.U_SEIpromo,'') as U_SEIpromo,
ISNULL(DTO.U_SEIPrice,0) as U_SEIPrice,
ISNULL(DTO.U_SEIDesc1,0) as U_SEIDesc1,
ISNULL(DTO.U_SEIDesc2,0) as U_SEIDesc2,
ISNULL(DTO.U_SEIDesc3,0) as U_SEIDesc3,
ISNULL(DTO.U_SEIDesc4,0) as U_SEIDesc4,
ISNULL(DTO.U_SEIDesc5,0) as U_SEIDesc5,
Emp.empID as EmpID
FROM [@SEIDESCUENTOSCLI] AS DTO
LEFT JOIN OITM as Art ON Dto.U_SEIArt = Art.ItemCode
LEFT JOIN OCRD AS CLI ON DTO.U_SEIInt = CLI.CardCode 
LEFT JOIN OMRC AS FAB ON DTO.U_SEIMarca = Fab.FirmCode
LEFT JOIN OHEM AS Emp ON CLI.SlpCode = Emp.salesPrson OR CLI.SlpCode IN (SELECT SEIEmp.U_SEIslphi FROM [@SEIEMPLE] SEIEmp WHERE ISNULL(SEIEmp.U_SEIestad,'') <> '99' AND SEIEmp.U_SEIslppa = Emp.salesPrson)
WHERE GETDATE() BETWEEN U_SEIIni AND U_SEIFin




GO

/**/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_ABONOSPAGO]'))
DROP VIEW [dbo].[TAB_ABONOSPAGO]
GO

CREATE VIEW [dbo].[TAB_ABONOSPAGO]
AS
SELECT     ISNULL(T0.U_SEICobrado, 0) AS Cobrado, ISNULL(T0.U_SEIDocEntry, 0) AS DocEntry, 0 AS IDCobro, ISNULL(T0.U_SEIEmpID, 0) AS EmpID, 
                      ISNULL(RIGHT('0000' + CAST(YEAR(T0.U_SEIFechaCobro) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(T0.U_SEIFechaCobro) AS varchar(4)), 2) 
                      + RIGHT('00' + CAST(DAY(T0.U_SEIFechaCobro) AS varchar(4)), 2), N'20000101') AS FechaCobro, ISNULL(RIGHT('0000' + CAST(YEAR(T0.U_SEIFechaTalon) 
                      AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(T0.U_SEIFechaTalon) AS varchar(4)), 2) + RIGHT('00' + CAST(DAY(T0.U_SEIFechaTalon) AS varchar(4)), 2), N'20000101') 
                      AS FechaVtoTalon, ISNULL(T0.U_SEIMedioCobro, N'') AS MedioCobro, ISNULL(T0.U_SEINumTalon, N'') AS NumTalon, ISNULL(T0.U_SEIVto, 0) AS IDVto, 'S' AS Sincro, 
                      '' AS IncidenciaSincro, ISNULL(RIGHT('0000' + CAST(YEAR(GETDATE()) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(GETDATE()) AS varchar(4)), 2) 
                      + RIGHT('00' + CAST(DAY(GETDATE()) AS varchar(4)), 2), N'20000101') AS FechaSincro, T1.CardCode, T2.CardFName AS CardName, T0.U_SEITipoDoc AS TipoDoc
FROM         dbo.[@COBROSPARCIALES] AS T0 INNER JOIN
                      dbo.ORIN AS T1 ON T0.U_SEIDocEntry = T1.DocEntry INNER JOIN
                      dbo.OCRD AS T2 ON T1.CardCode = T2.CardCode
WHERE     (T0.U_SEITipoDoc = 'A')

GO

/**/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_FACTURASCOBRO]'))
DROP VIEW [dbo].[TAB_FACTURASCOBRO]
GO

/*INNER JOIN INV6 T3 on T0.U_SEIDocEntry = T3.DocEntry AND T0.U_SEIVto = T3.InstlmntID
WHERE T0.U_SEICobrado <> T3.Paid --Que no esten cobrados ya
WHERE NO ESTA CON TESORERIA YA... Se estan borrando de esta tabla al hacer la tesoreria.*/
CREATE VIEW [dbo].[TAB_FACTURASCOBRO]
AS
SELECT     ISNULL(T0.U_SEICobrado, 0) AS Cobrado, ISNULL(T0.U_SEIDocEntry, 0) AS DocEntry, 0 AS IDCobro, ISNULL(T0.U_SEIEmpID, 0) AS EmpID, 
                      ISNULL(RIGHT('0000' + CAST(YEAR(T0.U_SEIFechaCobro) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(T0.U_SEIFechaCobro) AS varchar(4)), 2) 
                      + RIGHT('00' + CAST(DAY(T0.U_SEIFechaCobro) AS varchar(4)), 2), N'20000101') AS FechaCobro, ISNULL(RIGHT('0000' + CAST(YEAR(T0.U_SEIFechaTalon) 
                      AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(T0.U_SEIFechaTalon) AS varchar(4)), 2) + RIGHT('00' + CAST(DAY(T0.U_SEIFechaTalon) AS varchar(4)), 2), N'20000101') 
                      AS FechaVtoTalon, ISNULL(T0.U_SEIMedioCobro, N'') AS MedioCobro, ISNULL(T0.U_SEINumTalon, N'') AS NumTalon, ISNULL(T0.U_SEIVto, 0) AS IDVto, 'S' AS Sincro, 
                      '' AS IncidenciaSincro, ISNULL(RIGHT('0000' + CAST(YEAR(GETDATE()) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(GETDATE()) AS varchar(4)), 2) 
                      + RIGHT('00' + CAST(DAY(GETDATE()) AS varchar(4)), 2), N'20000101') AS FechaSincro, T1.CardCode, T2.CardFName AS CardName, T0.U_SEITipoDoc AS TipoDoc
FROM         dbo.[@COBROSPARCIALES] AS T0 INNER JOIN
                      dbo.OINV AS T1 ON T0.U_SEIDocEntry = T1.DocEntry INNER JOIN
                      dbo.OCRD AS T2 ON T1.CardCode = T2.CardCode
WHERE     (T0.U_SEITipoDoc = 'F')

GO

/**/


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_DOCUMENTOSCOBROPAGO]'))
DROP VIEW [dbo].[TAB_DOCUMENTOSCOBROPAGO]
GO

CREATE VIEW [dbo].[TAB_DOCUMENTOSCOBROPAGO]
AS
SELECT     Cobrado, DocEntry, IDCobro, EmpID, FechaCobro, FechaVtoTalon, MedioCobro, NumTalon, IDVto, Sincro, IncidenciaSincro, FechaSincro, CardCode, CardName, 
                      TipoDoc
FROM         dbo.TAB_FACTURASCOBRO
Union all
SELECT     Cobrado, DocEntry, IDCobro, EmpID, FechaCobro, FechaVtoTalon, MedioCobro, NumTalon, IDVto, Sincro, IncidenciaSincro, FechaSincro, CardCode, CardName, 
                      TipoDoc
FROM         dbo.TAB_ABONOSPAGO

GO

/**/ 

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_ABONOSPENDIENTESCAB]'))
DROP VIEW [dbo].[TAB_ABONOSPENDIENTESCAB]
GO

CREATE VIEW [dbo].[TAB_ABONOSPENDIENTESCAB]
AS
SELECT     ISNULL(T0.DocEntry, 0) AS DocEntry, ISNULL(T0.DocNum, N'') AS DocNum, ISNULL(T0.CardCode, N'') AS CardCode, ISNULL(T0.CardName, N'') AS CardName, 
                      CONVERT(char(10), ISNULL(T0.DocDate, N'20000101'), 112) AS DocDate, CONVERT(char(10), ISNULL(T0.DocDueDate, N'20000101'), 112) AS DocDueDate, 
                      ISNULL(T2.EmpID, N'') AS EmpID, ISNULL(T2.ViaPago, N'') AS ViaPago, 'S' AS Seleccionable, ISNULL(T0.Comments, N'') AS Comentarios, T0.DocTotal AS Total, 
                      T0.DocTotal - ISNULL
                          ((SELECT     SUM(Paid) AS Expr1
                              FROM         dbo.RIN6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) AS PendienteFra, ISNULL
                          ((SELECT     SUM(Paid) AS Expr1
                              FROM         dbo.RIN6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) AS Cobrado
FROM         dbo.ORIN AS T0 INNER JOIN
                      dbo.RIN6 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
                      dbo.TAB_CLIENTES AS T2 ON T2.Codigo = T0.CardCode
WHERE     (T1.Status = 'O') AND (T1.ObjType = 14) AND (T1.DocEntry NOT IN
                          (SELECT     NumDocumento
                            FROM          MOV_TLYERP.dbo.TESORERIADET_TLY AS X
                            WHERE      (TipoDocumento = 'A'))) AND (ISNULL(T1.U_SEIpdape, 'N') = 'N' OR
                      ISNULL
                          ((SELECT     SUM(Paid) AS Expr1
                              FROM         dbo.RIN6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) <> T0.DocTotal AND ISNULL(T1.U_SEIpdape, 'N') = 'Y')

GO

/**/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_ABONOSPENDIENTESLIN]'))
DROP VIEW [dbo].[TAB_ABONOSPENDIENTESLIN]
GO

CREATE VIEW [dbo].[TAB_ABONOSPENDIENTESLIN]
AS
SELECT     ISNULL(T0.DocEntry, 0) AS DocEntry, ISNULL(T1.InstlmntID, 0) AS IDVto, CONVERT(char(10), ISNULL(T1.DueDate, '20000101'), 112) AS FechaVto, ISNULL(T1.InsTotal, 
                      0) AS TotalVto, ISNULL(T1.Paid, 0) AS Pagado, ISNULL(T2.EmpID, 0) AS EmpID
FROM         dbo.ORIN AS T0 INNER JOIN
                      dbo.RIN6 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
                      dbo.TAB_ABONOSPENDIENTESCAB AS T2 ON T0.DocEntry = T2.DocEntry
WHERE     (T1.Status = 'O') AND (T1.ObjType = 14)

GO

/**/


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_FACTURASPENDIENTESCAB]'))
DROP VIEW [dbo].[TAB_FACTURASPENDIENTESCAB]
GO

CREATE VIEW [dbo].[TAB_FACTURASPENDIENTESCAB]
AS
SELECT     ISNULL(T0.DocEntry, 0) AS DocEntry, ISNULL(T0.DocNum, N'') AS DocNum, ISNULL(T0.CardCode, N'') AS CardCode, ISNULL(T0.CardName, N'') AS CardName, 
                      CONVERT(char(10), ISNULL(T0.DocDate, N'20000101'), 112) AS DocDate, CONVERT(char(10), ISNULL(T0.DocDueDate, N'20000101'), 112) AS DocDueDate, 
                      ISNULL(T2.EmpID, N'') AS EmpID, ISNULL(T2.ViaPago, N'') AS ViaPago, CASE WHEN ISNULL(T0.Comments, N'') 
                      LIKE '%IMPAGAT%' THEN 'S' ELSE CASE ISNULL(T2.ViaPago, '') 
                      WHEN 'Reposici�n Agente' THEN 'S' WHEN 'Contado Riguroso' THEN 'S' WHEN 'Contado' THEN 'S' WHEN 'Tal�n' THEN 'S' ELSE 'N' END END AS Seleccionable, 
                      ISNULL(T0.Comments, N'') AS Comentarios, T0.DocTotal AS Total, T0.DocTotal - ISNULL
                          ((SELECT     SUM(Paid) AS Expr1
                              FROM         dbo.INV6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) AS PendienteFra, ISNULL
                          ((SELECT     SUM(Paid) AS Expr1
                              FROM         dbo.INV6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) AS Cobrado
FROM         dbo.OINV AS T0 INNER JOIN
                      dbo.INV6 AS T1 ON T0.DocEntry = T1.DocEntry INNER JOIN
                      dbo.TAB_CLIENTES AS T2 ON T2.Codigo = T0.CardCode
WHERE     (T1.Status = 'O') AND (T1.ObjType = 13) 
AND (T1.DocEntry NOT IN    (SELECT     NumDocumento   FROM  MOV_TLYERP.dbo.TESORERIADET_TLY AS X)) 
AND (
	ISNULL(T1.U_SEIpdape, N'N') = 'N' 
	OR
    ISNULL((SELECT SUM(Paid) AS Expr1
                              FROM         dbo.INV6 AS X
                              WHERE     (DocEntry = T0.DocEntry)), 0) <> T0.DocTotal AND ISNULL(T1.U_SEIpdape, N'N') = 'Y')


GO

/**/


IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_FACTURASPENDIENTESLIN]'))
DROP VIEW [dbo].[TAB_FACTURASPENDIENTESLIN]
GO

CREATE VIEW [dbo].[TAB_FACTURASPENDIENTESLIN]
AS SELECT
	
	
	 ISNULL(T0.DocEntry,0) as DocEntry,
	 ISNULL(T1.InstlmntID,0) as IDVto,
	 CONVERT(char(10), ISNULL(T1.DueDate,'20000101'), 112) as FechaVto,
	 ISNULL(T1.InsTotal,0) as TotalVto,
	 ISNULL(T1.Paid,0) as Cobrado,
	 ISNULL(T2.EMPID,0) as EmpID

FROM OINV T0 
INNER JOIN INV6 T1 ON T0.DocEntry = T1.DocEntry 
INNER JOIN TAB_FACTURASPENDIENTESCAB T2 ON T0.DocEntry = T2.DocEntry
WHERE T1.Status = 'O' 
AND T1.ObjType = 13

GO

/**/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_DOCUMENTOSPENDIENTESCAB]'))
DROP VIEW [dbo].[TAB_DOCUMENTOSPENDIENTESCAB]
GO

CREATE VIEW [dbo].[TAB_DOCUMENTOSPENDIENTESCAB]
AS

SELECT TOP (100) PERCENT *
FROM (
	SELECT 'F' AS TipoDoc,* FROM TAB_FACTURASPENDIENTESCAB
	UNION ALL 
	SELECT 'A' AS TipoDoc,* FROM TAB_ABONOSPENDIENTESCAB) 
	DocPendientes


GO

/**/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TAB_DOCUMENTOSPENDIENTESLIN]'))
DROP VIEW [dbo].[TAB_DOCUMENTOSPENDIENTESLIN]
GO

CREATE VIEW [dbo].[TAB_DOCUMENTOSPENDIENTESLIN]
AS

SELECT TOP (100) PERCENT *
FROM (
	SELECT 'F' AS TipoDoc,* FROM TAB_FACTURASPENDIENTESLIN
	UNION ALL 
	SELECT 'A' AS TipoDoc,* FROM TAB_ABONOSPENDIENTESLIN) 
	DocPendientes



GO











/****** Object:  View [dbo].[TAB_FAVORITOS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_FAVORITOS]
AS

select 



	 ISNULL([U_SEIEmpID],0) as EmpID
      ,ISNULL([U_SEIItemCode] ,'') as ItemCode
      ,'S' as Sincro
      ,'' as ErrorSincro 



from [@SEIFAVORITOS] T0

GO

/****** Object:  View [dbo].[TAB_HISTORICODOCUMENTOSCAB]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[TAB_HISTORICODOCUMENTOSCAB]
	AS
	SELECT 	
	'PEDIDO' as TipoDoc,
	Doc.CardCode as CardCode,
	Doc.CardName as CardName,	
	ISNULL(Doc.U_SEIrutid,'') as RutaID,
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDate)as varchar(4)),2),'20000101') as DocDate,
	Vis.empID as EmpID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDueDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDueDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDueDate)as varchar(4)),2),'20000101') as FechaEntrega,
	ISNULL(RIGHT('0000' + CAST(Doc.DocTime as varchar(4)),4),'0000') as HoraCreacion,		
	Doc.DocNum as DocNumSAP,
	Doc.DocEntry as DocEntrySAP,
	'0' as Longitud,
	'0' as Latitud,
	ISNULL(Doc.Comments,'') as Comentarios,
	ISNULL(Doc.NumAtCard,'') as NumDocCliente,
	ISNULL(Via.PymntGroup,'') as CondicionPago,
	ISNULL(Doc.DiscPrcnt,0) as DtoGralPorcent,
	ISNULL(Doc.DiscSum,0) as DtoGralEuros,
	Doc.DocTotal - ISNULL(Doc.VatSum,0) + ISNULL(Doc.DiscSum,0) + abs(ISNULL(Doc.RoundDif,0))as TotalBruto,
	Doc.DocTotal as TotalNeto,
	Doc.DocTotal as TotalIVAIncluido,
	'N' as Retenido,
	ISNULL(Vis.RecargoEquiv,'N') as RecargoEquivalencia,
	CASE WHEN ISNULL(Lin.TargetType,'') <> '' THEN 'S' ELSE 'N' END as Entregado,
	CASE WHEN ISNULL(Alb.TargetType,'') <> '' THEN 'S' ELSE 'N' END  as Facturado,
	ISNULL(Dir.Address,'') as IDDireccion,
	ISNULL(Dir.Street,'') as Direccion,
	ISNULL(Dir.County,'') as Poblacion,
	ISNULL(Dir.City,'') as Ciudad,
	ISNULL(Dir.ZipCode,'') as CP,
	ISNULL(Dir.County,'') as Provincia,
	ISNULL(Doc.NumAtCard,'') as NumeroPedidoCliente
	FROM ORDR Doc
	LEFT JOIN RDR1 Lin ON Doc.DocEntry = Lin.DocEntry AND Lin.LineNum = 0 AND (Lin.TargetType = '15' OR Lin.TargetType = '13') 
	LEFT JOIN DLN1 Alb ON Lin.TrgetEntry = Alb.DocEntry AND Alb.LineNum = 0 AND Alb.TargetType = '13'
	INNER JOIN TAB_Clientes Vis ON Vis.Codigo = Doc.CardCode 	
	LEFT JOIN CRD1 Dir on Doc.CardCode = Dir.CardCode AND Doc.ShipToCode = Dir.Address
	LEFT JOIN OSLP Slp on Doc.SlpCode = Slp.SlpCode
	LEFT JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
	LEFT JOIN OCTG Via on Via.GroupNum = Doc.GroupNum
	WHERE DATEDIFF(MONTH,Doc.DocDate,GETDATE()) <= 3
	
	UNION ALL
	
	SELECT 	
	'ALBARAN' as TipoDoc,
	Doc.CardCode as CardCode,
	Doc.CardName as CardName,	
	ISNULL(Doc.U_SEIrutid,'') as RutaID,
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDate)as varchar(4)),2),'20000101') as DocDate,
	Vis.empID as EmpID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDueDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDueDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDueDate)as varchar(4)),2),'20000101') as FechaEntrega,
	ISNULL(RIGHT('0000' + CAST(Doc.DocTime as varchar(4)),4),'0000') as HoraCreacion,		
	Doc.DocNum as DocNumSAP,
	Doc.DocEntry as DocEntrySAP,
	'0' as Longitud,
	'0' as Latitud,
	ISNULL(Doc.Comments,'') as Comentarios,
	ISNULL(Doc.NumAtCard,'') as NumDocCliente,
	ISNULL(Via.PymntGroup,'') as CondicionPago,
	ISNULL(Doc.DiscPrcnt,0) as DtoGralPorcent,
	ISNULL(Doc.DiscSum,0) as DtoGralEuros,
	Doc.DocTotal - ISNULL(Doc.VatSum,0) + ISNULL(Doc.DiscSum,0) + abs(ISNULL(Doc.RoundDif,0))as TotalBruto,
	Doc.DocTotal as TotalNeto,
	Doc.DocTotal as TotalIVAIncluido,
	'N' as Retenido,
	ISNULL(Vis.RecargoEquiv,'N') as RecargoEquivalencia,
	'S' as Entregado,
	CASE WHEN ISNULL(Lin.TargetType,'') <> '' THEN 'S' ELSE 'N' END as Facturado,	
	ISNULL(Dir.Address,'') as IDDireccion,
	ISNULL(Dir.Street,'') as Direccion,
	ISNULL(Dir.County,'') as Poblacion,
	ISNULL(Dir.City,'') as Ciudad,
	ISNULL(Dir.ZipCode,'') as CP,
	ISNULL(Dir.County,'') as Provincia,
	ISNULL(Doc.NumAtCard,'') as NumeroPedidoCliente
	FROM ODLN Doc
	LEFT JOIN DLN1 Lin ON Doc.DocEntry = Lin.DocEntry AND Lin.LineNum = 0 AND Lin.TargetType = '13'
	INNER JOIN TAB_Clientes Vis ON Vis.Codigo = Doc.CardCode 	
	LEFT JOIN CRD1 Dir on Doc.CardCode = Dir.CardCode AND Doc.ShipToCode = Dir.Address
	LEFT JOIN OSLP Slp on Doc.SlpCode = Slp.SlpCode
	LEFT JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
	LEFT JOIN OCTG Via on Via.GroupNum = Doc.GroupNum
	WHERE DATEDIFF(MONTH,Doc.DocDate,GETDATE()) <= 3

	UNION ALL
	
	SELECT 	
	'DEVOLUCION' as TipoDoc,
	Doc.CardCode as CardCode,
	Doc.CardName as CardName,	
	ISNULL(Doc.U_SEIrutid,'') as RutaID,
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDate)as varchar(4)),2),'20000101') as DocDate,
	Vis.empID as EmpID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDueDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDueDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDueDate)as varchar(4)),2),'20000101') as FechaEntrega,
	ISNULL(RIGHT('0000' + CAST(Doc.DocTime as varchar(4)),4),'0000') as HoraCreacion,		
	Doc.DocNum as DocNumSAP,
	Doc.DocEntry as DocEntrySAP,
	'0' as Longitud,
	'0' as Latitud,
	ISNULL(Doc.Comments,'') as Comentarios,
	ISNULL(Doc.NumAtCard,'') as NumDocCliente,
	ISNULL(Via.PymntGroup,'') as CondicionPago,
	ISNULL(Doc.DiscPrcnt,0) as DtoGralPorcent,
	ISNULL(Doc.DiscSum,0) as DtoGralEuros,
	Doc.DocTotal - ISNULL(Doc.VatSum,0) + ISNULL(Doc.DiscSum,0) + abs(ISNULL(Doc.RoundDif,0))as TotalBruto,
	Doc.DocTotal as TotalNeto,
	Doc.DocTotal as TotalIVAIncluido,
	'N' as Retenido,
	ISNULL(Vis.RecargoEquiv,'N') as RecargoEquivalencia,
	'N' as Entregado,
	'N' as Facturado,
	ISNULL(Dir.Address,'') as IDDireccion,
	ISNULL(Dir.Street,'') as Direccion,
	ISNULL(Dir.County,'') as Poblacion,
	ISNULL(Dir.City,'') as Ciudad,
	ISNULL(Dir.ZipCode,'') as CP,
	ISNULL(Dir.County,'') as Provincia,
	ISNULL(Doc.NumAtCard,'') as NumeroPedidoCliente
	FROM ORDN Doc	
	INNER JOIN TAB_Clientes Vis ON Vis.Codigo = Doc.CardCode 	
	LEFT JOIN CRD1 Dir on Doc.CardCode = Dir.CardCode AND Doc.ShipToCode = Dir.Address
	LEFT JOIN OSLP Slp on Doc.SlpCode = Slp.SlpCode
	LEFT JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
	LEFT JOIN OCTG Via on Via.GroupNum = Doc.GroupNum
	WHERE DATEDIFF(MONTH,Doc.DocDate,GETDATE()) <= 3

	UNION ALL

	SELECT 	
	'FACTURA' as TipoDoc,
	Doc.CardCode as CardCode,
	Doc.CardName as CardName,
	ISNULL(Doc.U_SEIrutid,'') as RutaID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDate)as varchar(4)),2),'20000101') as DocDate,
	Vis.empID as EmpID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDueDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDueDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDueDate)as varchar(4)),2),'20000101') as FechaEntrega,
	ISNULL(RIGHT('0000' + CAST(Doc.DocTime as varchar(4)),4),'0000') as HoraCreacion,		
	Doc.DocNum as DocNumSAP,
	Doc.DocEntry as DocEntrySAP,
	'0' as Longitud,
	'0' as Latitud,
	ISNULL(Doc.Comments,'') as Comentarios,
	ISNULL(Doc.NumAtCard,'') as NumDocCliente,
	ISNULL(Via.PymntGroup,'') as CondicionPago,
	ISNULL(Doc.DiscPrcnt,0) as DtoGralPorcent,
	ISNULL(Doc.DiscSum,0) as DtoGralEuros,
	Doc.DocTotal - ISNULL(Doc.VatSum,0) + ISNULL(Doc.DiscSum,0) + abs(ISNULL(Doc.RoundDif,0))as TotalBruto,
	Doc.DocTotal as TotalNeto,
	Doc.DocTotal as TotalIVAIncluido,
	'N' as Retenido,
	ISNULL(Vis.RecargoEquiv,'N') as RecargoEquivalencia,
	'S' as Entregado,
	'S' as Facturado,
	ISNULL(Dir.Address,'') as IDDireccion,
	ISNULL(Dir.Street,'') as Direccion,
	ISNULL(Dir.County,'') as Poblacion,
	ISNULL(Dir.City,'') as Ciudad,
	ISNULL(Dir.ZipCode,'') as CP,
	ISNULL(Dir.County,'') as Provincia,
	ISNULL(Doc.NumAtCard,'') as NumeroPedidoCliente
	FROM OINV Doc	
	INNER JOIN TAB_Clientes Vis ON Vis.Codigo = Doc.CardCode 	
	LEFT JOIN CRD1 Dir on Doc.CardCode = Dir.CardCode AND Doc.ShipToCode = Dir.Address
	LEFT JOIN OSLP Slp on Doc.SlpCode = Slp.SlpCode
	LEFT JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
	LEFT JOIN OCTG Via on Via.GroupNum = Doc.GroupNum
	WHERE DATEDIFF(MONTH,Doc.DocDate,GETDATE()) <= 3
	
	UNION ALL
	
	SELECT 	
	'ABONO' as TipoDoc,
	Doc.CardCode as CardCode,
	Doc.CardName as CardName,	
	ISNULL(Doc.U_SEIrutid,'') as RutaID,
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDate)as varchar(4)),2),'20000101') as DocDate,
	Vis.empID as EmpID,	
	ISNULL(RIGHT('0000' + CAST(year(Doc.DocDueDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Doc.DocDueDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Doc.DocDueDate)as varchar(4)),2),'20000101') as FechaEntrega,
	ISNULL(RIGHT('0000' + CAST(Doc.DocTime as varchar(4)),4),'0000') as HoraCreacion,		
	Doc.DocNum as DocNumSAP,
	Doc.DocEntry as DocEntrySAP,
	'0' as Longitud,
	'0' as Latitud,
	ISNULL(Doc.Comments,'') as Comentarios,
	ISNULL(Doc.NumAtCard,'') as NumDocCliente,
	ISNULL(Via.PymntGroup,'') as CondicionPago,
	ISNULL(Doc.DiscPrcnt,0) as DtoGralPorcent,
	ISNULL(Doc.DiscSum,0) as DtoGralEuros,
	Doc.DocTotal - ISNULL(Doc.VatSum,0) + ISNULL(Doc.DiscSum,0) + abs(ISNULL(Doc.RoundDif,0))as TotalBruto,
	Doc.DocTotal as TotalNeto,
	Doc.DocTotal as TotalIVAIncluido,
	'N' as Retenido,
	ISNULL(Vis.RecargoEquiv,'N') as RecargoEquivalencia,
	'S' as Entregado,
	'S' as Facturado,
	ISNULL(Dir.Address,'') as IDDireccion,
	ISNULL(Dir.Street,'') as Direccion,
	ISNULL(Dir.County,'') as Poblacion,
	ISNULL(Dir.City,'') as Ciudad,
	ISNULL(Dir.ZipCode,'') as CP,
	ISNULL(Dir.County,'') as Provincia,
	ISNULL(Doc.NumAtCard,'') as NumeroPedidoCliente
	FROM ORIN Doc	
	INNER JOIN TAB_Clientes Vis ON Vis.Codigo = Doc.CardCode 	
	LEFT JOIN CRD1 Dir on Doc.CardCode = Dir.CardCode AND Doc.ShipToCode = Dir.Address
	LEFT JOIN OSLP Slp on Doc.SlpCode = Slp.SlpCode
	LEFT JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
	LEFT JOIN OCTG Via on Via.GroupNum = Doc.GroupNum
	WHERE DATEDIFF(MONTH,Doc.DocDate,GETDATE()) <= 3








GO

/****** Object:  View [dbo].[TAB_HISTORICODOCUMENTOSLIN]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








	CREATE VIEW [dbo].[TAB_HISTORICODOCUMENTOSLIN]
	AS
	SELECT 
	Cab.TipoDoc as TipoDoc,
	Cab.DocEntrySAP as DocEntrySAP,	
	Lin.LineNum as LineNumSAP,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	ISNULL(Lin.Factor1,0) as CantidadFactor,
	ISNULL(Lin.Quantity,0) as CantidadReal,
	ISNULL(Lin.Price,0) as Precio,
	CASE WHEN ISNULL(Lin.Quantity, 0) <> ISNULL(Lin.Factor1,0) * ISNULL(Art.SalFactor1,0) THEN 'U' ELSE 'C' END as TipoUnidadesCajas,
	ISNULL(Lin.unitMsr,'') as TipoUnidades,
	ISNULL(Lin.U_SEIDesc1,0) as Dto1,
	ISNULL(Lin.U_SEIDesc2,0) as Dto2,
	ISNULL(Lin.U_SEIDesc3,0) as Dto3,
	ISNULL(Lin.U_SEIDesc4,0) as Dto4,
	ISNULL(Lin.U_SEIDesc5,0) as Dto5,
	ISNULL(Lin.DiscPrcnt,0) as dtoTotalCalculado,
	0 as dtoManual,
	ISNULL(Lin.U_SEIDescL,'') as CodigoDescuento,
	ISNULL(Lin.PriceBefDi,0) * ISNULL(Lin.Quantity,0) as TotalSinDescuentos,
	ISNULL(Lin.LineTotal,0) as TotalConDescuentos,
	ISNULL(Lin.VatPrcnt,0) as IVAPorcentajeLinea,
	ISNULL(Lin.EquVatPer,0) as IVARecargoPorcentajeLinea,	
	Cab.EmpID as EmpID	
	FROM RDR1 Lin
	INNER JOIN TAB_HISTORICODOCUMENTOSCAB Cab on Cab.DocEntrySAP = Lin.DocEntry AND Cab.TipoDoc = 'PEDIDO'
	INNER JOIN OITM Art on Lin.ItemCode = Art.ItemCode
	INNER JOIN TAB_ARTICULOS Vis on Art.ItemCode = Vis.ItemCode
	UNION ALL
	SELECT 
	Cab.TipoDoc as TipoDoc,
	Cab.DocEntrySAP as DocEntrySAP,	
	Lin.LineNum as LineNumSAP,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	ISNULL(Lin.Factor1,0) as CantidadFactor,
	ISNULL(Lin.Quantity,0) as CantidadReal,
	ISNULL(Lin.Price,0) as Precio,
	CASE WHEN ISNULL(Lin.Quantity, 0) <> ISNULL(Lin.Factor1,0) * ISNULL(Lin.Factor2,0) * ISNULL(Lin.Factor3,0) THEN 'U' ELSE 'C' END as TipoUnidadesCajas,
	ISNULL(Lin.unitMsr,'') as TipoUnidades,
	ISNULL(Lin.U_SEIDesc1,0) as Dto1,
	ISNULL(Lin.U_SEIDesc2,0) as Dto2,
	ISNULL(Lin.U_SEIDesc3,0) as Dto3,
	ISNULL(Lin.U_SEIDesc4,0) as Dto4,
	ISNULL(Lin.U_SEIDesc5,0) as Dto5,
	ISNULL(Lin.DiscPrcnt,0) as dtoTotalCalculado,
	0 as dtoManual,
	ISNULL(Lin.U_SEIDescL,'') as CodigoDescuento,
	ISNULL(Lin.PriceBefDi,0) * ISNULL(Lin.Quantity,0) as TotalSinDescuentos,
	ISNULL(Lin.LineTotal,0) as TotalConDescuentos,
	ISNULL(Lin.VatPrcnt,0) as IVAPorcentajeLinea,
	ISNULL(Lin.EquVatPer,0) as IVARecargoPorcentajeLinea,	
	Cab.EmpID as EmpID	
	FROM DLN1 Lin
	INNER JOIN TAB_HISTORICODOCUMENTOSCAB Cab on Cab.DocEntrySAP = Lin.DocEntry AND Cab.TipoDoc = 'ALBARAN'
	INNER JOIN OITM Art on Lin.ItemCode = Art.ItemCode
	INNER JOIN TAB_ARTICULOS Vis on Art.ItemCode = Vis.ItemCode
	UNION ALL
	SELECT 
	Cab.TipoDoc as TipoDoc,
	Cab.DocEntrySAP as DocEntrySAP,	
	Lin.LineNum as LineNumSAP,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	ISNULL(Lin.Factor1,0) as CantidadFactor,
	ISNULL(Lin.Quantity,0) as CantidadReal,
	ISNULL(Lin.Price,0) as Precio,
	CASE WHEN ISNULL(Lin.Quantity, 0) <> ISNULL(Lin.Factor1,0) * ISNULL(Lin.Factor2,0) * ISNULL(Lin.Factor3,0) THEN 'U' ELSE 'C' END as TipoUnidadesCajas,
	ISNULL(Lin.unitMsr,'') as TipoUnidades,
	ISNULL(Lin.U_SEIDesc1,0) as Dto1,
	ISNULL(Lin.U_SEIDesc2,0) as Dto2,
	ISNULL(Lin.U_SEIDesc3,0) as Dto3,
	ISNULL(Lin.U_SEIDesc4,0) as Dto4,
	ISNULL(Lin.U_SEIDesc5,0) as Dto5,
	ISNULL(Lin.DiscPrcnt,0) as dtoTotalCalculado,
	0 as dtoManual,
	ISNULL(Lin.U_SEIDescL,'') as CodigoDescuento,
	ISNULL(Lin.PriceBefDi,0) * ISNULL(Lin.Quantity,0) as TotalSinDescuentos,
	ISNULL(Lin.LineTotal,0) as TotalConDescuentos,
	ISNULL(Lin.VatPrcnt,0) as IVAPorcentajeLinea,
	ISNULL(Lin.EquVatPer,0) as IVARecargoPorcentajeLinea,	
	Cab.EmpID as EmpID	
	FROM RDN1 Lin
	INNER JOIN TAB_HISTORICODOCUMENTOSCAB Cab on Cab.DocEntrySAP = Lin.DocEntry AND Cab.TipoDoc = 'DEVOLUCION'
	INNER JOIN OITM Art on Lin.ItemCode = Art.ItemCode
	INNER JOIN TAB_ARTICULOS Vis on Art.ItemCode = Vis.ItemCode
	UNION ALL
	SELECT 
	Cab.TipoDoc as TipoDoc,
	Cab.DocEntrySAP as DocEntrySAP,	
	Lin.LineNum as LineNumSAP,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	ISNULL(Lin.Factor1,0) as CantidadFactor,
	ISNULL(Lin.Quantity,0) as CantidadReal,
	ISNULL(Lin.Price,0) as Precio,
	CASE WHEN ISNULL(Lin.Quantity, 0) <> ISNULL(Lin.Factor1,0) * ISNULL(Lin.Factor2,0) * ISNULL(Lin.Factor3,0) THEN 'U' ELSE 'C' END as TipoUnidadesCajas,
	ISNULL(Lin.unitMsr,'') as TipoUnidades,
	ISNULL(Lin.U_SEIDesc1,0) as Dto1,
	ISNULL(Lin.U_SEIDesc2,0) as Dto2,
	ISNULL(Lin.U_SEIDesc3,0) as Dto3,
	ISNULL(Lin.U_SEIDesc4,0) as Dto4,
	ISNULL(Lin.U_SEIDesc5,0) as Dto5,
	ISNULL(Lin.DiscPrcnt,0) as dtoTotalCalculado,
	0 as dtoManual,
	ISNULL(Lin.U_SEIDescL,'') as CodigoDescuento,
	ISNULL(Lin.PriceBefDi,0) * ISNULL(Lin.Quantity,0) as TotalSinDescuentos,
	ISNULL(Lin.LineTotal,0) as TotalConDescuentos,
	ISNULL(Lin.VatPrcnt,0) as IVAPorcentajeLinea,
	ISNULL(Lin.EquVatPer,0) as IVARecargoPorcentajeLinea,	
	Cab.EmpID as EmpID	
	FROM INV1 Lin
	INNER JOIN TAB_HISTORICODOCUMENTOSCAB Cab on Cab.DocEntrySAP = Lin.DocEntry AND Cab.TipoDoc = 'FACTURA'
	INNER JOIN OITM Art on Lin.ItemCode = Art.ItemCode
	INNER JOIN TAB_ARTICULOS Vis on Art.ItemCode = Vis.ItemCode
	UNION ALL
	SELECT 
	Cab.TipoDoc as TipoDoc,
	Cab.DocEntrySAP as DocEntrySAP,	
	Lin.LineNum as LineNumSAP,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	ISNULL(Lin.Factor1,0) as CantidadFactor,
	ISNULL(Lin.Quantity,0) as CantidadReal,
	ISNULL(Lin.Price,0) as Precio,
	CASE WHEN ISNULL(Lin.Quantity, 0) <> ISNULL(Lin.Factor1,0) * ISNULL(Lin.Factor2,0) * ISNULL(Lin.Factor3,0) THEN 'U' ELSE 'C' END as TipoUnidadesCajas,
	ISNULL(Lin.unitMsr,'') as TipoUnidades,
	ISNULL(Lin.U_SEIDesc1,0) as Dto1,
	ISNULL(Lin.U_SEIDesc2,0) as Dto2,
	ISNULL(Lin.U_SEIDesc3,0) as Dto3,
	ISNULL(Lin.U_SEIDesc4,0) as Dto4,
	ISNULL(Lin.U_SEIDesc5,0) as Dto5,
	ISNULL(Lin.DiscPrcnt,0) as dtoTotalCalculado,
	0 as dtoManual,
	ISNULL(Lin.U_SEIDescL,'') as CodigoDescuento,
	ISNULL(Lin.PriceBefDi,0) * ISNULL(Lin.Quantity,0) as TotalSinDescuentos,
	ISNULL(Lin.LineTotal,0) as TotalConDescuentos,
	ISNULL(Lin.VatPrcnt,0) as IVAPorcentajeLinea,
	ISNULL(Lin.EquVatPer,0) as IVARecargoPorcentajeLinea,	
	Cab.EmpID as EmpID	
	FROM RIN1 Lin
	INNER JOIN TAB_HISTORICODOCUMENTOSCAB Cab on Cab.DocEntrySAP = Lin.DocEntry AND Cab.TipoDoc = 'ABONO'
	INNER JOIN OITM Art on Lin.ItemCode = Art.ItemCode
	INNER JOIN TAB_ARTICULOS Vis on Art.ItemCode = Vis.ItemCode








GO

/****** Object:  View [dbo].[TAB_MENSAJESBAJADA]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_MENSAJESBAJADA]
AS
SELECT   
C.Code, 
C.Priority, 
rtrim(replace(replace(replace(cast(C.Subject as varchar(100)),CHAR(10),''),CHAR(13),' '),CHAR(9),' ')) as Subject, 
rtrim(replace(replace(replace(cast(C.UserText as varchar(160)),CHAR(10),''),CHAR(13),' '),CHAR(9),' '))  as UserText, 
Emp.empID as EmpId, 
ISNULL(L.WasRead, 'N') as WasRead,
ISNULL(U.USER_CODE, '') AS De,
'N' AS Eliminado
FROM OALR AS C 
INNER JOIN dbo.OAIB AS L ON C.Code = L.AlertCode 
INNER JOIN dbo.OUSR AS U ON U.USERID = C.UserSign
INNER JOIN OHEM Emp ON Emp.USERID = L.UserSign
WHERE (L.WasRead = 'N') AND (L.Deleted = 'N') AND (C.Type = 'M')




GO

/****** Object:  View [dbo].[TAB_NOTAS]    Script Date: 04/07/2016 19:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[TAB_NOTAS]
AS
select 
	  ISNULL([U_SEIIDNota],0) as Codigo
      ,ISNULL([U_SEITexto],'') as Texto
      ,ISNULL([U_SEIEmpID],0) as EmpID
      ,'S' as Sincro
      ,20010101 as FechaSincro
      ,'' as IncidenciaSincro
from [@SEINOTAS] T0
GO


/**/

CREATE VIEW [dbo].[TAB_USUARIOS]
AS
SELECT
T3.empID as EmpID,
ISNULL(T3.firstName,'') + ' ' + ISNULL(T3.middleName,'') + ' ' + ISNULL(T3.lastName,'') as Name,
--ISNULL(T3.U_SEIPass,'') as Pass, crear campo para password
'1234' as Pass,
--CASE WHEN ISNULL(U_SEIEmp1,'0') <> '0' THEN 'S' ELSE 
--CASE WHEN ISNULL(U_SEIEmp2,'0') <> '0' THEN 'S' ELSE 
--'N' END END as SuperUser, crear campo para permisos Super Usuario
'N' as SuperUser, 
--Crear Campo
--CASE WHEN ISNULL(U_SEIEmp1,'0') <> '0' THEN U_SEIEmp1 ELSE '-1' END as EmpIDExtra1, No sabemos que es
--CASE WHEN ISNULL(U_SEIEmp2,'0') <> '0' THEN U_SEIEmp2 ELSE '-1' END as EmpIDExtra2, No sabemos que es
-1 as EmpIDExtra1,
-1 as EmpIDExtra2,
ISNULL(T3.userId,'0') as SAPUser,
ISNULL(T3.salesPrson, 0) as SlpCode,
--ISNULL(T3.U_SEICamRuta,'N') as CambioRuta, Crear campo
'N' as CambioRuta,
-- crear campo
5000 as ObjetivoMensual,
ISNULL((SELECT COUNT(P1.DocEntry) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND P1.docdate = CAST(GETDATE() AS DATE)),0) as  NumeroPedidosSAPDia,
ISNULL((SELECT COUNT(P1.DocEntry) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND MONTH(P1.DocDate) = MONTH(getdate()) AND YEAR(P1.docdate) = YEAR(GETDATE())),0) as  NumeroPedidosSAPMes,
ISNULL((SELECT SUM(P1.DocTotal) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND P1.docdate = CAST(GETDATE() AS DATE)),0) as  ImportePedidosSAPDia,
ISNULL((SELECT SUM(P1.DocTotal) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND MONTH(P1.DocDate) = MONTH(getdate()) AND YEAR(P1.docdate) = YEAR(GETDATE())),0) as  ImportePedidosSAPMes,
--ISNULL((SELECT COUNT(CP.Code) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND CAST(CP.U_SEIFechaCobro as DATE) = CAST(GETDATE() AS DATE)),0) as NumeroCobrosSAPDia, se deben sacar los cobros por dia
99 as NumeroCobrosSAPDia,
--ISNULL((SELECT COUNT(CP.Code) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND MONTH(CAST(CP.U_SEIFechaCobro as DATE)) = MONTH(CAST(GETDATE() AS DATE)) AND YEAR(CAST(CP.U_SEIFechaCobro as DATE)) = YEAR(CAST(GETDATE() AS DATE))),0) as NumeroCobrosSAPMes, por mes
99 as NumeroCobrosSAPMes, 
--ISNULL((SELECT SUM(CP.U_SEICobrado) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND CAST(CP.U_SEIFechaCobro as DATE) = CAST(GETDATE() AS DATE)),0) as ImporteCobrosSAPDia, el importe por dia
9999 as ImporteCobrosSAPDia,
--ISNULL((SELECT SUM(CP.U_SEICobrado) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND MONTH(CAST(CP.U_SEIFechaCobro as DATE)) = MONTH(CAST(GETDATE() AS DATE)) AND YEAR(CAST(CP.U_SEIFechaCobro as DATE)) = YEAR(CAST(GETDATE() AS DATE))),0) as ImporteCobrosSAPMes, importe por mes
9999 as ImporteCobrosSAPMes,
--verificar si tienen el almacen central o por vendedor o por zona?
'0' as AlmacenCentral,
ISNULL(T5.Warehouse,'') as AlmacenUsuario,
--verificar que lista se le asigna a cada usuario
'3' as ListaPrecioCompra,
--Verificar la lista que se le asigna a cada usuario
'2' as ListaPrecioVenta,
--ISNULL(T3.U_SEILocal, 'N') as Localizable   verificar funcionalidad
--CREAR EL CAMPO LOCALIZABLE
'N' as Localizable,
100000 as ObjetivoAcumulado
from OSLP T1
--[@SEIEMPLE] T0
--INNER JOIN OSLP T1 ON T0.U_SEIslppa = T1.SlpCode AND ISNULL(T0.U_SEIestad,'') <> '99'
--Va con INNER JOIN
LEFT JOIN OHEM T3 ON T3.salesPrson = T1.SlpCode
LEFT JOIN OUSR T4 ON T3.userId = T4.USERID
LEFT JOIN OUDG T5 ON T4.DfltsGroup = T5.Code
WHERE T3.EmpID is not null
GO
--Poner la version en la apk en 1


CREATE VIEW [TAB_ARTICULOSHISTORICOSDESACTIVADOS]
AS
SELECT     
	U_SEIItemCode AS ItemCode, 
	U_SEICardCode AS CardCode, 
	U_SEIEmpId AS EmpID, 
	'N' AS Borrado,
	ISNULL(RIGHT('0000' + CAST(YEAR(U_SEIFecha) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(U_SEIFecha) AS varchar(4)), 2) + RIGHT('00' + CAST(DAY(U_SEIFecha) AS varchar(4)), 2), N'20000101') AS Fecha,
	'S' AS Sincro, 
	'' AS ErrorSincro,
	ISNULL(RIGHT('0000' + CAST(YEAR(GETDATE()) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(GETDATE()) AS varchar(4)), 2)+ RIGHT('00' + CAST(DAY(GETDATE()) AS varchar(4)), 2), N'20000101') AS FechaSincro

FROM         
	[@ARTHISTDESACTIVADO] AS T0
GO




