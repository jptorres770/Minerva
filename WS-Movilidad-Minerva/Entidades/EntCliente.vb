﻿Public Class EntCliente

    Public Property Codigo As String
    Public Property Nombre As String
    Public Property NombreComercial As String
    Public Property FechaAlta As Integer

    Public Property Grupo As Integer
    Public Property EmpID As Integer
    Public Property Congelado As String
    Public Property NIF As String

    Public Property Telefono1 As String
    Public Property Telefono2 As String
    Public Property TelefonoMv As String
    Public Property Email As String
    Public Property Contacto As String

    Public Property LimiteCreditoTotal As Double
    Public Property CreditoConsumido As Double
    Public Property CreditoDisponible As Double


    Public Property VentasUltAnyo As Double
    Public Property VentasAnyoAnt As Double
    Public Property GestionSAC As String
    Public Property ImpMedioPedido As Double
    Public Property FechaUltAlbaran As Integer
    Public Property ObsAlbaran As String
    Public Property Notas As String

    Public Property DireccionFactura As String
    Public Property PoblacionFactura As String
    Public Property CPFactura As String
    Public Property CiudadFactura As String
    Public Property ProvinciaFactura As String

    Public Property Latitud As String
    Public Property Longitud As String

    Public Property CondicionPago As String
    Public Property DiaPago As String
    Public Property ViaPago As String
    Public Property Facturacion As String
    Public Property ListaPreciosCode As String
    Public Property ListaPreciosName As String
    Public Property PlazoMedioPago As Integer
    Public Property NumeroCuenta As String

    Public Property RecargoEquiv As String

    Public Property Categoria As String
    Public Property Actividad As String


    Public Property Sincro As String
    Public Property FechaSincro As Integer
    Public Property IncidenciaSincro As String
    Public Property DescuentoEfectivo As String
End Class
