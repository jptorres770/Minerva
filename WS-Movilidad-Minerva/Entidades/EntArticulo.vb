﻿Public Class EntArticulo

    Public Property ItemCode As String
    Public Property ItemDesc As String
    Public Property RutaFoto As String
    Public Property RutaFicha As String
    Public Property ListaPrecioCompra As String
    Public Property ListaPrecioVenta As String

    Public Property EAN As String
    Public Property FamiliaCode As Integer
    Public Property FamiliaName As String

    Public Property SubFamiliaCode As String
    Public Property SubFamiliaName As String

    Public Property FabricanteCode As Integer
    Public Property FabricanteName As String

    Public Property ProveedorCode As String
    Public Property ProveedorName As String

    Public Property UnidadVenta As String

    Public Property UnidadesPorCaja As Double
    Public Property Cajas As Double
    Public Property Kilos As Double

    Public Property IVA As Double
    Public Property IVARecargo As Double

    'Public Property interno_seleccionado As Boolean

    Public Property Paletizacion1 As Double
    Public Property Paletizacion2 As Double
    Public Property Paletizacion3 As Double

    Public Property PlazoServicio As String
    Public Property SobrePedido As String

    Public Property Notas As String

    Public Property Novedad As String
    Public Property Promocion As String


    'propiedades
    Public Property Prop1 As String
    Public Property Prop2 As String
    Public Property Prop3 As String
    Public Property Prop4 As String
    Public Property Prop5 As String
    Public Property Prop6 As String
    Public Property Prop7 As String
    Public Property Prop8 As String
    Public Property Prop9 As String
    Public Property Prop10 As String
    Public Property Prop11 As String
    Public Property Prop12 As String
    Public Property Prop13 As String
    Public Property Prop14 As String
    Public Property Prop15 As String
    Public Property Prop16 As String
    Public Property Prop17 As String
    Public Property Prop18 As String
    Public Property Prop19 As String
    Public Property Prop20 As String
    Public Property Prop21 As String
    Public Property Prop22 As String
    Public Property Prop23 As String
    Public Property Prop24 As String
    Public Property Prop25 As String
    Public Property Prop26 As String
    Public Property Prop27 As String
    Public Property Prop28 As String
    Public Property Prop29 As String
    Public Property Prop30 As String
    Public Property Prop31 As String
    Public Property Prop32 As String
    Public Property Prop33 As String
    Public Property Prop34 As String
    Public Property Prop35 As String
    Public Property Prop36 As String
    Public Property Prop37 As String
    Public Property Prop38 As String
    Public Property Prop39 As String
    Public Property Prop40 As String
    Public Property Prop41 As String
    Public Property Prop42 As String
    Public Property Prop43 As String
    Public Property Prop44 As String
    Public Property Prop45 As String
    Public Property Prop46 As String
    Public Property Prop47 As String
    Public Property Prop48 As String
    Public Property Prop49 As String
    Public Property Prop50 As String
    Public Property Prop51 As String
    Public Property Prop52 As String
    Public Property Prop53 As String
    Public Property Prop54 As String
    Public Property Prop55 As String
    Public Property Prop56 As String
    Public Property Prop57 As String
    Public Property Prop58 As String
    Public Property Prop59 As String
    Public Property Prop60 As String
    Public Property Prop61 As String
    Public Property Prop62 As String
    Public Property Prop63 As String
    Public Property Prop64 As String

End Class
