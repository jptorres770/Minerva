﻿Public Class EntDocumentoCabPORTAL
    Public Property DocEntry As Integer
    Public Property CardCode As String
    Public Property CardName As String
    Public Property DocNum As Integer
    Public Property DocDate As String
    Public Property TaxDate As String
    Public Property DocDueDate As String
    Public Property NumAtCard As String
    Public Property SubTotal As String
    Public Property DocTotal As String
    Public Property VatSum As String
    Public Property Descuento As String
    Public Property ListaLineasDocumento As List(Of EntDocumentoLinePORTAL)

End Class