﻿Option Strict On

Imports Entidades
Imports System.Data.SqlClient
Imports System.Globalization

Public Class clsDatosBajada
    Dim oUtil As clsUtil = New clsUtil
    Public Enum eFatherCard
        Yes = 1
        No = 0
        Todas = 3
    End Enum

    'traza del portal web
    Public Function getLocalizaciones(ByVal Empleado As Integer, ByVal Fecha As String, ByVal hash As String) As List(Of EntLocalizaciones)
        Dim oLocalizaciones As List(Of EntLocalizaciones) = New List(Of EntLocalizaciones)
        Dim oCredentials As List(Of USERSAP) = Nothing
        Dim md5Hash As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Try
            oCredentials = getSessionInfo(Empleado.ToString)
            Dim SQL = "SELECT * " +
                      "FROM ""@TABLETLOCALIZACIONE"" " +
                      "WHERE ""U_SEIFecha"" = '" & Fecha & "' AND ""U_SEIEmpleado"" = '" & Empleado & "' " +
                      "ORDER BY ""U_SEIFecha"" , ""U_SEIHora"" ASC"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "LOCALIZACIONES", "LOCALIZACIONES")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntLocalizaciones
                        With oEntidad
                            .Codigo = CInt(linea.Item("Code").ToString)
                            .Longitud = linea.Item("U_SEILon").ToString
                            .Latitud = linea.Item("U_SEILat").ToString
                            .Fecha = linea.Item("U_SEIFecha").ToString
                            .Hora = CInt(linea.Item("U_SEIHora").ToString)
                            .Empleado = linea.Item("U_SEIEmpleado").ToString
                            .Proveedor = linea.Item("U_SEIProveedor").ToString
                        End With
                        oLocalizaciones.Add(oEntidad)
                    Next

                End If
            End If

            If oUtil.VerifyMd5Hash(md5Hash, oCredentials(0).password.ToString, hash) = True Then
                Return oLocalizaciones
            Else
                Dim oLocal As List(Of EntLocalizaciones) = New List(Of EntLocalizaciones)
                Return oLocal
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        End Try

    End Function

    'almacenes
    Public Function getAlmacenes() As List(Of EntAlmacen)
        Dim oAlmacenes As List(Of EntAlmacen) = New List(Of EntAlmacen)

        Try
            Dim SQL As String = "SELECT T0.""WhsCode"", T0.""WhsName"" FROM OWHS T0 WHERE ""Locked"" = 'N' "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Almacenes", "Almacenes")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntAlmacen
                        With oEntidad
                            .ID = linea.Item("WhsCode").ToString
                            .Descripcion = linea.Item("WhsName").ToString
                        End With
                        oAlmacenes.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oAlmacenes

    End Function


    'Login portal Web
    Public Function LoginPortalWEB(ByVal user As String, ByVal password As String) As String

        Dim oCredentials As List(Of USERSAP) = Nothing
        Dim md5Hash As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Try
            oCredentials = getSessionInfo(user)

            Dim hashTurn As String = oUtil.GetMd5Hash(md5Hash, oCredentials(0).password.ToString)

            If Not IsNothing(oCredentials) Then
                If password = oCredentials(0).password Then

                    Return hashTurn
                Else
                    Return "Credenciales invalidas"
                End If
            End If
        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        End Try

    End Function

    'Pedidos

    Public Function getPedidosPORTAL(ByVal Fecha As String, ByVal user As String, ByVal hash As String) As List(Of EntEmpleadoPedidoPORTAL)

        Dim oEmpleados As List(Of EntEmpleadoPedidoPORTAL) = New List(Of EntEmpleadoPedidoPORTAL)
        Dim oCredentials As List(Of USERSAP) = Nothing
        Dim md5Hash As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Try
            oCredentials = getSessionInfo(user)
            If oUtil.VerifyMd5Hash(md5Hash, oCredentials(0).password.ToString, hash) = True Then
                oEmpleados = getEmpleadosPedidosPORTAL(Fecha, user, oCredentials(0).permisos.ToString)
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        End Try

        Return oEmpleados

    End Function
    'facturas
    Public Function getFacturasPORTAL(ByVal Fecha As String, ByVal user As String, ByVal hash As String) As List(Of EntEmpleadoFacturaPORTAL)

        Dim oEmpleados As List(Of EntEmpleadoFacturaPORTAL) = New List(Of EntEmpleadoFacturaPORTAL)
        Dim oCredentials As List(Of USERSAP) = Nothing
        Dim md5Hash As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Try
            oCredentials = getSessionInfo(user)
            If oUtil.VerifyMd5Hash(md5Hash, oCredentials(0).password.ToString, hash) = True Then
                oEmpleados = getEmpleadosFacturasPORTAL(Fecha, user, oCredentials(0).permisos.ToString)
            End If
        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        End Try

        Return oEmpleados

    End Function

    Public Function getEmpleadosPedidosPORTAL(ByVal Fecha As String, ByVal User As String, ByVal Permisos As String) As List(Of EntEmpleadoPedidoPORTAL)
        Dim oCredenciales As List(Of EntEmpleadoPedidoPORTAL) = New List(Of EntEmpleadoPedidoPORTAL)

        Try
            Dim SQL As String = ""
            If Permisos = "S" Then
                SQL = "SELECT " +
                                "T0.""empID"", " +
                                "CONCAT(CONCAT(CONCAT(T0.""firstName"", ' '), T0.""middleName""), T0.""lastName"") AS ""Nombre"", " +
                                "T0.""jobTitle"", " +
                                "T0.""salesPrson"", " +
                                "T1.""U_SEIalmac"" " +
                                "FROM OHEM T0 " +
                                "INNER JOIN OSLP T1 " +
                                "ON T1.""SlpCode"" = T0.""salesPrson"" " +
                                "WHERE T1.""DocDate"" = '" & Fecha & "' "
            Else
                SQL = "SELECT " +
                                "T0.""empID"", " +
                                "CONCAT(CONCAT(CONCAT(T0.""firstName"", ' '), T0.""middleName""), T0.""lastName"") AS ""Nombre"", " +
                                "T0.""jobTitle"", " +
                                "T0.""salesPrson"", " +
                                "T1.""U_SEIalmac"" " +
                                "FROM OHEM T0 " +
                                "INNER JOIN OSLP T1 " +
                                "ON T1.""SlpCode"" = T0.""salesPrson"" " +
                                "WHERE T0.""empID"" = '" & User & "' " +
                                "AND T1.""DocDate"" = '" & Fecha & "' "
            End If



            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "EMPLEADO", "EMPLEADO")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntEmpleadoPedidoPORTAL
                        Dim oPedidosCab As New EntDocumentoCabPORTAL
                        With oEntidad
                            .EmpID = CInt(linea.Item("empID").ToString)
                            .Cargo = linea.Item("jobTitle").ToString
                            .Nombre = linea.Item("Nombre").ToString
                            .EmpVentas = linea.Item("salesPrson").ToString
                            .Almacen = linea.Item("U_SEIalmac").ToString
                        End With
                        oEntidad.ListaPedidos = getPedidosCabPORTAL(CInt(oEntidad.EmpVentas), Fecha)
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function


    Public Function getEmpleadosFacturasPORTAL(ByVal Fecha As String, ByVal User As String, ByVal Permisos As String) As List(Of EntEmpleadoFacturaPORTAL)
        Dim oCredenciales As List(Of EntEmpleadoFacturaPORTAL) = New List(Of EntEmpleadoFacturaPORTAL)

        Try
            Dim SQL As String = ""
            If Permisos = "S" Then
                SQL = "SELECT TOP 10 " +
                                "T0.""empID"", " +
                                "CONCAT(CONCAT(CONCAT(T0.""firstName"", ' '), T0.""middleName""), T0.""lastName"") AS ""Nombre"", " +
                                "T0.""jobTitle"", " +
                                "T0.""salesPrson"", " +
                                "T1.""U_SEIalmac"" " +
                                "FROM OHEM T0 " +
                                "INNER JOIN OSLP T1 " +
                                "ON T1.""SlpCode"" = T0.""salesPrson"" "
            Else
                SQL = "SELECT " +
                                "T0.""empID"", " +
                                "CONCAT(CONCAT(CONCAT(T0.""firstName"", ' '), T0.""middleName""), T0.""lastName"") AS ""Nombre"", " +
                                "T0.""jobTitle"", " +
                                "T0.""salesPrson"", " +
                                "T1.""U_SEIalmac"" " +
                                "FROM OHEM T0 " +
                                "INNER JOIN OSLP T1 " +
                                "ON T1.""SlpCode"" = T0.""salesPrson"" " +
                                "WHERE T0.""empID"" = '" & User & "' "
            End If


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "EMPLEADO", "EMPLEADO")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntEmpleadoFacturaPORTAL
                        Dim oPedidosCab As New EntDocumentoCabPORTAL
                        With oEntidad
                            .EmpID = CInt(linea.Item("empID").ToString)
                            .Cargo = linea.Item("jobTitle").ToString
                            .Nombre = linea.Item("Nombre").ToString
                            .EmpVentas = linea.Item("salesPrson").ToString
                            .Almacen = linea.Item("U_SEIalmac").ToString
                        End With
                        oEntidad.ListaFacturas = getFacturasCabPORTAL(CInt(oEntidad.EmpVentas), Fecha)
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function



    'Traer Informacion de Facturas y Empleados
    Public Function getFacturasCabPORTAL(slpCode As Integer, ByVal Fecha As String) As List(Of EntDocumentoCabPORTAL)
        Dim oCredenciales As List(Of EntDocumentoCabPORTAL) = New List(Of EntDocumentoCabPORTAL)

        Try
            Dim SQL As String = "SELECT " +
                                "T0.""DocEntry"", " +
                                "T0.""CardCode"", " +
                                "T0.""CardName"", " +
                                "T0.""DocNum"", " +
                                "T0.""DocDate"", " +
                                "T0.""TaxDate"", " +
                                "T0.""DocDueDate"", " +
                                "T0.""NumAtCard"", " +
                                "T0.""DocTotal"", " +
                                "T0.""VatSum"", " +
                                "T0.""DiscPrcnt"" " +
                                "FROM OINV T0 " +
                                "WHERE T0.""SlpCode"" = '" & slpCode & "' " +
                                "AND T0.""DocDate"" = '" & Fecha & "' " +
                                "AND T0.""CANCELED"" = 'N'"
            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "FACTURACAB", "FACTURACAB")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntDocumentoCabPORTAL
                        Dim oEntidadLineas As New EntDocumentoLinePORTAL
                        With oEntidad
                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .CardCode = linea.Item("CardCode").ToString
                            .CardName = linea.Item("CardName").ToString
                            .DocNum = CInt(linea.Item("DocNum").ToString)
                            .DocDate = linea.Item("DocDate").ToString
                            .TaxDate = linea.Item("TaxDate").ToString
                            .DocDueDate = linea.Item("DocDueDate").ToString
                            .NumAtCard = linea.Item("NumAtCard").ToString
                            .DocTotal = linea.Item("DocTotal").ToString
                            .VatSum = linea.Item("VatSum").ToString
                            .Descuento = linea.Item("DiscPrcnt").ToString

                        End With
                        oEntidad.ListaLineasDocumento = getFacturasLinPORTAL(oEntidad.DocEntry)
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function

    'Lineas Facturas Portal
    Public Function getFacturasLinPORTAL(docEntry As Integer) As List(Of EntDocumentoLinePORTAL)
        Dim oCredenciales As List(Of EntDocumentoLinePORTAL) = New List(Of EntDocumentoLinePORTAL)

        Try
            Dim SQL As String = "SELECT " +
                                "T0.""DocEntry"", " +
                                "T0.""ItemCode"", " +
                                "T0.""Dscription"", " +
                                "T0.""Quantity"", " +
                                "T0.""TaxCode"", " +
                                "T0.""WhsCode"", " +
                                "T0.""Price"", " +
                                "T0.""DiscPrcnt"", " +
                                "T0.""LineTotal"", " +
                                "T0.""VatSum""" +
                                "FROM INV1 T0 " +
                                "WHERE T0.""DocEntry"" = '" & docEntry & "'"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "PEDIDOLIN", "PEDIDOLIN")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntDocumentoLinePORTAL
                        With oEntidad
                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .ItemCode = linea.Item("ItemCode").ToString
                            .ItemDscription = linea.Item("Dscription").ToString
                            .Quantity = linea.Item("Quantity").ToString
                            .TaxCode = linea.Item("TaxCode").ToString
                            .WhsCode = linea.Item("WhsCode").ToString
                            .Price = CDbl(linea.Item("Price").ToString)
                            .DiscPrcnt = CDbl(linea.Item("DiscPrcnt").ToString)
                            .LineTotal = CDbl(linea.Item("LineTotal").ToString)
                            .VatSum = CDbl(linea.Item("VatSum").ToString)
                        End With
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function




    'Traer informacion de Pedidos y Empleados
    Public Function getPedidosCabPORTAL(slpCode As Integer, ByVal Fecha As String) As List(Of EntDocumentoCabPORTAL)
        Dim oCredenciales As List(Of EntDocumentoCabPORTAL) = New List(Of EntDocumentoCabPORTAL)

        Try
            Dim SQL As String = "SELECT " +
                                "T0.""DocEntry"", " +
                                "T0.""CardCode"", " +
                                "T0.""CardName"", " +
                                "T0.""DocNum"", " +
                                "T0.""DocDate"", " +
                                "T0.""TaxDate"", " +
                                "T0.""DocDueDate"", " +
                                "T0.""NumAtCard"", " +
                                "T0.""DocTotal"", " +
                                "T0.""VatSum"", " +
                                "T0.""DiscPrcnt"" " +
                                "FROM ORDR T0 " +
                                "WHERE T0.""SlpCode"" = '" & slpCode & "' " +
                                "AND T0.""DocDate"" = '" & Fecha & "' " +
                                "AND T0.""CANCELED"" = 'N'"
            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "PEDIDOCAB", "PEDIDOCAB")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntDocumentoCabPORTAL
                        Dim oEntidadLineas As New EntDocumentoLinePORTAL
                        With oEntidad
                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .CardCode = linea.Item("CardCode").ToString
                            .CardName = linea.Item("CardName").ToString
                            .DocNum = CInt(linea.Item("DocNum").ToString)
                            .DocDate = linea.Item("DocDate").ToString
                            .TaxDate = linea.Item("TaxDate").ToString
                            .DocDueDate = linea.Item("DocDueDate").ToString
                            .NumAtCard = linea.Item("NumAtCard").ToString
                            .DocTotal = linea.Item("DocTotal").ToString
                            .VatSum = linea.Item("VatSum").ToString
                            .Descuento = linea.Item("DiscPrcnt").ToString

                        End With
                        oEntidad.ListaLineasDocumento = getPedidosLinPORTAL(oEntidad.DocEntry)
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function

    'Traer informacion de Pedidos y Empleados
    Public Function getPedidosLinPORTAL(docEntry As Integer) As List(Of EntDocumentoLinePORTAL)
        Dim oCredenciales As List(Of EntDocumentoLinePORTAL) = New List(Of EntDocumentoLinePORTAL)

        Try
            Dim SQL As String = "SELECT " +
                                "T0.""DocEntry"", " +
                                "T0.""ItemCode"", " +
                                "T0.""Dscription"", " +
                                "T0.""Quantity"", " +
                                "T0.""TaxCode"", " +
                                "T0.""WhsCode"", " +
                                "T0.""Price"", " +
                                "T0.""DiscPrcnt"", " +
                                "T0.""LineTotal"", " +
                                "T0.""VatSum""" +
                                "FROM RDR1 T0 " +
                                "WHERE T0.""DocEntry"" = '" & docEntry & "'"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "PEDIDOLIN", "PEDIDOLIN")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntDocumentoLinePORTAL
                        With oEntidad
                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .ItemCode = linea.Item("ItemCode").ToString
                            .ItemDscription = linea.Item("Dscription").ToString
                            .Quantity = linea.Item("Quantity").ToString
                            .TaxCode = linea.Item("TaxCode").ToString
                            .WhsCode = linea.Item("WhsCode").ToString
                            .Price = CDbl(linea.Item("Price").ToString)
                            .DiscPrcnt = CDbl(linea.Item("DiscPrcnt").ToString)
                            .LineTotal = CDbl(linea.Item("LineTotal").ToString)
                            .VatSum = CDbl(linea.Item("VatSum").ToString)
                        End With
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function

    'usuarios portal
    Public Function getUsersPortal(ByVal user As String, ByVal hash As String) As List(Of USERSAP)
        Dim oCredenciales As List(Of USERSAP) = New List(Of USERSAP)
        Dim otest As List(Of USERSAP) = Nothing
        Dim md5Hash As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
        Try

            otest = getSessionInfo(user)
            Dim SQL As String = ""
            If otest(0).permisos = "S" Then
                SQL = "SELECT ISNULL(T2.""firstName"", '') + ' ' + ISNULL(T2.""middleName"", '') + ' ' + ISNULL(T2.""lastName"", '') AS ""Name"", " +
                                "T2.""empID"" As ""User"", " +
                                "T2.""U_SEIPass"" AS ""Password"", " +
                                "T2.""jobTitle"" As ""Cargo"", " +
                                "T2.""empID"" AS ""Empleado"", " +
                                "T2.""U_SEI_PORTAL_ADMIN"" AS ""Permisos"" " +
                                "From OHEM T2 WHERE T2.""salesPrson"" != '-1' "
            Else
                SQL = "SELECT ISNULL(T2.""firstName"", '') + ' ' + ISNULL(T2.""middleName"", '') + ' ' + ISNULL(T2.""lastName"", '') AS ""Name"", " +
                                "T2.""empID"" As ""User"", " +
                                "T2.""U_SEIPass"" AS ""Password"", " +
                                "T2.""jobTitle"" As ""Cargo"", " +
                                "T2.""empID"" AS ""Empleado"", " +
                                "T2.""U_SEI_PORTAL_ADMIN"" AS ""Permisos"" " +
                                "From OHEM T2 WHERE T2.""empID"" = '" & user & "' "
            End If


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "USERSPORTAL", "USERSPORTAL")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New USERSAP
                        With oEntidad
                            .user = linea.Item("User").ToString
                            .password = linea.Item("Password").ToString
                            .cargo = linea.Item("Cargo").ToString
                            .empleado = CInt(linea.Item("Empleado").ToString)
                            .nombre = linea.Item("Name").ToString
                        End With
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If
            If oUtil.VerifyMd5Hash(md5Hash, otest(0).password.ToString, hash) = True Then
                Return oCredenciales
            Else
                Dim oLocal As List(Of USERSAP) = New List(Of USERSAP)
                Return oLocal
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try


    End Function

    'Traer informacion de sesión
    Public Function getSessionInfo(ByVal User As String) As List(Of USERSAP)
        Dim oCredenciales As List(Of USERSAP) = New List(Of USERSAP)

        Try
            Dim SQL As String = "SELECT ISNULL(T2.""firstName"", '') + ' ' + ISNULL(T2.""middleName"", '') + ' ' + ISNULL(T2.""lastName"", '') AS ""Name"", " +
                                "T2.""empID"" As ""User"", " +
                                "T2.""U_SEIPass"" As ""Password"", " +
                                "T2.""jobTitle"" As ""Cargo"", " +
                                "T2.""empID"" As ""Empleado"", " +
                                "T2.""U_SEI_PORTAL_ADMIN"" As ""Permisos"" " +
                                "From OHEM T2 " +
                                "Where T2.""empID"" = '" & User & "' "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "CREDENCIALES", "CREDENCIALES")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New USERSAP
                        With oEntidad
                            .user = linea.Item("User").ToString
                            .password = linea.Item("Password").ToString
                            .cargo = linea.Item("Cargo").ToString
                            .empleado = CInt(linea.Item("Empleado").ToString)
                            .nombre = linea.Item("Name").ToString
                            .permisos = linea.Item("Permisos").ToString
                        End With
                        oCredenciales.Add(oEntidad)
                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oCredenciales

    End Function



    'Grupos de Articulos
    Public Function getGruposDeArticulos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntGrupoArticulos)

        Dim oLista As New List(Of EntGrupoArticulos)

        Try

            Dim SQL As String = ""
            SQL = "SELECT " +
                 "T0.ItmsGrpCod AS 'IdGrupo', " +
                 "T0.ItmsGrpNam AS 'NombreGrupo' " +
                 "From OITB T0 " +
                 "Where T0.Locked = 'N' "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_GRUPOSDECLIENTES", "TAB_GRUPODECLIENTES")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntGrupoArticulos
                        With oEntidad

                            .IDGrupo = CInt(linea.Item("IDGrupo").ToString)
                            .NombreGrupo = linea.Item("NombreGrupo").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function
    'Propiedades de los Articulos
    Public Function getPropArticulos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntPropArticulos)

        Dim oLista As New List(Of EntPropArticulos)

        Try

            Dim SQL As String = ""
            SQL = "SELECT " +
                  "T0.ItmsTypCod AS 'IDPropiedad', " +
                  "T0.ItmsGrpNam AS 'NombrePropiedad' " +
                  "From OITG T0 "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_PROPIEDADDELOSARTICULOS", "TAB_PROPIEDADDELOSARTICULOS")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntPropArticulos
                        With oEntidad

                            .IDPropiedad = CInt(linea.Item("IDPropiedad").ToString)
                            .NombrePropiedad = linea.Item("NombrePropiedad").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function
    'Fabricante de Articulos
    Public Function getFabricanteArticulos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntFabArticulos)

        Dim oLista As New List(Of EntFabArticulos)

        Try

            Dim SQL As String = ""
            SQL = "SELECT " +
                "T0.FirmName AS 'NombreFabricante', " +
                "T0.FirmCode AS 'IDFabricante' " +
                "From OMRC T0"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_FABARTICULOS", "TAB_FABARTICULOS")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntFabArticulos
                        With oEntidad

                            .IDFabricante = CInt(linea.Item("IDFabricante").ToString)
                            .NombreFabricante = linea.Item("NombreFabricante").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function
    Public Function getGruposDeClientes(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntGpClientes)

        Dim oLista As New List(Of EntGpClientes)

        Try

            Dim SQL As String = ""
            SQL = "Select " +
                  "T0.GroupCode As 'IdGpCliente', " +
                  "T0.GroupName AS 'NameGpCliente' " +
                  "FROM OCRG T0 " +
                  "WHERE T0.GroupType = 'C' " +
                  "AND T0.Locked = 'N' "


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_GRUPOSDECLIENTES", "TAB_GRUPOSDECLIENTES")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntGpClientes
                        With oEntidad

                            .IdGpCliente = CInt(linea.Item("IdGpCliente").ToString)
                            .NameGpCliente = linea.Item("NameGpCliente").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    Public Function getDocumentosPendientesCab(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntFacturaPendienteCab)
        Dim oLista As New List(Of EntFacturaPendienteCab)
        Try
            Dim SQL As String = ""
            SQL &= "SELECT " &
                   "T0.DocEntry as 'DocEntry', " &
                   "T0.DocNum as 'NumeroFactura', " &
                   "T0.NumAtCard as 'NumeroFacturaVendedor', " &
                   "T0.DocDate as 'FechaFactura', " &
                   "T0.CardCode as 'IDCliente', " &
                   "T0.DocDueDate as 'FechaVencimiento', " &
                   "T0.DocTotal as 'ImporteTotal', " &
                   "T0.DocCur as 'Moneda', " &
                   "T0.PaidToDate as 'ImportePagado', " &
                   "T0.DocTotal - T0.PaidToDate as 'ImportePendiente'" &
                   "FROM OINV T0 " &
                   "WHERE T0.DocStatus = 'O' " &
                   "AND T0.CANCELED = 'N' " &
                   "AND T0.ObjType = '13' " &
                   "ORDER BY T0.DocDueDate DESC "


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_DOCUMENTOSPENDIENTESCAB", "TAB_DOCUMENTOSPENDIENTESCAB")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntFacturaPendienteCab
                        With oEntidad

                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .FechaFactura = linea.Item("FechaFactura").ToString
                            .FechaVencimiento = linea.Item("FechaVencimiento").ToString
                            .ImportePagado = CDbl(linea.Item("ImportePagado").ToString)
                            .ImporteTotal = CDbl(linea.Item("ImporteTotal").ToString)
                            .Moneda = linea.Item("Moneda").ToString
                            .NumeroFactura = CInt(linea.Item("NumeroFactura").ToString)
                            .IDCliente = linea.Item("IDCliente").ToString
                            .ImportePendiente = CDbl(linea.Item("ImportePendiente").ToString)
                            .NumeroFacturaVendedor = linea.Item("NumeroFacturaVendedor").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getDocumentosPendientesLin(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntFacturaPendienteLin)
        Dim oLista As New List(Of EntFacturaPendienteLin)

        Try
            Dim SQL As String = ""
            SQL &= "SELECT " &
                    "T0.DocEntry As 'DocEntry', " &
                    "T1.ItemCode as 'CodigoArticulo', " &
                    "T1.Dscription as 'DescripcionArticulo', " &
                    "T1.Quantity as 'Cantidad', " &
                    "T1.Price as 'Precio', " &
                    "T1.Currency as 'Moneda', " &
                    "T1.WhsCode AS 'Almacen', " &
                    "T1.LineNum AS 'LineNum'" &
                    "FROM OINV T0 " &
                    "INNER JOIN INV1 T1 ON T1.DocEntry = T0.DocEntry " &
                    "WHERE T0.DocStatus = 'O' " &
                    "And T0.CANCELED = 'N' " &
                    "AND T0.ObjType = '13' " &
                    "ORDER BY T0.DocDueDate DESC "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_DOCUMENTOSPENDIENTESLIN", "TAB_DOCUMENTOSPENDIENTESLIN")
            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntFacturaPendienteLin
                        With oEntidad

                            .DocEntry = CInt(linea.Item("DocEntry").ToString)
                            .CodigoArticulo = linea.Item("CodigoArticulo").ToString
                            .DescripcionArticulo = linea.Item("DescripcionArticulo").ToString
                            .Cantidad = CDbl(linea.Item("Cantidad").ToString)
                            .Price = CDbl(linea.Item("Precio").ToString)
                            .Moneda = linea.Item("Moneda").ToString
                            .Almacen = linea.Item("Almacen").ToString
                            .LineNum = CInt(linea.Item("LineNum").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getGpDescCab(ByVal empleado As Integer, ByVal version As Integer) As List(Of EntGpDescCab)

        Dim oLista As New List(Of EntGpDescCab)

        Try
            Dim SQL As String = ""
            SQL &= "SELECT "
            SQL &= "T0.""AbsEntry"" AS ""NumGpDesc"", "
            SQL &= "T0.""Type"" As ""TipoGpDesc"", "
            SQL &= "T0.""ObjType"" As ""TipoObjeto"", "
            SQL &= "T0.""ObjCode"" As ""CodObjeto"", "
            SQL &= "T0.""DiscRel"" As ""DescuentoRealizado"" "
            SQL &= "From OEDG T0 "
            SQL &= "WHERE T0.""ValidFor"" = 'Y'"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "GpDescCab", "GpDescCab")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntGpDescCab
                        With oEntidad
                            .NumGpDesc = CInt(linea.Item("NumGpDesc").ToString)
                            .TipoGpDesc = linea.Item("TipoGpDesc").ToString
                            .TipoObjeto = linea.Item("TipoObjeto").ToString
                            .CodObjeto = linea.Item("CodObjeto").ToString
                            .DescuentoRealizado = linea.Item("DescuentoRealizado").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getGpDescOfertas(empleado As Integer, version As Integer) As List(Of EntGpDescOfertas)
        Dim oLista As New List(Of EntGpDescOfertas)

        Try
            Dim SQL As String = ""
            SQL &= "SELECT "
            SQL &= "T0.""AbsEntry"" AS ""NumGpDesc"", "
            SQL &= "CASE T0.""ObjType"" "
            SQL &= "WHEN 52 THEN 'GpArticulos' "
            SQL &= "When 8 Then 'PropArticulos' "
            SQL &= "WHEN 43 THEN 'FabArticulo' "
            SQL &= "When 4 Then 'ArticuloEspecifico' "
            SQL &= "END "
            SQL &= "As ""TipoObjeto"", "
            SQL &= "T0.""ObjKey"" AS ""Objeto"", "
            SQL &= "T0.""PayFor"" As ""Cantidad"", "
            SQL &= "T0.""ForFree"" AS ""Oferta"", "
            SQL &= "T0.""UpTo"" As ""MaxOferta"" "
            SQL &= "FROM EDG1 T0 "
            SQL &= "WHERE T0.""DiscType"" = 'P' "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "GpDescOfertas", "GpDescOfertas")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntGpDescOfertas
                        With oEntidad
                            .NumGpDesc = CInt(linea.Item("NumGpDesc").ToString)
                            .TipoObjeto = linea.Item("TipoObjeto").ToString
                            .Objeto = linea.Item("Objeto").ToString
                            .Cantidad = CDbl(linea.Item("Cantidad").ToString)
                            .Oferta = CDbl(linea.Item("Oferta").ToString)
                            .MaxOferta = CDbl(linea.Item("MaxOferta").ToString)
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista
    End Function

    Public Function getPreciosEspeciales(empleado As Integer, version As Integer) As List(Of EntDescEspeciales)
        Dim oLista As New List(Of EntDescEspeciales)

        Try
            Dim SQL As String = ""
            SQL &= "SELECT "
            SQL &= "T0.""ItemCode"" As ""CodArticulo"", "
            SQL &= "T0.""CardCode"" AS ""CodCliente"", "
            SQL &= "T0.""Currency"" AS ""Moneda"", "
            SQL &= "T0.""Discount"" AS ""Descuento"", "
            SQL &= "T0.""ListNum"" As ""ListaDePrecios"" "
            SQL &= "FROM OSPP T0"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "GpDescLin", "GpDescLin")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntDescEspeciales
                        With oEntidad
                            .CodArticulo = linea.Item("CodArticulo").ToString
                            .CodCliente = linea.Item("CodCliente").ToString
                            .Moneda = linea.Item("Moneda").ToString
                            .Descuento = CDbl(linea.Item("Descuento").ToString)
                            .ListaDePrecios = CInt(linea.Item("ListaDePrecios").ToString)
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista
    End Function

    Public Function getGpDescLin(empleado As Integer, version As Integer) As List(Of EntGpDescLin)
        Dim oLista As New List(Of EntGpDescLin)

        Try
            Dim SQL As String = ""
            SQL &= "Select "
            SQL &= "T0.""AbsEntry"" As ""NumGpDesc"", "
            SQL &= "CASE T0.""ObjType"" "
            SQL &= "When 52 Then 'GpArticulos' "
            SQL &= "WHEN 8 THEN 'PropArticulos' "
            SQL &= "When 43 Then 'FabArticulo' "
            SQL &= "WHEN 4 THEN 'ArticuloEspecifico' "
            SQL &= "End "
            SQL &= "AS ""TipoObjeto"", "
            SQL &= "T0.""ObjKey"" As ""Objeto"", "
            SQL &= "T0.""Discount"" AS ""Descuento"" "
            SQL &= "FROM EDG1 T0 "
            SQL &= "WHERE T0.""DiscType"" = 'D'"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "GpDescLin", "GpDescLin")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntGpDescLin
                        With oEntidad
                            .NumGpDesc = CInt(linea.Item("NumGpDesc").ToString)
                            .TipoObjeto = linea.Item("TipoObjeto").ToString
                            .Objeto = linea.Item("Objeto").ToString
                            .Descuento = CDbl(linea.Item("Descuento").ToString)
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista
    End Function

    Public Function getCuentasTransferencia(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntCuentaTransferencia)

        Dim oLista As New List(Of EntCuentaTransferencia)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM [@SEICUETRANS]"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "CuentasTransferencia", "CuentasTransferencia")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntCuentaTransferencia
                        With oEntidad
                            .Cuenta = linea.Item("U_SEICuenta").ToString
                            .Nombre = linea.Item("U_SEIDescripcion").ToString
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getCuentasCheques(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntCuentasCheques)

        Dim oLista As New List(Of EntCuentasCheques)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM [@SEICUECHEQUE]"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "CuentasCheques", "CuentasCheques")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntCuentasCheques
                        With oEntidad
                            .Cuenta = linea.Item("U_SEICuenta").ToString
                            .Nombre = linea.Item("U_SEIDescripcion").ToString
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getArticulos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntArticulo)

        Dim oLista As New List(Of EntArticulo)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_Articulos"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Articulos", "TAB_Articulos")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntArticulo
                        With oEntidad

                            .ItemCode = linea.Item("ItemCode").ToString
                            .ItemDesc = linea.Item("ItemDesc").ToString
                            .RutaFoto = linea.Item("RutaFoto").ToString
                            .RutaFicha = linea.Item("RutaFicha").ToString
                            .ListaPrecioCompra = linea.Item("ListaPrecioCompra").ToString
                            .ListaPrecioVenta = linea.Item("ListaPrecioVenta").ToString

                            .EAN = linea.Item("EAN").ToString

                            .FamiliaCode = CInt(linea.Item("FamiliaCode").ToString)
                            .FamiliaName = linea.Item("FamiliaName").ToString

                            .SubFamiliaCode = linea.Item("SubFamiliaCode").ToString
                            .SubFamiliaName = linea.Item("SubFamiliaName").ToString

                            .FabricanteCode = CInt(linea.Item("FabricanteCode").ToString)
                            .FabricanteName = linea.Item("FabricanteName").ToString

                            .ProveedorCode = linea.Item("ProveedorCode").ToString
                            .ProveedorName = linea.Item("ProveedorName").ToString

                            .UnidadVenta = linea.Item("UnidadVenta").ToString

                            .UnidadesPorCaja = CDbl(linea.Item("UnidadesPorCaja").ToString)
                            .Cajas = CDbl(linea.Item("Cajas").ToString)
                            .Kilos = CDbl(linea.Item("Kilos").ToString)

                            .IVA = CDbl(linea.Item("IVA").ToString)
                            .IVARecargo = CDbl(linea.Item("IVARecargo").ToString)

                            .Paletizacion1 = CDbl(linea.Item("Paletizacion1").ToString)
                            .Paletizacion2 = CDbl(linea.Item("Paletizacion2").ToString)
                            .Paletizacion3 = CDbl(linea.Item("Paletizacion3").ToString)

                            .PlazoServicio = linea.Item("PlazoServicio").ToString
                            .SobrePedido = linea.Item("SobrePedido").ToString
                            .Notas = linea.Item("Notas").ToString

                            .Novedad = linea.Item("Novedad").ToString
                            .Promocion = linea.Item("Promocion").ToString

                            .Prop1 = linea.Item("1").ToString
                            .Prop2 = linea.Item("2").ToString
                            .Prop3 = linea.Item("3").ToString
                            .Prop4 = linea.Item("4").ToString
                            .Prop5 = linea.Item("5").ToString
                            .Prop6 = linea.Item("6").ToString
                            .Prop7 = linea.Item("7").ToString
                            .Prop8 = linea.Item("8").ToString
                            .Prop9 = linea.Item("9").ToString
                            .Prop10 = linea.Item("10").ToString
                            .Prop11 = linea.Item("11").ToString
                            .Prop12 = linea.Item("12").ToString
                            .Prop13 = linea.Item("13").ToString
                            .Prop14 = linea.Item("14").ToString
                            .Prop15 = linea.Item("15").ToString
                            .Prop16 = linea.Item("16").ToString
                            .Prop17 = linea.Item("17").ToString
                            .Prop18 = linea.Item("18").ToString
                            .Prop19 = linea.Item("19").ToString
                            .Prop20 = linea.Item("20").ToString
                            .Prop21 = linea.Item("21").ToString
                            .Prop22 = linea.Item("22").ToString
                            .Prop23 = linea.Item("23").ToString
                            .Prop24 = linea.Item("24").ToString
                            .Prop25 = linea.Item("25").ToString
                            .Prop26 = linea.Item("26").ToString
                            .Prop27 = linea.Item("27").ToString
                            .Prop28 = linea.Item("28").ToString
                            .Prop29 = linea.Item("29").ToString
                            .Prop30 = linea.Item("30").ToString
                            .Prop31 = linea.Item("31").ToString
                            .Prop32 = linea.Item("32").ToString
                            .Prop33 = linea.Item("33").ToString
                            .Prop34 = linea.Item("34").ToString
                            .Prop35 = linea.Item("35").ToString
                            .Prop36 = linea.Item("36").ToString
                            .Prop37 = linea.Item("37").ToString
                            .Prop38 = linea.Item("38").ToString
                            .Prop39 = linea.Item("39").ToString
                            .Prop40 = linea.Item("40").ToString
                            .Prop41 = linea.Item("41").ToString
                            .Prop42 = linea.Item("42").ToString
                            .Prop43 = linea.Item("43").ToString
                            .Prop44 = linea.Item("44").ToString
                            .Prop45 = linea.Item("45").ToString
                            .Prop46 = linea.Item("46").ToString
                            .Prop47 = linea.Item("47").ToString
                            .Prop48 = linea.Item("48").ToString
                            .Prop49 = linea.Item("49").ToString
                            .Prop50 = linea.Item("50").ToString
                            .Prop51 = linea.Item("51").ToString
                            .Prop52 = linea.Item("52").ToString
                            .Prop53 = linea.Item("53").ToString
                            .Prop54 = linea.Item("54").ToString
                            .Prop55 = linea.Item("55").ToString
                            .Prop56 = linea.Item("56").ToString
                            .Prop57 = linea.Item("57").ToString
                            .Prop58 = linea.Item("58").ToString
                            .Prop59 = linea.Item("59").ToString
                            .Prop60 = linea.Item("60").ToString
                            .Prop61 = linea.Item("61").ToString
                            .Prop62 = linea.Item("62").ToString
                            .Prop63 = linea.Item("63").ToString
                            .Prop64 = linea.Item("64").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getArticulosOfertas(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntArticuloOferta)

        Dim oLista As New List(Of EntArticuloOferta)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_ARTICULOSOFERTAS WHERE 1=1 "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ARTICULOSOFERTAS", "TAB_ARTICULOSOFERTAS")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntArticuloOferta
                        With oEntidad

                            .ItemCode = linea.Item("ItemCode").ToString
                            .Almacen = linea.Item("Almacen").ToString
                            .Cantidad = CDbl(linea.Item("Cantidad").ToString)
                            .FechaCaducidad = CInt(linea.Item("FechaCaducidad").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getArticulosPrecios(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntArticuloPrecio)

        Dim oLista As New List(Of EntArticuloPrecio)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_ArticulosPrecios "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ArticuloPrecio", "TAB_ArticuloPrecio")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntArticuloPrecio
                        With oEntidad

                            .ItemCode = linea.Item("ItemCode").ToString
                            .ListaPrecios = linea.Item("ListaPrecios").ToString
                            .ListaNombre = linea.Item("ListaNombre").ToString
                            .Precio = CDbl(linea.Item("Precio").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getArticulosStock(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntArticuloStock)

        Dim oLista As New List(Of EntArticuloStock)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_ArticulosStocks WHERE 1=1 "
            SQL &= " And EmpID = " & Empleado

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ArticuloStock", "TAB_ArticuloStock")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntArticuloStock
                        With oEntidad

                            .ItemCode = linea.Item("ItemCode").ToString
                            .WhsCode = linea.Item("WhsCode").ToString
                            .WhsName = linea.Item("WhsName").ToString
                            .StockAlmacen = CDbl(linea.Item("StockAlmacen").ToString)
                            .StockOferta = CDbl(linea.Item("StockOferta").ToString)
                            .StockComprometido = CDbl(linea.Item("StockComprometido").ToString)
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function




    'USUARIOS

    Public Function getUsuarios(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntUsuario)

        Dim oLista As New List(Of EntUsuario)

        Try

            'Si no es admin.
            'If Empleado <> 0 Then
            ''Actualizamos el acumulado del usuario.
            'ActualizarAcumuladoObjetivoUsuario(Empleado, Version)
            'End If

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_Usuarios WHERE 1=1 "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Usuarios", "Usuarios")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntUsuario
                        With oEntidad


                            .EmpID = CInt(linea.Item("EmpID"))
                            .Name = linea.Item("Name").ToString
                            .Pass = linea.Item("Pass").ToString
                            .SuperUser = linea.Item("SuperUser").ToString
                            .SAPUser = CInt(linea.Item("SAPUser").ToString)
                            .ObjetivoMensual = CDbl(linea.Item("ObjetivoMensual").ToString)

                            'Nuevo valor.
                            .ObjetivoAcumulado = CDbl(linea.Item("ObjetivoAcumulado").ToString)

                            .NumeroCobrosSAPDia = CInt(linea.Item("NumeroCobrosSAPDia").ToString)
                            .NumeroCobrosSAPMes = CInt(linea.Item("NumeroCobrosSAPMes").ToString)
                            .NumeroPedidosSAPDia = CInt(linea.Item("NumeroPedidosSAPDia").ToString)
                            .NumeroPedidosSAPMes = CInt(linea.Item("NumeroPedidosSAPMes").ToString)

                            .ImporteCobrosSAPDia = CDbl(linea.Item("ImporteCobrosSAPDia").ToString)
                            .ImporteCobrosSAPMes = CDbl(linea.Item("ImporteCobrosSAPMes").ToString)
                            .ImportePedidosSAPDia = CDbl(linea.Item("ImportePedidosSAPDia").ToString)
                            .ImportePedidosSAPMes = CDbl(linea.Item("ImportePedidosSAPMes").ToString)

                            .AlmacenCentral = linea.Item("AlmacenCentral").ToString
                            .AlmacenUsuario = linea.Item("AlmacenUsuario").ToString
                            .ListaPrecioCompra = linea.Item("ListaPrecioCompra").ToString
                            .ListaPrecioVenta = linea.Item("ListaPrecioVenta").ToString

                            .Localizable = linea.Item("Localizable").ToString
                            .CambioRuta = linea.Item("CambioRuta").ToString

                            .EmpIDExtra1 = CInt(linea.Item("EmpIDExtra1").ToString)
                            .EmpIDExtra2 = CInt(linea.Item("EmpIDExtra2").ToString)

                            .NumeracionActualFactura = linea.Item("NumeracionActualFactura").ToString
                            .NumeracionActualNotaCredito = linea.Item("NumeracionActualNotaCredito").ToString
                            .NumeracionActualCobro = linea.Item("NumeracionActualCobro").ToString
                            .Prefijo = linea.Item("Prefijo").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If

            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getBancos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntBanco)

        Dim oLista As New List(Of EntBanco)
        Try

            Dim SQL As String = ""
            SQL &= "SELECT BankCode as IDBanco, BankName as NombreBanco FROM ODSC"

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Banco", "TAB_Banco")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntBanco
                        With oEntidad

                            .IDBanco = linea.Item("IDBanco").ToString
                            .NombreBanco = linea.Item("NombreBanco").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    'CLIENTES

    Public Function getClientes(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntCliente)

        Dim oLista As New List(Of EntCliente)

        Try

            Dim SQL As String = ""
            SQL &= " Select * FROM TAB_Clientes WHERE 1=1 "
            SQL &= " And"
            SQL &= " EmpID = " & Empleado

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Clientes", "Cliente")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntCliente
                        With oEntidad

                            .Codigo = linea.Item("Codigo").ToString
                            .Nombre = linea.Item("Nombre").ToString
                            .NombreComercial = linea.Item("NombreComercial").ToString
                            .NIF = linea.Item("NIF").ToString
                            .FechaAlta = CInt(linea.Item("FechaAlta").ToString)

                            .Grupo = CInt(linea.Item("Grupo").ToString)
                            .EmpID = CInt(linea.Item("EmpID").ToString)
                            .Congelado = linea.Item("Congelado").ToString
                            .Telefono1 = linea.Item("Telefono1").ToString
                            .Telefono2 = linea.Item("Telefono2").ToString
                            .TelefonoMv = linea.Item("TelefonoMv").ToString
                            .Email = linea.Item("Email").ToString
                            .Contacto = linea.Item("Contacto").ToString
                            .LimiteCreditoTotal = CDbl(linea.Item("LimiteCreditoTotal").ToString)
                            .CreditoConsumido = CDbl(linea.Item("CreditoConsumido").ToString)
                            .CreditoDisponible = CDbl(linea.Item("CreditoDisponible").ToString)
                            .GestionSAC = linea.Item("GestionSAC").ToString
                            .ImpMedioPedido = CDbl(linea.Item("ImpMedioPedido").ToString)
                            .FechaUltAlbaran = CInt(linea.Item("FechaUltAlbaran").ToString)
                            .ObsAlbaran = linea.Item("ObsAlbaran").ToString
                            .Notas = linea.Item("Notas").ToString
                            .DireccionFactura = linea.Item("DireccionFactura").ToString
                            .PoblacionFactura = linea.Item("PoblacionFactura").ToString
                            .CPFactura = linea.Item("CPFactura").ToString
                            .CiudadFactura = linea.Item("CiudadFactura").ToString
                            .ProvinciaFactura = linea.Item("ProvinciaFactura").ToString
                            .Latitud = linea.Item("Latitud").ToString
                            .Longitud = linea.Item("Longitud").ToString
                            .CondicionPago = linea.Item("CondicionPago").ToString
                            .DiaPago = linea.Item("DiaPago").ToString
                            .ViaPago = linea.Item("ViaPago").ToString
                            .Facturacion = linea.Item("Facturacion").ToString
                            .ListaPreciosCode = linea.Item("ListaPreciosCode").ToString
                            .ListaPreciosName = linea.Item("ListaPreciosName").ToString
                            .PlazoMedioPago = CInt(linea.Item("PlazoMedioPago").ToString)
                            .NumeroCuenta = linea.Item("NumeroCuenta").ToString
                            .RecargoEquiv = linea.Item("RecargoEquiv").ToString

                            .Categoria = linea.Item("Categoria").ToString
                            .Actividad = linea.Item("Actividad").ToString
                            .Sincro = linea.Item("Sincro").ToString
                            .FechaSincro = CInt(linea.Item("FechaSincro").ToString)
                            .IncidenciaSincro = linea.Item("IncidenciaSincro").ToString

                            .VentasAnyoAnt = CDbl(linea.Item("VentasAnyoAnt").ToString)
                            .VentasUltAnyo = CDbl(linea.Item("VentasUltAnyo").ToString)
                            .DescuentoEfectivo = linea.Item("DescuentoEfectivo").ToString
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    Public Function getClientesContactos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntClienteContacto)

        Dim oLista As New List(Of EntClienteContacto)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM TAB_ClientesContactos WHERE 1=1 "
            SQL &= " AND "
            '("
            SQL &= " EmpID = " & Empleado



            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ClientesContactos", "TAB_ClientesContactos")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntClienteContacto
                        With oEntidad

                            .Orden = CInt(linea.Item("Orden").ToString)
                            .CardCode = linea.Item("CardCode").ToString
                            .Puesto = linea.Item("Puesto").ToString
                            .Nombre = linea.Item("Nombre").ToString
                            .Email = linea.Item("Email").ToString
                            .Telefono = linea.Item("Telefono").ToString
                            .EmpID = CInt(linea.Item("EmpID").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getClientesDirecciones(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntClienteDireccion)

        Dim oLista As New List(Of EntClienteDireccion)

        Try

            Dim SQL As String = ""

            SQL = "Select * From TAB_ClientesDirecciones Where 1 = 1 And (EmpID = " & Empleado & "Or " +
            "EmpID = (Select ISNULL(E1.U_SEIEmp1, 0) From OHEM E1 Where E1.EmpID = " & Empleado & " ) Or " +
            "EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & "))"


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ClientesDirecciones", "TAB_ClientesDirecciones")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntClienteDireccion
                        With oEntidad

                            .CardCode = linea.Item("CardCode").ToString
                            .IDDireccion = linea.Item("IDDireccion").ToString
                            .Direccion = linea.Item("Direccion").ToString
                            .Poblacion = linea.Item("Poblacion").ToString
                            .CP = linea.Item("CP").ToString
                            .Ciudad = linea.Item("Ciudad").ToString
                            .Provincia = linea.Item("Provincia").ToString
                            .Latitud = linea.Item("Latitud").ToString
                            .Longitud = linea.Item("Longitud").ToString

                            .Predeterminada = linea.Item("Predeterminada").ToString

                            .RutaID = linea.Item("RutaID").ToString
                            .RutaNombre = linea.Item("RutaNombre").ToString
                            .Lunes = linea.Item("Lunes").ToString
                            .Martes = linea.Item("Martes").ToString
                            .Miercoles = linea.Item("Miercoles").ToString
                            .Jueves = linea.Item("Jueves").ToString
                            .Viernes = linea.Item("Viernes").ToString
                            .Sabado = linea.Item("Sabado").ToString
                            .EmpID = CInt(linea.Item("EmpID").ToString)



                            .Sincro = "S"
                            .FechaSincro = 0
                            .IncidenciaSincro = ""

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    Public Function getClientesRuta(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntClienteRuta)

        Dim oLista As New List(Of EntClienteRuta)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_ClientesRuta WHERE 1=1 "
            SQL &= " And "
            '("
            SQL &= " EmpID = " & Empleado & " "
            'SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            'SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            'SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ClientesRuta", "TAB_ClientesRuta")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntClienteRuta
                        With oEntidad

                            .Ruta = linea.Item("Ruta").ToString
                            .Total = CInt(linea.Item("Total").ToString)
                            .Dia = linea.Item("Dia").ToString


                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    'HISTORICOS

    Public Function getHistoricoDocumentosCab(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntHistoricoDocumentoCab)

        Dim oLista As New List(Of EntHistoricoDocumentoCab)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_HistoricoDocumentosCab WHERE 1=1 "
            SQL &= " And EmpID = " & Empleado

            'De momento solo lo del empleado. Es mucha info...
            'SQL &= " And ("
            'SQL &= " EmpID = " & Empleado & " Or "
            'SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            'SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            'SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_HistoricoDocumentosCab", "TAB_HistoricoDocumentosCab")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntHistoricoDocumentoCab
                        With oEntidad

                            .DocEntrySAP = CLng(linea.Item("DocEntrySAP").ToString)
                            .DocNumSAP = linea.Item("DocNumSAP").ToString
                            .TipoDoc = linea.Item("TipoDoc").ToString

                            .CardCode = linea.Item("CardCode").ToString
                            .CardName = linea.Item("CardName").ToString
                            .RutaID = linea.Item("RutaID").ToString
                            .DocDate = CInt(linea.Item("DocDate").ToString)
                            .EmpID = CInt(linea.Item("EmpID").ToString)
                            .FechaEntrega = CInt(linea.Item("FechaEntrega").ToString)
                            .Longitud = linea.Item("Longitud").ToString
                            .Latitud = linea.Item("Latitud").ToString
                            .Comentarios = linea.Item("Comentarios").ToString
                            .HoraCreacion = CInt(linea.Item("HoraCreacion").ToString)
                            .CondicionPago = linea.Item("CondicionPago").ToString
                            .TotalBruto = CDbl(linea.Item("TotalBruto").ToString)
                            .TotalNeto = CDbl(linea.Item("TotalNeto").ToString)
                            .DtoGralPorcent = CDbl(linea.Item("DtoGralPorcent").ToString)
                            .DtoGralEuros = CDbl(linea.Item("DtoGralEuros").ToString)
                            .TotalIVAIncluido = CDbl(linea.Item("TotalIVAIncluido").ToString)
                            .RecargoEquivalencia = linea.Item("RecargoEquivalencia").ToString
                            .IDDireccion = linea.Item("IDDireccion").ToString
                            .Direccion = linea.Item("Direccion").ToString
                            .Poblacion = linea.Item("Poblacion").ToString
                            .CP = linea.Item("CP").ToString
                            .Ciudad = linea.Item("Ciudad").ToString
                            .Provincia = linea.Item("Provincia").ToString
                            .NumeroPedidoCliente = linea.Item("NumeroPedidoCliente").ToString
                            .Sincro = "S"


                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getHistoricoDocumentosLin(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntHistoricoDocumentoLin)

        Dim oLista As New List(Of EntHistoricoDocumentoLin)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_HistoricoDocumentosLin WHERE 1=1 "
            SQL &= " And EmpID = " & Empleado

            'SQL &= " And ("
            'SQL &= " EmpID = " & Empleado & " Or "
            'SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            'SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            'SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_HistoricoDocumentosLin", "TAB_HistoricoDocumentosLin")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntHistoricoDocumentoLin
                        With oEntidad

                            .DocEntrySAP = CLng(linea.Item("DocEntrySAP").ToString)
                            .LineNumSAP = CLng(linea.Item("LineNumSAP").ToString)
                            .TipoDoc = linea.Item("TipoDoc").ToString

                            .ItemCode = linea.Item("ItemCode").ToString
                            .ItemDesc = linea.Item("ItemDesc").ToString

                            .CantidadFactor = CDbl(linea.Item("CantidadFactor").ToString)
                            .CantidadReal = CDbl(linea.Item("CantidadReal").ToString)
                            .TipoUnidadesCajas = linea.Item("TipoUnidadesCajas").ToString

                            .Precio = CDbl(linea.Item("Precio").ToString)
                            .dto1 = CDbl(linea.Item("dto1").ToString)
                            .dto2 = CDbl(linea.Item("dto2").ToString)
                            .dto3 = CDbl(linea.Item("dto3").ToString)
                            .dto4 = CDbl(linea.Item("dto4").ToString)
                            .dto5 = CDbl(linea.Item("dto5").ToString)
                            .dtoTotalCalculado = CDbl(linea.Item("dtoTotalCalculado").ToString)
                            .dtoManual = CDbl(linea.Item("dtoManual").ToString)
                            .CodigoDescuento = linea.Item("CodigoDescuento").ToString
                            .TotalSinDescuentos = CDbl(linea.Item("TotalSinDescuentos").ToString)
                            .TotalConDescuentos = CDbl(linea.Item("TotalConDescuentos").ToString)
                            .IVAPorcentajeLinea = CDbl(linea.Item("IVAPorcentajeLinea").ToString)
                            .IVARecargoPorcentajeLinea = CDbl(linea.Item("IVARecargoPorcentajeLinea").ToString)
                            .Sincro = "S"
                            .SOferta = linea.Item("U_SEIofertacad").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    'MENSAJES

    Public Function getMensajesEntrada(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntMensajeEntrada)

        Dim oLista As New List(Of EntMensajeEntrada)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_MensajesBajada WHERE 1=1 "
            SQL &= " And ("
            SQL &= " EmpID = " & Empleado & " Or "
            SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            SQL &= " ) "


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "Mensajes", "Mensaje")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntMensajeEntrada
                        With oEntidad

                            .Code = CLng(linea.Item("Code"))
                            .Priority = CInt(linea.Item("Priority").ToString)
                            .Subject = linea.Item("Subject").ToString
                            .UserText = CStr(linea.Item("UserText"))
                            .WasRead = CStr(linea.Item("WasRead").ToString)
                            .EmpID = CInt(linea.Item("EmpID").ToString)
                            .De = linea.Item("De").ToString
                            .Eliminado = linea.Item("Eliminado").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    'TABLAS USUARIO


    Public Function getNotas(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntNota)

        Dim oLista As New List(Of EntNota)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_Notas WHERE 1=1 "
            SQL &= " And EmpID = " & Empleado


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Notas", "TAB_Notas")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntNota
                        With oEntidad

                            .Codigo = CInt(linea.Item("Codigo"))
                            .Texto = linea.Item("Texto").ToString
                            .EmpId = CInt(linea.Item("EmpId").ToString)

                            .Sincro = linea.Item("Sincro").ToString
                            .FechaSincro = CInt(linea.Item("FechaSincro").ToString)
                            .IncidenciaSincro = linea.Item("IncidenciaSincro").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getArticulosHistoricosDesactivados(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntArticuloHistoricoDesactivado)

        Dim oLista As New List(Of EntArticuloHistoricoDesactivado)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  [TAB_ARTICULOSHISTORICOSDESACTIVADOS] WHERE 1=1 "
            SQL &= " And EmpID = " & Empleado


            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_ARTICULOSHISTORICOSDESACTIVADOS", "TAB_ARTICULOSHISTORICOSDESACTIVADOS")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntArticuloHistoricoDesactivado
                        With oEntidad

                            .ItemCode = linea.Item("ItemCode").ToString
                            .CardCode = linea.Item("CardCode").ToString
                            .EmpID = CInt(linea.Item("EmpId").ToString)
                            .Fecha = CInt(linea.Item("Fecha").ToString)
                            .Borrado = linea.Item("Borrado").ToString
                            .Sincro = linea.Item("Sincro").ToString
                            .ErrorSincro = linea.Item("ErrorSincro").ToString
                            .FechaSincro = CInt(linea.Item("FechaSincro").ToString)
                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getCatalogos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntCatalogo)

        Dim oLista As New List(Of EntCatalogo)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_Catalogos WHERE 1=1 "
            SQL &= " And ("
            SQL &= " EmpID = " & Empleado & " Or "
            SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Catalogos", "TAB_Catalogos")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntCatalogo
                        With oEntidad

                            .IDUnicoCatalogo = CLng(linea.Item("IDCatalogo"))
                            .NombreCatalogo = linea.Item("NombreCatalogo").ToString
                            .CardCode = linea.Item("CardCode").ToString
                            .CardName = linea.Item("CardName").ToString
                            .FechaCreacion = CInt(linea.Item("FechaCreacion").ToString)
                            .Notas = linea.Item("Notas").ToString
                            .EmpID = CInt(linea.Item("EmpID").ToString)
                            .Sincro = linea.Item("Sincro").ToString
                            .ErrorSincro = linea.Item("ErrorSincro").ToString
                            .Borrado = "N"

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getCatalogoArticulos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntCatalogoArticulos)

        Dim oLista As New List(Of EntCatalogoArticulos)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_CatalogosArticulos WHERE 1=1 "
            SQL &= " And ("
            SQL &= " EmpID = " & Empleado & " Or "
            SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_CatalogoArticulos", "TAB_CatalogoArticulos")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntCatalogoArticulos
                        With oEntidad

                            .IDCatalogo = CLng(linea.Item("IDCatalogo"))
                            .IDLinea = CLng(linea.Item("IDLinea").ToString)
                            .ItemCode = linea.Item("ItemCode").ToString
                            .ItemDesc = linea.Item("ItemDesc").ToString
                            .Posicion = CInt(linea.Item("Posicion").ToString)
                            .EmpID = CInt(linea.Item("EmpID").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getFavoritos(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntFavoritos)

        Dim oLista As New List(Of EntFavoritos)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_Favoritos WHERE 1=1 "
            SQL &= " And ("
            SQL &= " EmpID = " & Empleado & " Or "
            SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") Or "
            SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
            SQL &= " ) "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Favoritos", "TAB_Favoritos")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntFavoritos
                        With oEntidad

                            .EmpID = CInt(linea.Item("EmpID"))
                            .ItemCode = linea.Item("ItemCode").ToString
                            .Sincro = linea.Item("Sincro").ToString
                            .ErrorSincro = linea.Item("ErrorSincro").ToString

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    'TESORERIA

    Public Function getIngresosCab(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntTesoreriaCab)

        Dim oLista As New List(Of EntTesoreriaCab)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_IngresosCab WHERE 1=1 "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_IngresosCab", "TAB_IngresosCab")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntTesoreriaCab
                        With oEntidad

                            .IDIngresoCab = CLng(linea.Item("IDIngresoCab").ToString)
                            .FechaIngreso = CInt(linea.Item("FechaIngreso").ToString)
                            .MedioIngreso = linea.Item("MedioIngreso").ToString
                            .IDBanco = linea.Item("IDBanco").ToString
                            .NombreBanco = linea.Item("NombreBanco").ToString
                            .ImporteTotal = CDbl(linea.Item("ImporteTotal").ToString)
                            .Sincro = linea.Item("Sincro").ToString
                            .IncidenciaSincro = linea.Item("IncidenciaSincro").ToString
                            .FechaSincro = CInt(linea.Item("FechaSincro").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function

    Public Function getIngresosLin(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntTesoreriaLin)

        Dim oLista As New List(Of EntTesoreriaLin)

        Try

            Dim SQL As String = ""
            SQL &= " SELECT * FROM  TAB_IngresosLin WHERE 1=1 "

            Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_IngresosLin", "TAB_IngresosLin")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then



                    'MAPEO
                    For Each linea As DataRow In DS.Tables.Item(0).Rows
                        Dim oEntidad As New EntTesoreriaLin
                        With oEntidad

                            .IDIngresoCab = CLng(linea.Item("IDIngresoCab").ToString)
                            .IDIngresoLin = CLng(linea.Item("IdIngresoLin").ToString)
                            .DocEntryFra = CInt(linea.Item("DocEntryFra").ToString)
                            .DocNumFra = linea.Item("DocNumFra").ToString
                            .ClienteCode = linea.Item("ClienteCode").ToString
                            .ClienteName = linea.Item("ClienteName").ToString
                            .ImporteCobrado = CDbl(linea.Item("ImporteCobrado").ToString)
                            .ImporteRestante = CDbl(linea.Item("ImporteRestante").ToString)

                            .NumTalon = linea.Item("NumTalon").ToString
                            .FechaVtoTalon = CInt(linea.Item("FechaVtoTalon").ToString)

                        End With
                        oLista.Add(oEntidad)
                    Next

                End If
            End If

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    'UTILIDADES

    Public Function getEmpleadoCorrecto(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As Boolean

        Dim mCon As SqlConnection = Nothing
        Dim SQL As String = ""

        Try
            If Password = "" Then
                If Not Empleado = Nothing And Not Password = Nothing And Not Version = Nothing Then
                    clsLog.Log.Warn("Password vacio: -Empleado: " & Empleado.ToString & " -Password: " & Password & " -Version:" & Version.ToString)
                End If

                Return False
            End If

            If Empleado = 0 And CDbl(Password) = 1234 Then
                clsLog.Log.Info("Administrador logeado!")
                Return True
            End If

            If Version < Integer.Parse(ConfigurationManager.AppSettings.Get("minVersion").ToString) Then
                Throw New Exception("Version tablet obsoleta")
            End If

            SQL = "Select count(EmpID) from TAB_Usuarios WHERE EmpID = " & Empleado & " AND Pass = '" & Password & "'"

            mCon = New clsConexion().OpenConexionSQLSAP()
            Dim oCommand As SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandTimeout = 180
            oCommand.CommandText = SQL

            If (CInt(oCommand.ExecuteScalar()) > 0) Then
                oCommand = Nothing
                Return True
            Else
                oCommand = Nothing
                Return False
            End If



        Catch ex As SqlException

            If ex.Message.Contains("interbloqueo") Or ex.Message.Contains("deadlock") Then

                'Si es una excepcion de interbloqueo no notificar. Es un problema de SQL server + SAP.
                clsLog.Log.Warn(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            ElseIf ex.Number = -2 Then  'Esto es timeout, tampoco notificar.

                clsLog.Log.Warn(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            Else

                clsLog.Log.Fatal(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Throw ex

            End If

            Return False

        Catch ex As Exception

            If Not Empleado = Nothing And Not Password = Nothing And Not Version = Nothing Then
                clsLog.Log.Error("-Empleado: " & Empleado.ToString & "-Password: " & Password & "-Version:" & Version.ToString)
            End If

            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return False

        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

    End Function

    Private Function getRiesgoClientes(ByVal Empleado As Integer, ByVal Version As Integer) As List(Of EntClienteRiesgo)

        Dim oLista As New List(Of EntClienteRiesgo)

        Try

            If InsertarTemporalRiesgoClientes(Empleado) Then

                Dim SQL As String = ""
                SQL &= " SELECT * FROM TAB_ClienteRiesgo WHERE 1=1 "
                SQL &= " AND ("
                SQL &= " EmpID = " & Empleado & " OR "
                SQL &= " EmpID = (SELECT ISNULL(E1.U_SEIEmp1,0) FROM OHEM E1 WHERE E1.EmpID = " & Empleado & ") OR "
                SQL &= " EmpID = (SELECT ISNULL(E2.U_SEIEmp2,0) FROM OHEM E2 WHERE E2.EmpID = " & Empleado & ") "
                SQL &= " ) "

                Dim DS As DataSet = New clsConexion().ObtenerDS(SQL, "TAB_Riesgo", "TAB_Riesgo")

                If Not DS Is Nothing Then
                    If DS.Tables.Count > 0 Then


                        'MAPEO
                        For Each linea As DataRow In DS.Tables.Item(0).Rows
                            Dim oEntidad As New EntClienteRiesgo
                            With oEntidad

                                .Banco = linea.Item("Banco").ToString
                                .CardCode = linea.Item("CardCode").ToString
                                .CardName = linea.Item("CardName").ToString
                                .CL = linea.Item("CL").ToString
                                .Comentarios = linea.Item("Comentarios").ToString
                                .EmpID = CInt(linea.Item("EmpID").ToString)
                                .Estado = linea.Item("EmpId").ToString
                                .EV = CInt(linea.Item("EmpId").ToString)
                                .FechaContable = CInt(linea.Item("EmpId").ToString)

                                .FechaVencimiento = CInt(linea.Item("EmpId").ToString)
                                .FechaVencimientoEfecto = CInt(linea.Item("EmpId").ToString)
                                .FormaCobro = linea.Item("EmpId").ToString

                                .NumDoc = linea.Item("EmpId").ToString
                                .NumEfecto = CInt(linea.Item("EmpId").ToString)
                                .NumRemesa = CInt(linea.Item("EmpId").ToString)
                                .Pendiente = CDbl(linea.Item("EmpId").ToString)
                                .Riesgo = CDbl(linea.Item("EmpId").ToString)

                                .TipoDoc = linea.Item("EmpId").ToString
                                .TotalDoc = CDbl(linea.Item("EmpId").ToString)
                                .TotalEfecto = CDbl(linea.Item("EmpId").ToString)
                                .ViaCobro = linea.Item("EmpId").ToString


                            End With
                            oLista.Add(oEntidad)
                        Next

                    End If
                End If
            End If
        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try

        Return oLista

    End Function


    'FUNCIONES RIESGO (JOAN)

    Private Function InsertarTemporalRiesgoClientes(ByVal EmpID As Integer) As Boolean

        Dim retVal As Boolean = True
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try
            mCon = New clsConexion().OpenConexionSQLSAP()
            ocommand = mCon.CreateCommand()
            ocommand.Parameters.Clear()
            ocommand.CommandTimeout = 120

            Dim ls As String

            ls = ""
            ls = ls & " DELETE [@SEIINFO6] "
            ls = ls & " WHERE U_SEInompc ='" & EmpID & "'"

            ocommand.CommandText = ls
            ocommand.ExecuteNonQuery()

            Dim sWhere As String = ""
            sWhere = sWhere & " AND   T2.duedate >='" & CDate(Date.Now.AddYears(-1)).ToString("yyyyMMdd") & "'"
            sWhere = sWhere & " AND   T2.duedate <='" & CDate(Date.Now.AddYears(1)).ToString("yyyyMMdd") & "'"

            Dim AñadirCampos As String = " ,T0.U_SEIcredi, T0.U_SEICLAS"
            '--------------------------------------------------------------------------------------
            ' 0.1.0- Albaranes Abiertos y no Facturados 
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & Consulta_AlbaransNOFacturats(eFatherCard.Todas, AñadirCampos)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "1", EmpID)
            '--------------------------------------------------------------------------------------
            ' 0.2.0- Devoluciones en Albaranes Abiertos 
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & Consulta_Devol_en_Alba_open(eFatherCard.Todas, AñadirCampos)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "1", EmpID)

            '--------------------------------------------------------------------------------------
            ' 1.0- Facturas Pendiente de Cobro sin Pago Creado  
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_1(AñadirCampos, eFatherCard.Todas)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "1", EmpID)
            '
            '--------------------------------------------------------------------------------------
            ' 2.0- Facturas Retencion Impuestos Creado Estado - Abiertas 
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_2(eFatherCard.Todas)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "2", EmpID)
            '
            '--------------------------------------------------------------------------------------
            ' 3.0- Facturas Retencion Impuestos  Estado - Cerradas 
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_3(eFatherCard.Todas)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "3", EmpID)
            '
            '--------------------------------------------------------------------------------------
            ' 4.0- corrección de facturas estado abiertas 
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_4(eFatherCard.Todas)
            ls = ls & sWhere
            '
            InsertarDatosR(ls, "4", EmpID)

            '--------------------------------------------------------------------------------------
            ' 9.- Abonos Facturas  (TODOS) 
            '     203 it_DownPayment
            '     13  it_Invoice
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_9(AñadirCampos)
            ls = ls & sWhere

            InsertarDatosR(ls, "9", EmpID)
            '
            '--------------------------------------------------------------------------------------
            ' 11.- Registro Diario Finanzas
            '--------------------------------------------------------------------------------------
            ls = ""
            ls = ls & ConsultaPagosRecibidos_11("", AñadirCamposFinanzas(eFatherCard.Todas), eFatherCard.Todas)
            ls = ls & " AND   T0.RefDate >='" & CDate(Date.Now.AddYears(-1)).ToString("yyyyMMdd") & "'"
            ls = ls & " AND   T0.RefDate >='" & CDate(Date.Now.AddYears(1)).ToString("yyyyMMdd") & "'"

            '
            InsertarDatosR(ls, "11", EmpID)


            '***
            '--------------------------------------------------------------------------------------------------------
            ' 12.- Cartera   ( Al generar el Pagament automaticament agafa el client PADRE si es un client fill 
            '                                                                              si te consolidació IC )
            '
            '--------------------------------------------------------------------------------------------------------
            '
            Dim sWhere2 As String
            '
            ls = ""
            ls = ls & ConsultaCartera_F()  ' Facturas Venta
            sWhere2 = Replace(sWhere, "T2.", "T0.")
            sWhere2 = Replace(sWhere2, "T9.", "T6.")
            ls = ls & sWhere2
            '
            InsertarDatosR(ls, "12", EmpID)
            '
            ls = ""
            ls = ls & ConsultaCartera_A()  ' Abonos Venta
            sWhere2 = Replace(sWhere, "T2.", "T0.")
            sWhere2 = Replace(sWhere2, "T9.", "T6.")
            ls = ls & sWhere2
            '
            InsertarDatosR(ls, "12", EmpID)

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = False
        End Try

        Return retVal

    End Function

    Private Function Consulta_AlbaransNOFacturats(ByVal eFatherCard As eFatherCard, Optional sSelect As String = "") As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 0.1.- Albaranes no facturados y abiertos
        '--------------------------------------------------------------------------------------
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        '
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Albarán     ' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " T0.Doctotal as 'Total Doc.',(T2.InsTotal-T2.PaidToDate-T2.TotalBlck) as 'Pendiente'," 'T1.TotalBlck-> Pago Parcial
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode, T0.U_SEIGTOS as 'Gastos',"
        ls = ls & " T2.InstlmntID as NumPlazo" & sSelect
        ls = ls & " FROM  [ODLN] T0  INNER  JOIN [DLN6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE T2.TotalBlck <> T2.InsTotal"
        ls = ls & " AND ( T2.InsTotal- T2.PaidToDate- T2.TotalBlck)<>0" ' TotalBlck -> Importe Efecto en estado Enviado
        ls = ls & " AND   T2.Status = 'O'"
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
            ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
        ElseIf eFatherCard = clsDatosBajada.eFatherCard.No Then
            ls = ls & " AND ISNULL(T0.FatherCard,'')=''"      ' NO Tiene Codigo Consolidación o Padre
        End If
        '
        Consulta_AlbaransNOFacturats = ls
        '
    End Function
    '
    Private Function Consulta_Devol_en_Alba_open(ByVal eFatherCard As eFatherCard, Optional sSelect As String = "") As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 0.2.- Devoluciones en Albaranes Abiertos
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        '
        '--------------------------------------------------------------------------------------
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Devolución  ' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " (T0.Doctotal)*(-1) as 'Total Doc.',(T2.InsTotal-T2.PaidToDate-T2.TotalBlck)*(-1)  as 'Pendiente',"
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode, T0.U_SEIGTOS as 'Gastos',"
        ls = ls & " T2.InstlmntID as NumPlazo" & sSelect
        ls = ls & " FROM  [ORDN] T0  INNER  JOIN [RDN6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE T2.TotalBlck <> T2.InsTotal"     ' Siempre se cumple esta condicion
        ' Devoluciones en Albaranes Abiertos
        ' Una vez cerrado , por facturación o Cancelado ya no tiene que salir
        ls = ls & " AND   EXISTS (SELECT R1.DocEntry"
        ls = ls & " FROM  [dbo].[RDN1] R1 INNER JOIN ODLN R2"
        ls = ls & " ON R1.BaseEntry = R2.DocEntry"
        ls = ls & " Where T0.DocEntry = R1.DocEntry"
        ls = ls & " AND   R1.BaseType = 15"                    ' Procedente de un albaran
        ls = ls & " AND   R2.DocStatus='O' )"
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
            ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
        ElseIf eFatherCard = clsDatosBajada.eFatherCard.No Then
            ls = ls & " AND ISNULL(T0.FatherCard,'')=''"      ' NO Tiene Codigo Consolidación o Padre
        End If
        '
        Consulta_Devol_en_Alba_open = ls
        '
    End Function

    Private Function ConsultaPagosRecibidos_1(Optional sSelect As String = "", _
                                                Optional ByVal eFatherCard As eFatherCard = clsDatosBajada.eFatherCard.No) As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 1.- Facturas Pendiente de Cobro sin Pago Creado
        '--------------------------------------------------------------------------------------
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Facturas    ' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " T0.Doctotal as 'Total Doc.',(T2.InsTotal-T2.PaidToDate-T2.TotalBlck) as 'Pendiente'," 'T1.TotalBlck-> Pago Parcial
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode, T0.U_SEIGTOS as 'Gastos',"
        ls = ls & " T2.InstlmntID as NumPlazo" & sSelect
        ls = ls & " FROM  [OINV] T0  INNER  JOIN [INV6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN OSLP T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE T2.TotalBlck <> T2.InsTotal"
        ls = ls & " AND ( T2.InsTotal- T2.PaidToDate- T2.TotalBlck)<>0" ' TotalBlck -> Importe Efecto en estado Enviado
        ls = ls & " AND   T2.Status = 'O'"
        '
        Select Case eFatherCard
            Case clsDatosBajada.eFatherCard.Yes
                ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
                ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
            Case clsDatosBajada.eFatherCard.No
                ls = ls & " AND ISNULL(T0.FatherCard,'')=''"     '  Sin Codigo Consolidación
            Case clsDatosBajada.eFatherCard.Todas
        End Select
        '
        ConsultaPagosRecibidos_1 = ls
        '
    End Function

    Private Function ConsultaPagosRecibidos_2(Optional ByVal eFatherCard As eFatherCard = clsDatosBajada.eFatherCard.No) As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 2.- Facturas Retencion Impuestos Creado Estado - Abiertas -
        '--------------------------------------------------------------------------------------
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Fact.Ret.imp' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " T0.Doctotal as 'Total Doc.',T2.InsTotal-T2.PaidToDate as 'Pendiente',"
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode,"
        ls = ls & " T0.ObjType"
        ls = ls & " FROM  [ODPI] T0  INNER  JOIN [DPI6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE (T2.TotalBlck <> T2.InsTotal OR  T2.TotalBlck = 0 )"
        ls = ls & " AND    T2.Status = 'O'"
        '
        Select Case eFatherCard
            Case clsDatosBajada.eFatherCard.Yes
                ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
                ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
            Case clsDatosBajada.eFatherCard.No
                ls = ls & " AND ISNULL(T0.FatherCard,'')=''"     '  Sin Codigo Consolidación
            Case clsDatosBajada.eFatherCard.Todas
        End Select
        '
        ConsultaPagosRecibidos_2 = ls
        '
    End Function
    '
    Private Function ConsultaPagosRecibidos_3(Optional ByVal eFatherCard As eFatherCard = clsDatosBajada.eFatherCard.No) As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 3.- Facturas Retencion Impuestos  Estado - Cerradas -
        '--------------------------------------------------------------------------------------
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        '
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Fact.Ret.imp' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " T0.Doctotal as 'Total Doc.',T2.InsTotal-T2.PaidToDate as 'Pendiente',"
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode,"
        ls = ls & " T0.ObjType"
        ls = ls & " FROM  [ODPI] T0  INNER  JOIN [DPI6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE (T2.TotalBlck <> T2.InsTotal OR  T2.TotalBlck = 0 )"
        ls = ls & " AND   T2.Status = 'C'"
        ls = ls & " AND   T0.Posted = 'N'"
        ls = ls & " AND   T0.CANCELED = 'N'"
        ls = ls & " AND   T0.DpmStatus = 'O'"
        '
        Select Case eFatherCard
            Case clsDatosBajada.eFatherCard.Yes
                ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
                ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
            Case clsDatosBajada.eFatherCard.No
                ls = ls & " AND ISNULL(T0.FatherCard,'')=''"     '  Sin Codigo Consolidación
            Case clsDatosBajada.eFatherCard.Todas
        End Select
        '
        ConsultaPagosRecibidos_3 = ls
        '
    End Function
    '
    Private Function ConsultaPagosRecibidos_4(Optional ByVal eFatherCard As eFatherCard = clsDatosBajada.eFatherCard.No) As String
        '
        Dim ls As String
        Dim sCardCode As String

        '
        '--------------------------------------------------------------------------------------
        ' 4.- Corrección de Facturas Estado Abiertas
        ' No se usan la tablas OCIN, CIN6, sino que se usan OCSI y CSI6
        '--------------------------------------------------------------------------------------
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "CardCode"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        '
        ls = ""
        ls = ls & " SELECT T0." & sCardCode & " as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Fact.Correc.' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " T0.Doctotal as 'Total Doc.',T2.InsTotal-T2.PaidToDate as 'Pendiente',"
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode,"
        ls = ls & " T0.ObjType"
        ls = ls & " FROM  [OCSI] T0  INNER  JOIN [CSI6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE (T2.TotalBlck <> T2.InsTotal OR  T2.TotalBlck = 0)"
        ls = ls & " AND    T2.Status = 'O'"
        '
        Select Case eFatherCard
            Case clsDatosBajada.eFatherCard.Yes
                ls = ls & " AND        T0.FatherType = 'P'"       ' Consolidación por Pagos 
                ls = ls & " AND ISNULL(T0.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
            Case clsDatosBajada.eFatherCard.No
                ls = ls & " AND ISNULL(T0.FatherCard,'')=''"     '  Sin Codigo Consolidación
            Case clsDatosBajada.eFatherCard.Todas
        End Select
        '
        ConsultaPagosRecibidos_4 = ls
        '
    End Function

    Private Function ConsultaPagosRecibidos_9(Optional sSelect As String = "") As String
        '
        Dim ls As String
        '
        '--------------------------------------------------------------------------------------
        ' 9.- Abonos Facturas
        '     203 it_DownPayment
        '     13  it_Invoice
        '--------------------------------------------------------------------------------------
        '
        ls = ""
        ls = ls & " SELECT T0.Cardcode as 'Cliente', T0.CardName as 'Nombre Cliente',"
        ls = ls & " T2.InsTotal-T2.PaidToDate as 'Pendiente','Abonos      ' as 'Documento',T0.DocNum as 'Nº', T0.Comments 'Comentarios',"
        ls = ls & " T0.DocDate as 'F.Contab.', T2.DueDate as ' F.Vencimiento',"
        ls = ls & " (T0.Doctotal)*(-1) as 'Total Doc.',(T2.InsTotal-T2.PaidToDate-T2.TotalBlck)*(-1)  as 'Pendiente',"
        ls = ls & " T12.slpname as 'Comercial',T12.slpcode,T0.GroupNum as GroupNum,T10.Pymcode,T0.U_SEIGTOS as 'Gastos',"
        ls = ls & " T0.ObjType,'' as NumPlazo" & sSelect
        ls = ls & " FROM  [ORIN] T0  INNER  JOIN [RIN6] T2"
        ls = ls & " ON  T2.DocEntry = T0.DocEntry"
        ls = ls & " LEFT JOIN OCTG T1 ON T0.GroupNum = T1.GroupNum"
        ls = ls & " INNER JOIN OCRD T9 ON T0.Cardcode=T9.cardcode"
        ls = ls & " LEFT JOIN CRD2 T10 on T9.cardcode=T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " LEFT JOIN oslp T12 on T9.slpcode=T12.slpcode"
        ls = ls & " WHERE ( T2.InsTotal- T2.PaidToDate- T2.TotalBlck)<>0" ' TotalBlck -> Importe Efecto en estado Enviado

        'ls = ls & " WHERE (T2.TotalBlck <> T2.InsTotal OR T2.[TotalBlck] = 0)" ' *2007*
        ls = ls & " AND    T2.Status = 'O'"
        ls = ls & " AND   NOT EXISTS (SELECT U0.DocEntry FROM  [dbo].[RIN1] U0"
        ls = ls & "       WHERE (T0.DocEntry = U0.DocEntry  AND  (U0.BaseType = 13  OR  U0.BaseType = 203 )) )"
        ls = ls & "       AND   NOT EXISTS (SELECT U0.DocEntry FROM  [dbo].[RIN3] U0"
        ls = ls & "       WHERE (T0.DocEntry = U0.DocEntry"
        ls = ls & "       AND  (U0.BaseType = 13  OR  U0.BaseType = 203 )) )"
        '
        ConsultaPagosRecibidos_9 = ls
        '
    End Function

    Private Function ConsultaPagosRecibidos_11(Optional sOpcion As String = "", _
                                                Optional sSelect As String = "", _
                                                Optional ByVal eFatherCard As eFatherCard = clsDatosBajada.eFatherCard.No) As String
        '
        Dim ls As String
        Dim sCardCode As String
        '
        '--------------------------------------------------------------------------------------
        ' 11.- Registro Diario Finanzas
        '--------------------------------------------------------------------------------------
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            sCardCode = "T1.FatherCard"  ' Saldo de Cada Cliente C100002, C002530 tienen la misma factura 6002027
        Else
            sCardCode = "T0.ShortName"    ' A las Cadenas tan solo puede salir en un cliente C100002 (PADRE) C002530 (Fill)
        End If
        '
        ls = ""
        ls = ls & " SELECT " & sCardCode & " as 'Cliente', T1.CardName as 'Nombre Cliente','' as 'Forma Cobro',T11.descript as 'Via de Cobro',"
        ls = ls & " 'Diario' as 'Documento',T0.Transid as 'Nº',  "
        ls = ls & " Comentarios=CASE"
        ls = ls & " WHEN UPPER(SUBSTRING(T0.LineMemo,1,4))='IMP.' THEN 'Dotación M.'"
        ls = ls & " WHEN UPPER(SUBSTRING(T0.LineMemo,1,4))<>'IMP.' THEN T0.LineMemo"
        ls = ls & " END,"
        ls = ls & " T0.RefDate as 'F.Contab.', T0.DueDate as ' F.Vencimiento' ,"
        ls = ls & " (T0.Debit-T0.Credit) as 'Total Doc.',(T0.BalDueDeb-T0.BalDueCred) as 'Pendiente', T12.slpname as 'Comercial',"
        ls = ls & " T12.slpcode , 0 as GroupNum, T10.Pymcode, T200.Ref1" & sSelect

        '
        ls = ls & " FROM JDT1 T0"
        ls = ls & " LEFT JOIN OCRD T1 ON T0.ShortName = T1.CardCode"
        ls = ls & " LEFT JOIN CRD2 T10 on T1.cardcode  =T10.cardcode"
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode  =T11.Paymethcod"
        ls = ls & " left join oslp T12 on T1.slpcode   =T12.slpcode"
        ls = ls & " INNER join OJDT T200 on T0.TransID =T200.TransID"
        ls = ls & " LEFT JOIN OCRD T2 ON T1.FatherCard=T2.CardCode "
        '
        'ls = ls & " AND (T0.TransType=24 or T0.TransType=30 or T0.TransType=-2 or T0.TransType=46 )"   ' (-2) Opening Balance  (30) Journal Entry (Entrada en el diario)
        'ls = ls & " AND  T0.Closed = 'N'"                       ' Estado abierto
        'ls = ls & " AND (T0.Debit <> 0  OR  T0.Credit <> 0 )"

        ls = ls & " WHERE ((T0.[TransType] <> '13'"
        ls = ls & "         AND  T0.[TransType] <> '203'AND  ((T0.[TransType] <> '24'AND  T0.[TransType] <> '-5' )"
        'ls = ls & "             OR  (T0.[TransType] = '24'"
        'ls = ls & "             AND  T0.[LineType] <> 1"
        'ls = ls & "             AND  T0.[LineType] <> 2"
        'ls = ls & "             AND  T0.[LineType] <> 5 )"
        'ls = ls & "            OR  (T0.[TransType] = '-5'AND  T0.[LineType] <> 5 )"
        ls = ls & " )"
        ls = ls & "          AND  T0.[TransType] <> '165'"
        ls = ls & "          AND  T0.[TransType] <> '166'"
        ls = ls & "          AND  T0.[TransType] <> '163'"
        ls = ls & "          AND  T0.[TransType] <> '164'"
        ls = ls & "          AND  T0.[TransType] <> '14'"
        ls = ls & "          AND T0.[TransType] <> '182' )"
        ls = ls & "          OR  T0.[BatchNum] > 0 )"
        '--  AND  T0.[ShortName] = 'C2007'
        ls = ls & "   AND  T0.[Closed] = 'N'"                 ' Estado Abierto
        ls = ls & "   AND  (T0.[BalDueCred] <> 0 OR  T0.[BalDueDeb] <> 0 )"
        ls = ls & "   AND  ((T0.[SourceLine] <> -14  AND  T0.[SourceLine] <> -6 )"
        ls = ls & "     OR  T0.[SourceLine] IS NULL  )"
        ls = ls & "   AND  (T0.[TransType] <> '-2'OR  T1.[DataSource] <> 'T' )"
        ls = ls & " AND (T0.ShortName like 'C%')"
        '
        If sOpcion = "" Then
            '21-01-2009
            ' En el nou Pla Contable s'ha traspassat la compte 435000 a 436000
            ' A la Base de Dades SBOCBG2007 té el pla comptable antic i fa servir la compte 435000
            ls = ls & " AND (T0.Account='430000' OR T0.Account='435000' OR T0.Account='436000')" ' 430000 Cuenta de Cliente  (435000)Creditos de dudoso cobro
        ElseIf sOpcion = "430" Then
            ls = ls & " AND Account='430000'" ' (430000) Cuenta de Cliente
        ElseIf sOpcion = "435" Then
            ls = ls & " AND ( T0.Account='435000' OR T0.Account='436000' ) " ' (435000) Creditos de dudoso cobro .21-01-2009 ->(435000)Nou Pla Comptable
        End If
        ' 431000   Efectos Comerciales en Cartera
        ' 431100   Efectos Comerciales Descontados
        '
        If eFatherCard = clsDatosBajada.eFatherCard.Yes Then
            ls = ls & " AND        T1.FatherType = 'P'"       ' Consolidación por Pagos 
            ls = ls & " AND ISNULL(T1.FatherCard,'')<>''"     ' Tiene que tener Codigo Consolidación
        ElseIf eFatherCard = clsDatosBajada.eFatherCard.No Then
            ls = ls & " AND ISNULL(T1.FatherCard,'')=''"      ' NO Tiene Codigo Consolidación o Padre
        ElseIf eFatherCard = clsDatosBajada.eFatherCard.Todas Then
        End If
        '
        ConsultaPagosRecibidos_11 = ls
        '
    End Function

    Private Function ConsultaCartera_F() As String
        '
        Dim ls As String
        '
        ls = ""
        ls = ls & " SELECT 'Efecto' as 'Documento', T0.BoeNum as 'Nº Rebut', "
        ls = ls & " T0.PmntNum as 'Nº',  "
        ls = ls & " T0.BoeSum as 'Total Plazo', T0.DueDate as 'Vencimiento_Efecto',"
        ls = ls & " T0.BPBankCod as 'Banco presentación',"
        ls = ls & " T0.DepositNum as 'DepositNum',"
        ls = ls & " T0.PymMethNam , T0.BoeType, T0.PayMethCod as 'pymcode', T6.SlpCode as 'slpCode',"
        ls = ls & " T0.CardCode as 'Cliente',"
        ls = ls & " 'Status Efecte'= CASE"
        ls = ls & " WHEN T0.BoeStatus='F' THEN 'S/exito'"
        ls = ls & " WHEN T0.BoeStatus='S' THEN 'Enviado'"
        ls = ls & " WHEN T0.BoeStatus='G' THEN 'Creado'"
        ls = ls & " WHEN T0.BoeStatus='D' THEN 'Presentado'"
        ls = ls & " WHEN T0.BoeStatus='P' THEN 'Cobrado'"
        ls = ls & " Else 'En Proceso'"
        ls = ls & " END,"
        ls = ls & " T4.DueDate as ' F.Vencimiento',"
        ls = ls & " T5.DocDate as 'F.Contab.',"
        ls = ls & " T5.DocNum, "
        ls = ls & " T3.SumApplied as CantidadCobrada"
        ls = ls & " ,T3.InvType"
        '
        ls = ls & " ,T5.U_SEIcredi"
        ls = ls & " ,T5.U_SEICLAS"
        '
        ls = ls & " FROM OBOE T0"                                     ' Efectos
        ls = ls & " LEFT JOIN ORCT T2 ON T0.BoeKey=T2.BoeAbs"         ' Pagos Recibidos (Cobro)
        ls = ls & " LEFT JOIN RCT2 T3 ON T2.DocNum = T3.DocNum"       ' Pagos Recibidos Detalle
        ls = ls & " LEFT JOIN INV6 T4"                                ' Plazo Factura
        ls = ls & " ON T3.DocEntry=T4.DocEntry"
        ls = ls & " AND T3.InstId=T4.InstlmntID"
        ls = ls & " LEFT OUTER JOIN OINV T5"
        ls = ls & " ON T3.DocEntry=T5.DocEntry"
        ls = ls & " LEFT JOIN OCRD T6 ON T0.CardCode=T6.CardCode"     ' Clientes
        ls = ls & " LEFT JOIN OSLP T12 ON T6.SlpCode=T12.SlpCode"     ' Empleados
        ' 12-04-2010 Afegir la seleeció de vias de pagament
        ls = ls & " LEFT JOIN CRD2 T10 on T6.cardcode=T10.cardcode"   ' Vias de Pago
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"

        ls = ls & " Where T0.BoeType = 'I'"                           ' (I) Incoming -Cobro  (O)Outgoing -Pago-
        ls = ls & " AND (T0.BoeStatus='S' or T0.BoeStatus='G' or T0.BoeStatus='D')"
        ls = ls & " AND T3.InvType=13"
        '
        ConsultaCartera_F = ls
        '
    End Function
    '
    Private Function ConsultaCartera_A() As String
        '
        Dim ls As String
        '
        ls = ""
        ls = ls & " SELECT 'Efecto' as 'Documento', T0.BoeNum as 'Nº Rebut', "
        ls = ls & " T0.PmntNum as 'Nº',  "
        ls = ls & " T0.BoeSum as 'Total Plazo', T0.DueDate as 'Vencimiento_Efecto',"
        ls = ls & " T0.BPBankCod as 'Banco presentación',"
        ls = ls & " T0.DepositNum as 'DepositNum',"
        ls = ls & " T0.PymMethNam , T0.BoeType, T0.PayMethCod as 'pymcode', T6.SlpCode as 'slpCode',"
        ls = ls & " T0.CardCode as 'Cliente',"
        ls = ls & " 'Status Efecte'= CASE"
        ls = ls & " WHEN T0.BoeStatus='F' THEN 'S/exito'"
        ls = ls & " WHEN T0.BoeStatus='S' THEN 'Enviado'"
        ls = ls & " WHEN T0.BoeStatus='G' THEN 'Creado'"
        ls = ls & " WHEN T0.BoeStatus='D' THEN 'Presentado'"
        ls = ls & " WHEN T0.BoeStatus='P' THEN 'Cobrado'"
        ls = ls & " Else 'En Proceso'"
        ls = ls & " END,"
        ls = ls & " T4.DueDate as ' F.Vencimiento',"
        ls = ls & " T5.DocDate as 'F.Contab.',"
        ls = ls & " T5.DocNum, "
        ls = ls & " T3.SumApplied as CantidadCobrada"
        ls = ls & " ,T3.InvType"
        '
        ls = ls & " ,T5.U_SEIcredi"
        ls = ls & " ,T5.U_SEICLAS"
        '
        ls = ls & " FROM OBOE T0"                                     ' Efectos
        ls = ls & " LEFT JOIN ORCT T2 ON T0.BoeKey=T2.BoeAbs"         ' Pagos Recibidos (Cobro)
        ls = ls & " LEFT JOIN RCT2 T3 ON T2.DocNum = T3.DocNum"       ' Pagos Recibidos Detalle
        ls = ls & " LEFT JOIN RIN6 T4"                                ' Plazo Factura
        ls = ls & " ON T3.DocEntry=T4.DocEntry"
        ls = ls & " AND T3.InstId=T4.InstlmntID"
        ls = ls & " LEFT OUTER JOIN ORIN T5"
        ls = ls & " ON T3.DocEntry=T5.DocEntry"
        ls = ls & " LEFT JOIN OCRD T6 ON T0.CardCode=T6.CardCode"     ' Clientes
        ls = ls & " LEFT JOIN OSLP T12 ON T6.SlpCode=T12.SlpCode"     ' Empleados
        ' 12-04-2010 Afegir la seleeció de vias de pagament
        ls = ls & " LEFT JOIN CRD2 T10 on T6.cardcode=T10.cardcode"   ' Vias de Pago
        ls = ls & " LEFT JOIN OPYM T11 on T10.Pymcode=T11.Paymethcod"
        ls = ls & " Where T0.BoeType = 'I'"                           ' (I) Incoming -Cobro  (O)Outgoing -Pago-
        ls = ls & " AND (T0.BoeStatus='S' or T0.BoeStatus='G' or T0.BoeStatus='D')"
        ls = ls & " AND T3.InvType=14"
        '
        ConsultaCartera_A = ls
        '
    End Function

    Private Function InsertarDatosR(ByVal sSQL As String, ByVal sPaso As String, ByVal EmpID As Integer) As Boolean

        Dim retVal As Boolean = True
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand
        Dim SQLInsert As String = ""

        Try

            Dim DS As DataSet = New clsConexion().ObtenerDS(sSQL, "Riesgos", "Riesgos")


            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then

                    mCon = New clsConexion().OpenConexionSQLSAP()
                    ocommand = mCon.CreateCommand()
                    ocommand.Parameters.Clear()
                    ocommand.CommandTimeout = 120

                    For Each DR As DataRow In DS.Tables(0).Rows

                        Dim NextCode As String = New clsUtil().getNextCodeTabla("[@SEIINFO6]", mCon)

                        If sPaso <> "12" Then
                            Dim dRiesgo As Double = 0
                            Dim sClasif As String = ""

                            If sPaso = "11" Then
                                BuscarCredito_Finanzas(DR.Item("Cliente").ToString, CLng(DR.Item("Nº").ToString), DR.Item("Ref1").ToString, dRiesgo, sClasif)
                            End If


                            SQLInsert = " INSERT INTO [@SEIINFO6]"
                            SQLInsert &= "([Code]"
                            SQLInsert &= " ,[Name]"
                            SQLInsert &= " ,[U_SEInompc]"
                            SQLInsert &= " ,[U_SEItipod]"
                            SQLInsert &= " ,[U_slpcode]" '5
                            SQLInsert &= " ,[U_cardcode]"
                            SQLInsert &= " ,[U_docnum]"
                            SQLInsert &= " ,[U_groupnum]"
                            SQLInsert &= " ,[U_pymcode]"
                            SQLInsert &= " ,[U_comments]" '10
                            SQLInsert &= " ,[U_docdate]"
                            SQLInsert &= " ,[U_duedate]"
                            SQLInsert &= " ,[U_doctotal]"
                            SQLInsert &= " ,[U_docpend]"
                            SQLInsert &= " ,[U_SEIrefer]" '15
                            SQLInsert &= " ,[U_SEIplazo]"
                            SQLInsert &= " ,[U_SEItotap]"
                            SQLInsert &= " ,[U_SEIimpcp]"
                            SQLInsert &= " ,[U_SEIimppp]"
                            SQLInsert &= " ,[U_SEIrebut]" '20
                            SQLInsert &= " ,[U_SEIbanco]"
                            SQLInsert &= " ,[U_SEIcobef]"
                            SQLInsert &= " ,[U_SEIremes]"
                            SQLInsert &= " ,[U_SEIagrup]"
                            SQLInsert &= " ,[U_duedatef]" '25
                            SQLInsert &= " ,[U_SEIestef]"
                            SQLInsert &= " ,[U_BoeKey]"
                            SQLInsert &= " ,[U_SEIcredi]"
                            SQLInsert &= " ,[U_SEICLAS]"
                            SQLInsert &= " ,[U_SEIfcre]" ' 30
                            SQLInsert &= " ,[U_SEIfcre]"
                            SQLInsert &= " ,[U_SEIfplaz]"
                            SQLInsert &= " ,[U_SEIgrupc]"
                            SQLInsert &= " ,[U_SEIpercl]"
                            SQLInsert &= " ,[U_SEIperdi]" '35
                            SQLInsert &= " ) VALUES ("
                            SQLInsert &= "'" & NextCode & "'"
                            SQLInsert &= ",'" & NextCode & "'"
                            SQLInsert &= ",'" & EmpID & "'"
                            SQLInsert &= ",'" & DR.Item("Documento").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("slpCode").ToString & "'" '5
                            SQLInsert &= ",'" & DR.Item("Cliente").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("Nº").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("groupnum").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("pymcode").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("Comentarios").ToString & "'"   '10
                            SQLInsert &= ",'" & DR.Item("F.Contab.").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("F.Vencimiento").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("Total Doc.").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("Pendiente").ToString & "'"
                            SQLInsert &= ",''" '15
                            SQLInsert &= ",''"
                            SQLInsert &= ",0"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"   '20
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",'2'"
                            SQLInsert &= ",''" '25
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",'" & dRiesgo & "'"
                            SQLInsert &= ",'" & sClasif & "'"
                            SQLInsert &= ",''" '30
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''" '35
                            SQLInsert &= ")"

                            ocommand.CommandText = SQLInsert
                            ocommand.ExecuteNonQuery()

                        Else

                            SQLInsert = " INSERT INTO [@SEIINFO6]"
                            SQLInsert &= "([Code]"
                            SQLInsert &= " ,[Name]"
                            SQLInsert &= " ,[U_SEInompc]"
                            SQLInsert &= " ,[U_SEItipod]"
                            SQLInsert &= " ,[U_slpcode]" '5
                            SQLInsert &= " ,[U_cardcode]"
                            SQLInsert &= " ,[U_docnum]"
                            SQLInsert &= " ,[U_groupnum]"
                            SQLInsert &= " ,[U_pymcode]"
                            SQLInsert &= " ,[U_comments]" '10
                            SQLInsert &= " ,[U_docdate]"
                            SQLInsert &= " ,[U_duedate]"
                            SQLInsert &= " ,[U_doctotal]"
                            SQLInsert &= " ,[U_docpend]"
                            SQLInsert &= " ,[U_SEIrefer]" '15
                            SQLInsert &= " ,[U_SEIplazo]"
                            SQLInsert &= " ,[U_SEItotap]"
                            SQLInsert &= " ,[U_SEIimpcp]"
                            SQLInsert &= " ,[U_SEIimppp]"
                            SQLInsert &= " ,[U_SEIrebut]" '20
                            SQLInsert &= " ,[U_SEIbanco]"
                            SQLInsert &= " ,[U_SEIcobef]"
                            SQLInsert &= " ,[U_SEIremes]"
                            SQLInsert &= " ,[U_SEIagrup]"
                            SQLInsert &= " ,[U_duedatef]" '25
                            SQLInsert &= " ,[U_SEIestef]"
                            SQLInsert &= " ,[U_BoeKey]"
                            SQLInsert &= " ,[U_SEIcredi]"
                            SQLInsert &= " ,[U_SEICLAS]"
                            SQLInsert &= " ,[U_SEIfcre]" ' 30
                            SQLInsert &= " ,[U_SEIfcre]"
                            SQLInsert &= " ,[U_SEIfplaz]"
                            SQLInsert &= " ,[U_SEIgrupc]"
                            SQLInsert &= " ,[U_SEIpercl]"
                            SQLInsert &= " ,[U_SEIperdi]" '35
                            SQLInsert &= " ) VALUES ("
                            SQLInsert &= "'" & NextCode & "'"
                            SQLInsert &= ",'" & NextCode & "'"
                            SQLInsert &= ",'" & EmpID & "'"
                            SQLInsert &= ",'" & DR.Item("Documento").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("slpCode").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("Cliente").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("DocNum").ToString & "'"
                            SQLInsert &= ",''"
                            SQLInsert &= ",'" & DR.Item("pymcode").ToString & "'"
                            SQLInsert &= ",''"   '10
                            SQLInsert &= ",'" & DR.Item("F.Contab.").ToString & "'"
                            SQLInsert &= ",'" & DR.Item("F.Vencimiento").ToString & "'"
                            SQLInsert &= ",0"
                            SQLInsert &= ",0"
                            SQLInsert &= ",''" '15
                            SQLInsert &= ",''"

                            If DR.Item("InvType").ToString = "14" Then
                                SQLInsert &= ",'" & CDbl(DR.Item("CantidadCobrada.").ToString) * (-1) & "'"
                            Else
                                SQLInsert &= ",'" & CDbl(DR.Item("CantidadCobrada.").ToString) & "'"
                            End If

                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",'" & DR.Item("Nº Rebut").ToString & "'"   '20
                            SQLInsert &= ",'" & DR.Item("Banco presentación").ToString & "'"
                            SQLInsert &= ",''"
                            SQLInsert &= ",'" & DR.Item("DepositNum").ToString & "'"
                            SQLInsert &= ",'1'"
                            SQLInsert &= ",'" & DR.Item("Vencimiento_Efecto").ToString & "'" '25
                            SQLInsert &= ",'" & DR.Item("Status Efecte").ToString & "'"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''" '30
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''"
                            SQLInsert &= ",''" '35
                            SQLInsert &= ")"

                            ocommand.CommandText = SQLInsert
                            ocommand.ExecuteNonQuery()

                        End If

                    Next

                End If

            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " PASO:" & sPaso & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            clsLog.Log.Fatal("SQL:" & SQLInsert)
            Throw ex
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function

    Private Sub BuscarCredito_Finanzas(ByVal sCardCode As String, ByVal lAsiento As Long, ByVal sRef1 As String, _
                                     ByRef dRiesgo As Double, ByRef sClasif As String)

        Dim ls As String

        Try

            Dim DS As DataSet

            ls = ""
            ls = ls & " SELECT T0.FatherCard,T0.CreditLine , T0.U_SEICLAS ,  T1.CreditLine AS Creditfather, T1.U_SEICLAS as clasfather"
            ls = ls & " FROM OCRD T0"
            ls = ls & " LEFT OUTER JOIN OCRD T1 ON T0.FatherCard=T1.CardCode"
            ls = ls & " WHERE T0.CardCode= '" & sCardCode & "'"

            DS = New clsConexion().ObtenerDS(ls, "BuscarCredito_Finanzas", "BuscarCredito_Finanzas")

            If Not DS Is Nothing Then
                If DS.Tables.Count > 0 Then
                    For Each DR As DataRow In DS.Tables(0).Rows

                        dRiesgo = CDbl(DR.Item("CreditLine").ToString)
                        sClasif = DR.Item("U_SEICLAS").ToString

                        If DR.Item("FatherCard").ToString <> "" Then
                            dRiesgo = CDbl(DR.Item("Creditfather").ToString)
                            sClasif = DR.Item("clasfather").ToString
                        End If

                    Next
                End If
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Private Function AñadirCamposFinanzas(ByVal eFatherCard As eFatherCard) As String
        '
        Dim ls As String
        '
        ls = ""
        ls = ls & " ,U_SEIcredi =CASE "
        ls = ls & " 				 WHEN ISNULL(T2.CardCode,'') <>  '' THEN T2.CreditLine"
        ls = ls & " ELSE T1.CreditLine"
        ls = ls & " END"
        ls = ls & " ,U_SEIclas =CASE "
        ls = ls & " WHEN ISNULL(T2.CardCode,'') <>  '' THEN T2.U_SEIclas"
        ls = ls & " ELSE T1.U_SEIclas"
        ls = ls & " END"

        'If eFatherCard = SEI_globals_CBG.eFatherCard.No Or eFatherCard = SEI_globals_CBG.eFatherCard.Todas Then
        '    ls = ""
        '    ls = ls & " ,T1.U_SEIcredi, T1.U_SEICLAS"
        'Else
        '    ls = ""
        '    ls = ls & " U_SEIcredi =CASE "
        '    ls = ls & " 				 WHEN ISNULL(T2.CardCode,'') <>  '' THEN T2.CreditLine"
        '    ls = ls & " ELSE T1.CreditLine"
        '    ls = ls & " END"
        '    ls = ls & " ,U_SEIclas =CASE "
        '    ls = ls & " WHEN ISNULL(T2.CardCode,'') <>  '' THEN T2.U_SEIclas"
        '    ls = ls & " ELSE T1.U_SEIclas"
        '    ls = ls & " END"
        'End If
        '
        AñadirCamposFinanzas = ls
        '
    End Function

    Private Sub ActualizarAcumuladoObjetivoUsuario(ByVal Empleado As Integer, ByVal Version As Integer)

        'Esta funcion actualiza el objetivo acumulado del empleado.
        'Se esta guardando en el campo Salario del empleado.

        Dim SQL As String = ""
        Dim mCon As SqlConnection = Nothing
        Dim ocommand As SqlCommand

        Try

            mCon = New clsConexion().OpenConexionSQLSAP()
            ocommand = mCon.CreateCommand()
            ocommand.Parameters.Clear()

            SQL &= " UPDATE OHEM SET salary = ISNULL((SELECT Importe FROM TAB_USUARIOS_OBJETIVO where EmpID = " & Empleado & ") ,0) WHERE empID = " & Empleado

            ocommand.CommandText = SQL
            ocommand.CommandTimeout = 120

            ocommand.ExecuteNonQuery()


        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw ex
        Finally

        End Try



    End Sub

End Class
