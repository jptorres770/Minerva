﻿Public Class EntFacturaCobroCab

    Public Property FechaDeCobro As String
    Public Property ImporteTotal As Double
    Public Property IDCliente As String
    Public Property ReferenciaDePago As String
    Public Property EsAPK As String
    Public Property MedioDePago As List(Of EntMedioDePago)
    Public Property Facturas As List(Of EntFacturaPendienteCab)

End Class
