﻿Public Class EntDocumentoLinePORTAL

    Public Property DocEntry As Integer
    Public Property ItemCode As String
    Public Property ItemDscription As String
    Public Property Quantity As String
    Public Property TaxCode As String
    Public Property WhsCode As String
    Public Property Price As Double
    Public Property DiscPrcnt As Double
    Public Property LineTotal As Double
    Public Property VatSum As Double

End Class