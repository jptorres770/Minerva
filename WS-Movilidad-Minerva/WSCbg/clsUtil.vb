﻿Imports System.IO
Imports System.IO.Compression
Imports System
Imports System.Security.Cryptography
Imports System.Text

Public Class clsUtil


    Public Shared Function CompimirGZipBase64(ByVal textoEntrada As String) As String
        Dim base64String As String = ""

        Try

            Dim comprimido As Byte()
            Using memOut = New MemoryStream()
                Using gzipLector = New GZipStream(memOut, CompressionMode.Compress)
                    Using memCompresor As New MemoryStream(Encoding.UTF8.GetBytes(textoEntrada))
                        memCompresor.CopyTo(gzipLector)
                    End Using
                End Using

                comprimido = memOut.ToArray()
                base64String = Convert.ToBase64String(comprimido)

            End Using

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally

        End Try

        Return base64String

    End Function

    Public Function getNextCodeTabla(ByVal NombreTabla As String, ByRef oCon As SqlClient.SqlConnection) As String
        Dim retVal As String = ""
        Dim SQL As String = ""

        Try

            SQL = "Select ISNULL(MAX(CAST(Code as numeric)),0) + 1 from " & NombreTabla

            Dim oCommand As SqlClient.SqlCommand = oCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CStr(oCommand.ExecuteScalar())
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally

        End Try

        Return retVal

    End Function

    Public Function getCorreosEmpleado(ByVal Empleado As Integer) As String
        Dim retVal As String = ""
        Dim SQL As String = ""
        Dim mCon As SqlClient.SqlConnection = Nothing

        Try
            mCon = New clsConexion().OpenConexionSQLSAP

            'Siempre separados por comas
            SQL = "SELECT REPLACE(ISNULL(U_SEIMails,''), ';',',') as U_SEIMails FROM OHEM where EmpID='" & Empleado & "'"

            Dim oCommand As SqlClient.SqlCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CStr(oCommand.ExecuteScalar())
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


    '------------------------------------------ TOKEN

    Public Function GetMd5Hash(ByVal md5Hash As MD5, ByVal input As String) As String

        ' Convert the input string to a byte array and compute the hash.
        Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function 'GetMd5Hash


    ' Verify a hash against a string.
    Public Function VerifyMd5Hash(ByVal md5Hash As MD5, ByVal input As String, ByVal hash As String) As Boolean
        ' Hash the input.
        Dim hashOfInput As String = GetMd5Hash(md5Hash, input)

        ' Create a StringComparer an compare the hashes.
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

        If 0 = comparer.Compare(hashOfInput, hash) Then
            Return True
        Else
            Return False
        End If

    End Function 'VerifyMd5Hash


End Class